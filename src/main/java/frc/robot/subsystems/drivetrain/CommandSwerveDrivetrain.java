package frc.robot.subsystems.drivetrain;

import com.ctre.phoenix6.BaseStatusSignal;
import com.ctre.phoenix6.StatusCode;
import com.ctre.phoenix6.Utils;
import com.ctre.phoenix6.configs.CurrentLimitsConfigs;
import com.ctre.phoenix6.configs.TalonFXConfiguration;
import com.ctre.phoenix6.configs.TorqueCurrentConfigs;
import com.ctre.phoenix6.controls.CoastOut;
import com.ctre.phoenix6.controls.StaticBrake;
import com.ctre.phoenix6.controls.TorqueCurrentFOC;
import com.ctre.phoenix6.controls.VoltageOut;
import com.ctre.phoenix6.mechanisms.swerve.*;
import com.ctre.phoenix6.mechanisms.swerve.utility.PhoenixPIDController;
import com.pathplanner.lib.auto.AutoBuilder;
import com.pathplanner.lib.commands.PathPlannerAuto;
import com.pathplanner.lib.controllers.PPHolonomicDriveController;
import com.pathplanner.lib.util.GeometryUtil;
import com.pathplanner.lib.util.HolonomicPathFollowerConfig;
import com.pathplanner.lib.util.PIDConstants;
import com.pathplanner.lib.util.ReplanningConfig;
import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.filter.LinearFilter;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.math.system.plant.DCMotor;
import edu.wpi.first.math.trajectory.TrapezoidProfile;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.units.Angle;
import edu.wpi.first.units.Distance;
import edu.wpi.first.units.Measure;
import edu.wpi.first.units.Velocity;
import edu.wpi.first.wpilibj.*;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.FieldConstants;
import frc.robot.generated.TunerConstants;
import frc.robot.subsystems.superstructure.PIDToPositionCommand;
import frc.robot.util.SwerveSetpointGenerator;
import frc.robot.util.TalonUtil;
import frc.robot.util.Util;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static edu.wpi.first.units.Units.*;

/**
 * Class that extends the Phoenix SwerveDrivetrain class and implements subsystem
 * so it can be used in command-based projects easily.
 */
public class CommandSwerveDrivetrain extends SwerveDrivetrain implements Subsystem {
    static final double DEFAULT_TRANSLATIONAL_DEADBAND = 0.1;
    static final double DEFAULT_ROTATIONAL_DEADBAND = Units.degreesToRadians(0.1);
    private static final double kSimLoopPeriod = 0.005; // 5 ms
    private static final double BASE_KP = 11;
    private static final boolean USE_SETPOINT_GENERATOR = false;
    private static final boolean USE_TRAPEZOID = true;
    public final double AIM_KP;
    public final double AIM_KI = 0;
    public final double AIM_KD = 0;
    private final DCMotor kraken = DCMotor.getKrakenX60Foc(1);
    CoastTrain coaster = new CoastTrain();
    BrakeTrain brake = new BrakeTrain();
    private Pose2d prevPose = new Pose2d(new Translation2d(0, 0), Rotation2d.fromDegrees(0));
    private double prevTime = 0;
    private Pose2d curPose = new Pose2d();
    private Notifier simNotifier;
    private double lastSimTime;
    private Rotation2d autoDesiredAngle = Rotation2d.fromDegrees(0);
    private SwerveRequest.ApplyChassisSpeeds autoRequest = new ApplySpeedsWithDesat()
            .withDriveRequestType(SwerveModule.DriveRequestType.Velocity);
    // Request objects and methods
    private SwerveRequest.RobotCentric forwardStraight = new SwerveRequest.RobotCentric().withDriveRequestType(SwerveModule.DriveRequestType.OpenLoopVoltage);
    private DesatFacingAngle face = (DesatFacingAngle) new DesatFacingAngle()
            .withFieldCentric(true)
            .withDeadband(DEFAULT_TRANSLATIONAL_DEADBAND)
            .withRotationalDeadband(DEFAULT_ROTATIONAL_DEADBAND);
    private DesatFacingAngle robotCentric = (DesatFacingAngle) new DesatFacingAngle()
            .withFieldCentric(false)
            .withDeadband(DEFAULT_TRANSLATIONAL_DEADBAND)
            .withRotationalDeadband(DEFAULT_ROTATIONAL_DEADBAND);
    private DesatFieldCentric fieldCentric = (DesatFieldCentric) new DesatFieldCentric()
            .withDeadband(DEFAULT_TRANSLATIONAL_DEADBAND)
            .withRotationalDeadband(DEFAULT_ROTATIONAL_DEADBAND);

    private DesatFieldCentric robotCentricDrive = (DesatFieldCentric) new DesatFieldCentric()
            .withFieldCentric(false)
            .withDeadband(DEFAULT_TRANSLATIONAL_DEADBAND)
            .withRotationalDeadband(DEFAULT_ROTATIONAL_DEADBAND);
    private SwerveSetpointGenerator.SwerveKinematicLimits kinematicLimits = new SwerveSetpointGenerator.SwerveKinematicLimits(TunerConstants.kSpeedAt12VoltsMps,
            kraken.getTorque(60.) / TunerConstants.kDriveInertia,
            kraken.freeSpeedRadPerSec / TunerConstants.kSteerGearRatio * 0.95);
    private SwerveSetpointGenerator setpointGenerator = new SwerveSetpointGenerator(m_kinematics, m_moduleLocations);
    private SwerveSetpointGenerator.SwerveSetpoint prevSetpoint = new SwerveSetpointGenerator.SwerveSetpoint(new ChassisSpeeds(), new SwerveModuleState[]{
            new SwerveModuleState(),
            new SwerveModuleState(),
            new SwerveModuleState(),
            new SwerveModuleState()
    });
    private boolean autonomousShooting = false;
    private LinearFilter poseAverageFilter = LinearFilter.singlePoleIIR(0.1, 0.02);
    private double poseAvg;


    public CommandSwerveDrivetrain(SwerveDrivetrainConstants driveTrainConstants, SwerveModuleConstants... modules) {
        super(driveTrainConstants, modules);
        AIM_KP = 11;
        configurePathPlanner();
//        face.HeadingController = new PhoenixPIDController(AIM_KP, 0, 0);
        face.HeadingController = new PhoenixPIDController(AIM_KP, 0, AIM_KP * 0.1); //todo playwith D
        face.HeadingController.enableContinuousInput(-Math.PI, Math.PI);

        robotCentric.HeadingController = new PhoenixPIDController(AIM_KP, 0, AIM_KP * 0.1);
        robotCentric.HeadingController.enableContinuousInput(-Math.PI, Math.PI);

        for (var module : Modules) {
            var driveMotor = module.getDriveMotor();
            TalonFXConfiguration configuration = new TalonFXConfiguration();
            var statusCode = driveMotor.getConfigurator().refresh(configuration);
            if (statusCode != StatusCode.OK) {
                DriverStation.reportWarning("Unable to refresh drive motor!", false);
                continue;
            }
            var steerMotor = module.getSteerMotor();
            var steerConfig = new TalonFXConfiguration();
            statusCode = steerMotor.getConfigurator().refresh(steerConfig);
            if (statusCode != StatusCode.OK) {
                DriverStation.reportWarning("Unable to refresh steer motor!", false);
                continue;
            }
            configuration.CurrentLimits = new CurrentLimitsConfigs()
                    .withStatorCurrentLimit(80)
                    .withStatorCurrentLimitEnable(true)
                    .withSupplyCurrentLimit(60)
                    .withSupplyCurrentLimitEnable(true)
                    .withSupplyCurrentThreshold(120)
                    .withSupplyTimeThreshold(0.25);
            configuration.TorqueCurrent = new TorqueCurrentConfigs()
                    .withPeakForwardTorqueCurrent(90)
                    .withPeakReverseTorqueCurrent(-90);
            steerConfig.CurrentLimits = configuration.CurrentLimits;
            TalonUtil.applyAndCheckConfiguration(driveMotor, configuration);
            TalonUtil.applyAndCheckConfiguration(steerMotor, steerConfig);
            driveMotor.optimizeBusUtilization();
            steerMotor.optimizeBusUtilization();
        }

        if (Utils.isSimulation()) {
            startSimThread();
            autoRequest = autoRequest.withDriveRequestType(SwerveModule.DriveRequestType.OpenLoopVoltage);
        }
    }

    public static Pose2d getMaybeFlippedPose(Pose2d pose2d) {
        return FieldConstants.shouldFlip() ? GeometryUtil.flipFieldPose(pose2d) : pose2d;
    }

    public double[] getDriveWheelPositionsRadians() {
        double[] angles = new double[]{0, 0, 0, 0};
        for (int i = 0; i < 4; i++) {
            var posSignal = Modules[i].getDriveMotor().getPosition().refresh();
            var velSignal = Modules[i].getDriveMotor().getVelocity().refresh();
            angles[i] = Units.rotationsToRadians(BaseStatusSignal.getLatencyCompensatedValue(posSignal, velSignal)) / TunerConstants.kDriveGearRatio;
        }
        return angles;
    }

    private void configurePathPlanner() {
        double driveBaseRadius = 0;
        for (var moduleLocation : m_moduleLocations) {
            driveBaseRadius = Math.max(driveBaseRadius, moduleLocation.getNorm());
        }
        PPHolonomicDriveController.setRotationTargetOverride(this::getRotationTargetOverride);
        AutoBuilder.configureHolonomic(
                () -> this.getState().Pose, // Supplier of current robot pose
                this::seedFieldRelative,  // Consumer for seeding pose against auto
                this::getCurrentRobotChassisSpeeds,
                (speeds) -> this.setControl(autoRequest.withSpeeds(speeds)), // Consumer of ChassisSpeeds to drive the robot
                new HolonomicPathFollowerConfig(new PIDConstants(14, 0, 0), // Was 8
                        new PIDConstants(12, 0, 0),
                        TunerConstants.kSpeedAt12VoltsMps,
                        driveBaseRadius,
                        new ReplanningConfig()),
                () -> DriverStation.getAlliance().map((alliance -> alliance == DriverStation.Alliance.Red)).orElse(false), // ??
                this); // Subsystem for requirements
    }

    public void driveStraight(Measure<Velocity<Distance>> linearX, Measure<Velocity<Distance>> linearY) {
        this.setControl(forwardStraight.withVelocityX(linearX.in(MetersPerSecond))
                .withVelocityY(linearY.in(MetersPerSecond))
                .withDriveRequestType(SwerveModule.DriveRequestType.OpenLoopVoltage));
    }

    private SwerveModule.DriveRequestType getMode() {
        return RobotBase.isReal() ?
                SwerveModule.DriveRequestType.Velocity
                : SwerveModule.DriveRequestType.OpenLoopVoltage;
    }

    public void faceAngleFieldCentric(Rotation2d angle, Measure<Velocity<Distance>> xVel, Measure<Velocity<Distance>> yVel, boolean trapezoid) {
        this.setControl(face
                .withTrapezoid(trapezoid)
                .withTargetDirection(angle)
                .withVelocityX(xVel.in(MetersPerSecond))
                .withVelocityY(yVel.in(MetersPerSecond))
                .withDriveRequestType(getMode()));
    }

    public void faceAngleRobotCentric(Rotation2d angle, Measure<Velocity<Distance>> xVel, Measure<Velocity<Distance>> yVel) {
        this.setControl(robotCentric
                .withTrapezoid(false)
                .withVelocityX(xVel.in(MetersPerSecond))
                .withVelocityY(yVel.in(MetersPerSecond))
                .withTargetDirection(angle)
                .withDriveRequestType(getMode()));
    }

    public Optional<Rotation2d> getRotationTargetOverride() {
        if (autonomousShooting) {
            return Optional.of(autoDesiredAngle);
        }
        return Optional.empty();
    }

    public void setAutonomousShooting(boolean enabled) {
        autonomousShooting = enabled;
    }

    public void setAutonomousShotHeading(Rotation2d desiredHeading) {
        autoDesiredAngle = desiredHeading;
    }

    public void faceAngleFieldCentric(Rotation2d angle, Measure<Velocity<Distance>> xVel, Measure<Velocity<Distance>> yVel) {
        faceAngleFieldCentric(angle, xVel, yVel, USE_TRAPEZOID);
    }

    public void stop() {
        this.setControl(fieldCentric
                .withVelocityX(0)
                .withVelocityY(0));
    }

    public void driveFieldCentric(Measure<Velocity<Distance>> xVel, Measure<Velocity<Distance>> yVel, Measure<Velocity<Angle>> thetaVel) {
        this.setControl(fieldCentric
                .withVelocityX(xVel.in(MetersPerSecond))
                .withVelocityY(yVel.in(MetersPerSecond))
                .withRotationalRate(thetaVel.in(RadiansPerSecond))
                .withDriveRequestType(getMode()));
    }

    public void driveRobotCentric(Measure<Velocity<Distance>> xVel, Measure<Velocity<Distance>> yVel, Measure<Velocity<Angle>> thetaVel) {
        this.setControl(robotCentricDrive
                .withVelocityX(xVel.in(MetersPerSecond))
                .withVelocityY(yVel.in(MetersPerSecond))
                .withRotationalRate(thetaVel.in(RadiansPerSecond))
                .withDriveRequestType(getMode()));
    }


    @Override
    public void periodic() {
        curPose = getState().Pose;
        double currentTime = Timer.getFPGATimestamp();
        var difference = curPose.minus(prevPose).getTranslation().getNorm();
        poseAvg = poseAverageFilter.calculate(difference / (currentTime - prevTime)); // Was "/ 0.02"
        prevPose = curPose;
        prevTime = currentTime;
        SmartDashboard.putNumber("Average Vel", poseAvg);
        SmartDashboard.putNumber("Angular Velocity", Units.degreesToRadians(m_angularVelocity.getValue()));
    }

    public double getMovingAverageVelMagnitude() {
        return poseAvg;
    }

    public void coastAll() {
        this.setControl(coaster);
    }

    public void brakeAll() {
        this.setControl(brake);
    }

    public Command applyRequest(Supplier<SwerveRequest> requestSupplier) {
        return run(() -> this.setControl(requestSupplier.get()));
    }

    public Command getAutoPath(String pathName) {
        return new PathPlannerAuto(pathName);
    }

    public ChassisSpeeds getCurrentRobotChassisSpeeds() {
        return m_kinematics.toChassisSpeeds(getState().ModuleStates);
    }

    private void startSimThread() {
        this.lastSimTime = Utils.getCurrentTimeSeconds();

        /* Run simulation at a faster rate so PID gains behave more reasonably */
        this.simNotifier = new Notifier(() -> {
            final double currentTime = Utils.getCurrentTimeSeconds();
            double deltaTime = currentTime - this.lastSimTime;
            this.lastSimTime = currentTime;

            /* use the measured time delta, get battery voltage from WPILib */
            updateSimState(deltaTime, RobotController.getBatteryVoltage());
        });
        this.simNotifier.startPeriodic(kSimLoopPeriod);
    }

    public Command characterizedCoupleRatioTester() {
        var request = new ConstantSpeedRotation();

        return this.run(() -> this.setControl(request)).handleInterrupt(() -> System.out.println("couple ratio " + request.getAverageDistance()));
    }

    /**
     * Common method among SwerveRequest subclasses to run Desaturate, Setpoint Smoothing, and actually apply to the modules
     */
    private void applySpeedsToModules(SwerveRequest.SwerveControlRequestParameters parameters,
                                      SwerveModule[] modulesToApply,
                                      double rotationRate,
                                      double toApplyX,
                                      double toApplyY,
                                      double Deadband,
                                      double RotationalDeadband,
                                      SwerveModule.DriveRequestType DriveRequestType,
                                      SwerveModule.SteerRequestType SteerRequestType,
                                      boolean fieldCentric) {
        double toApplyOmega = rotationRate;
        if (Math.sqrt(toApplyX * toApplyX + toApplyY * toApplyY) < Deadband) {
            toApplyX = 0;
            toApplyY = 0;
        }
        if (Math.abs(toApplyOmega) < RotationalDeadband) {
            toApplyOmega = 0;
        }

        ChassisSpeeds speeds;
        if (fieldCentric) {
            speeds = ChassisSpeeds.discretize(ChassisSpeeds.fromFieldRelativeSpeeds(toApplyX, toApplyY, toApplyOmega,
                    parameters.currentPose.getRotation()), parameters.updatePeriod);
        } else {
            speeds = ChassisSpeeds.discretize(new ChassisSpeeds(toApplyX, toApplyY, toApplyOmega), parameters.updatePeriod);
        }

        var states = parameters.kinematics.toSwerveModuleStates(speeds, new Translation2d());
        SwerveDriveKinematics.desaturateWheelSpeeds(states,
                speeds,
                Units.feetToMeters(17),
                Units.feetToMeters(17),
                4.6
        );

        speeds = parameters.kinematics.toChassisSpeeds(states);

        states = applySetpointGenerator(parameters, speeds, states);
        for (int i = 0; i < modulesToApply.length; ++i) {
            modulesToApply[i].apply(states[i], DriveRequestType, SteerRequestType);
        }
    }

    private SwerveModuleState[] applySetpointGenerator(SwerveRequest.SwerveControlRequestParameters parameters, ChassisSpeeds speeds, SwerveModuleState[] states) {
        var outputStates = states;
        if (USE_SETPOINT_GENERATOR) {
            var setpoint = setpointGenerator.generateSetpoint(kinematicLimits, prevSetpoint, speeds, parameters.updatePeriod);
            prevSetpoint = setpoint;
            outputStates = setpoint.moduleStates();
        }
        return outputStates;
    }

    public Command pathFind(Pose2d pose) {
        return new PIDToPositionCommand(this, pose);
    }

    public Command pathFindToAutoStartPos() {
        var autoStart = new Pose2d(1.35, 4.10, Rotation2d.fromDegrees(180));
        return pathFind(getMaybeFlippedPose(autoStart));
    }

    static class CoastTrain implements SwerveRequest {
        CoastOut coast = new CoastOut();
        CoastOut coastTurn = new CoastOut();

        @Override
        public StatusCode apply(SwerveControlRequestParameters parameters, SwerveModule... modulesToApply) {
            Arrays.stream(modulesToApply).forEach((mod) -> {
                mod.getDriveMotor().setControl(coast);
                mod.getSteerMotor().setControl(coastTurn);
            });
            return StatusCode.OK;
        }
    }

    static class BrakeTrain implements SwerveRequest {
        StaticBrake brake = new StaticBrake();

        @Override
        public StatusCode apply(SwerveControlRequestParameters parameters, SwerveModule... modulesToApply) {
            Arrays.stream(modulesToApply).forEach((mod) -> {
                mod.getDriveMotor().setControl(brake);
                mod.getSteerMotor().setControl(brake);
            });
            return StatusCode.OK;
        }
    }

    static class ConstantSpeedRotation implements SwerveRequest {
        public Rotation2d rotationSoFar = new Rotation2d();
        boolean started = false;
        private List<Double> startPositions;
        private List<Double> totalDistances;
        private List<Double> totalSteerRotations;
        private List<Double> startingSteerRotations;
        private VoltageOut coupleOut = new VoltageOut(1);

        public double getAverageDistance() {
            double avg = 0;
            for (int i = 0; i <= 3; i++) {
                var steerRot = (totalSteerRotations.get(i) - startingSteerRotations.get(i));
                var distanceRot = (totalDistances.get(i) - startPositions.get(i));
                avg += Math.abs(distanceRot / steerRot);
                System.out.println(i + ": Steer rot: " + steerRot + " distance rot " + distanceRot + " average ratio " + avg);
            }
            return avg / 4.0;
        }

        @Override
        public StatusCode apply(SwerveControlRequestParameters parameters, SwerveModule... modulesToApply) {
            if (!started) {
                started = true;
                startingSteerRotations = Arrays.stream(modulesToApply).map(module -> module.getSteerMotor().getPosition().refresh().getValue()).toList();
                startPositions = Arrays.stream(modulesToApply).map(module -> module.getDriveMotor().getPosition().refresh().getValue()).collect(Collectors.toList());
            }
            totalSteerRotations = Arrays.stream(modulesToApply).map(module -> module.getSteerMotor().getPosition().refresh().getValue()).toList();
            totalDistances = Arrays.stream(modulesToApply).map(module -> module.getDriveMotor().getPosition().refresh().getValue()).collect(Collectors.toList());
            Arrays.stream(modulesToApply).forEach((module) -> module.getSteerMotor().setControl(coupleOut));
            return StatusCode.OK;
        }
    }

    // Since we are using a holonomic drivetrain, the rotation component of this pose
// represents the goal holonomic rotation
    private Optional<Measure<Velocity<Distance>>> maybeGamepieceDetectionYVel = Optional.empty();

    public void setMaybeGamepieceDetectionYVel(Optional<Measure<Velocity<Distance>>> maybeVel) {
        this.maybeGamepieceDetectionYVel = maybeVel;
    }
    class ApplySpeedsWithDesat extends SwerveRequest.ApplyChassisSpeeds {
        public StatusCode apply(SwerveControlRequestParameters parameters, SwerveModule... modulesToApply) {
            if (maybeGamepieceDetectionYVel.isPresent()) {
                Speeds.vyMetersPerSecond = maybeGamepieceDetectionYVel.get().in(MetersPerSecond);
            }
            var states = parameters.kinematics.toSwerveModuleStates(Speeds, CenterOfRotation);
            SwerveDriveKinematics.desaturateWheelSpeeds(states,
                    Speeds,
                    Units.feetToMeters(13),
                    Units.feetToMeters(13),
                    4.6
            );
            var speeds = parameters.kinematics.toChassisSpeeds(states);

            states = applySetpointGenerator(parameters, speeds, states);
            for (int i = 0; i < modulesToApply.length; ++i) {
                modulesToApply[i].apply(states[i], DriveRequestType, SteerRequestType);
            }
            return StatusCode.OK;
        }
    }


    // Create the constraints to use while pathfinding

    private class DesatFacingAngle extends SwerveRequest.FieldCentricFacingAngle {
        static final Measure<Angle> TOL = Degrees.of(1);
        boolean trapezoid = USE_TRAPEZOID;
        TrapezoidProfile angleProfile = new TrapezoidProfile(new TrapezoidProfile.Constraints(Units.degreesToRadians(800), Units.degreesToRadians(800)));
        TrapezoidProfile.State goalState = new TrapezoidProfile.State(0, 0);
        boolean FieldCentric;
        private TrapezoidProfile.State setpointState = new TrapezoidProfile.State(0, 0);
        private Rotation2d lastGoal = new Rotation2d();
        private TrapezoidProfile.State startState = new TrapezoidProfile.State(0, 0);
        private double startTime = 0;

        public DesatFacingAngle() {

        }

        public DesatFacingAngle withFieldCentric(boolean fieldCentric) {
            this.FieldCentric = fieldCentric;
            return this;
        }

        public DesatFacingAngle withTrapezoid(boolean trapezoid) {
            this.trapezoid = trapezoid;
            return this;
        }

        public StatusCode apply(SwerveRequest.SwerveControlRequestParameters parameters, SwerveModule... modulesToApply) {
            double setpointAngleRad = TargetDirection.getRadians();
            double angVelRadPerSec = parameters.kinematics.toChassisSpeeds(getState().ModuleStates).omegaRadiansPerSecond;
            double angVelDegPerSec = Units.radiansToDegrees(angVelRadPerSec);

            if (!Util.epsilonEquals(lastGoal, TargetDirection, TOL) && this.trapezoid) {
                // Copied below from how WPILib does it. Compute the shortest path from current to desired rotation.
                var measurement = parameters.currentPose.getRotation().getRadians();
                setpointState = new TrapezoidProfile.State(parameters.currentPose.getRotation().getRadians(), angVelRadPerSec);
                startState = new TrapezoidProfile.State(parameters.currentPose.getRotation().getRadians(), angVelRadPerSec);
                goalState = new TrapezoidProfile.State(TargetDirection.getRadians(), 0);

                // Get error which is the smallest distance between goal and measurement
                double errorBound = (Math.PI - (-Math.PI)) / 2.0;
                double goalMinDistance =
                        MathUtil.inputModulus(goalState.position - measurement, -errorBound, errorBound);
                double setpointMinDistance =
                        MathUtil.inputModulus(goalState.position - measurement, -errorBound, errorBound);

                // Recompute the profile goal with the smallest error, thus giving the shortest path. The goal
                // may be outside the input range after this operation, but that's OK because the controller
                // will still go there and report an error of zero. In other words, the setpoint only needs to
                // be offset from the measurement by the input range modulus; they don't need to be equal.
                goalState.position = goalMinDistance + measurement;
                setpointState.position = setpointMinDistance + measurement;
                startTime = parameters.timestamp;
            }
            if (this.trapezoid) {
                setpointState = angleProfile.calculate(parameters.timestamp - startTime, startState, goalState);
                setpointAngleRad = setpointState.position;
            }
            lastGoal = TargetDirection;

            double toApplyX = VelocityX;
            double toApplyY = VelocityY;

            double rotationRate = HeadingController.calculate(parameters.currentPose.getRotation().getRadians(),
                    setpointAngleRad,
                    parameters.timestamp);

            if (USE_TRAPEZOID) {
                rotationRate += setpointState.velocity;
            }

            applySpeedsToModules(parameters, modulesToApply, rotationRate, toApplyX, toApplyY, Deadband, RotationalDeadband, DriveRequestType, SteerRequestType, FieldCentric);

            return StatusCode.OK;
        }
    }

    public class DesatFieldCentric extends SwerveRequest.FieldCentric {

        boolean FieldCentric = true;

        public DesatFieldCentric withFieldCentric(boolean fieldCentric) {
            this.FieldCentric = fieldCentric;
            return this;
        }

        public StatusCode apply(SwerveRequest.SwerveControlRequestParameters parameters, SwerveModule... modulesToApply) {

            double toApplyX = VelocityX;
            double toApplyY = VelocityY;
            double toApplyOmega = RotationalRate;
            applySpeedsToModules(parameters, modulesToApply, RotationalRate, toApplyX, toApplyY, Deadband, RotationalDeadband, DriveRequestType, SteerRequestType, FieldCentric);

            return StatusCode.OK;
        }
    }

    public static class MOIFinder implements SwerveRequest{
        TorqueCurrentFOC torqueControl = new TorqueCurrentFOC(20);
        @Override
        public StatusCode apply(SwerveControlRequestParameters parameters, SwerveModule... modulesToApply) {
            for(int i = 0; i < modulesToApply.length; i++) {
                modulesToApply[i].applyCharacterization(parameters.swervePositions[i].getAngle().plus(Rotation2d.fromDegrees(90)), torqueControl);
            }
            return StatusCode.OK;
        }
    }

}
