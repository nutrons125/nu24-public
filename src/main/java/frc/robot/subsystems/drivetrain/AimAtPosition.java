package frc.robot.subsystems.drivetrain;

import com.ctre.phoenix6.Utils;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.units.Distance;
import edu.wpi.first.units.Measure;
import edu.wpi.first.units.Velocity;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.FieldConstants;
import frc.robot.subsystems.superstructure.Superstructure;
import frc.robot.util.Util;

import java.util.function.Supplier;

import static edu.wpi.first.units.Units.MetersPerSecond;
import static frc.robot.subsystems.superstructure.MovingShotCommand.STATIONARY_VEL_TOLERANCE;

public class AimAtPosition extends Command {
    private CommandSwerveDrivetrain drivetrain;
    private Translation2d pos;
    private final Supplier<Measure<Velocity<Distance>>> xVelSupplier;
    private final Supplier<Measure<Velocity<Distance>>> yVelSupplier;


    public AimAtPosition(CommandSwerveDrivetrain drivetrain, Translation2d pos, Supplier<Measure<Velocity<Distance>>> xVel, Supplier<Measure<Velocity<Distance>>> yVel) {
        this.xVelSupplier = xVel;
        this.yVelSupplier = yVel;
        addRequirements(this.drivetrain = drivetrain);
    }

    @Override
    public void initialize() {
        this.pos = FieldConstants.getCannonShotTranslation();
    }

    @Override
    public void execute() {
        var angle = pos.minus(drivetrain.getState().Pose.getTranslation()).getAngle();
        drivetrain.faceAngleFieldCentric(angle, xVelSupplier.get(), yVelSupplier.get(), false);

        var fieldSpeeds = drivetrain.getCurrentRobotChassisSpeeds();
        var fieldVel = new Translation2d(fieldSpeeds.vxMetersPerSecond, fieldSpeeds.vyMetersPerSecond);

        if (Util.epsilonEquals(angle.getDegrees(), drivetrain.getState().Pose.getRotation().getDegrees(), 5) && (fieldVel.getNorm() < (STATIONARY_VEL_TOLERANCE * 2))) {
            Superstructure.getInstance().setAimed(true);
        }
    }

}
