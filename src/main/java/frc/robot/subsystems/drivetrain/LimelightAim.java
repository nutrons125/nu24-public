package frc.robot.subsystems.drivetrain;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.units.*;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.vision.LimelightHelpers;
import frc.robot.subsystems.vision.LimelightVision;

import static edu.wpi.first.units.Units.*;

public class LimelightAim extends Command {
    private CommandSwerveDrivetrain drivetrain;
    private LimelightVision vision;
    private PIDController aimController = new PIDController(8, 0, 0);
    public LimelightAim(CommandSwerveDrivetrain drivetrain, LimelightVision vision) {
        this.vision = vision;
        this.drivetrain = drivetrain;
        addRequirements(drivetrain, vision);
    }

    static Measure<Velocity<Distance>> xyVel = MetersPerSecond.of(0);
    MutableMeasure<Velocity<Angle>> thetaVel = RadiansPerSecond.of(0).mutableCopy();

    @Override
    public void initialize() {
        LimelightHelpers.setPipelineIndex("limelight", 1);
    }

    public void execute() {
            var thetaSpeed = aimController.calculate(-Math.toRadians(LimelightHelpers.getTX("limelight")));
            drivetrain.driveFieldCentric(
                    xyVel,
                    xyVel,
                    thetaVel.mut_replace(thetaSpeed, RadiansPerSecond)
            );
    }

    @Override
    public void end(boolean interupted) {
        LimelightHelpers.setPipelineIndex("limelight", 0);
    }
}
