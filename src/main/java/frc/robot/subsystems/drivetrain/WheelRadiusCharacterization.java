package frc.robot.subsystems.drivetrain;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.units.Angle;
import edu.wpi.first.units.Distance;
import edu.wpi.first.units.Measure;
import edu.wpi.first.units.MutableMeasure;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.generated.TunerConstants;

import static edu.wpi.first.units.Units.*;

public class WheelRadiusCharacterization extends Command {
    private CommandSwerveDrivetrain drivetrain;
    private MutableMeasure<Angle> lastMeasuredYaw = Degrees.of(0).mutableCopy();
    private double[] startWheelPositions;
    private MutableMeasure<Distance> effectiveRadius = Inches.of(0).mutableCopy();
    private double accumulatedYawRad = 0;
    static final double DRIVE_RADIUS_M = Units.inchesToMeters(Math.hypot(10.625, 10.625));
    static final Measure<Angle> MIN_ANGLE_FOR_DATA = Degrees.of(360);
    public WheelRadiusCharacterization(CommandSwerveDrivetrain drivetrain) {
        this.drivetrain = drivetrain;
        addRequirements(drivetrain);
    }

    @Override
    public void initialize() {
        lastMeasuredYaw = Degrees.of(0).mutableCopy();
        startWheelPositions = drivetrain.getDriveWheelPositionsRadians();
        accumulatedYawRad = 0;
    }

    @Override
    public void execute() {
        drivetrain.driveFieldCentric(MetersPerSecond.of(0), MetersPerSecond.of(0), DegreesPerSecond.of(90));
        var averageWheelPosition = 0.0;
        var meas = drivetrain.getDriveWheelPositionsRadians();
        for (int i = 0; i < 4; i++) {
            averageWheelPosition += Math.abs(meas[i] - startWheelPositions[i]);
        }
        averageWheelPosition /= 4.0;
        final var angleRad = drivetrain.getState().Pose.getRotation().getRadians();
        accumulatedYawRad += MathUtil.angleModulus(angleRad - lastMeasuredYaw.in(Radians));
        lastMeasuredYaw.mut_replace(angleRad, Radians);
        effectiveRadius.mut_replace((accumulatedYawRad * DRIVE_RADIUS_M) / averageWheelPosition, Meters);
        SmartDashboard.putNumber("radius", effectiveRadius.in(Inches));
    }

    @Override
    public boolean isFinished() {
        return accumulatedYawRad > MIN_ANGLE_FOR_DATA.times(4).in(Radians);
    }
    @Override
    public void end(boolean interrupted) {
        if (accumulatedYawRad <= MIN_ANGLE_FOR_DATA.in(Radians)) {
            System.out.println("Not enough data for characterization and this was written by 125");
        } else {
            System.out.println(
                    "Effective Wheel Radius (written by 125): "
                            + effectiveRadius.in(Inches)
                            + " inches");
        }
    }
}
