package frc.robot.subsystems.drivetrain;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.units.*;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.superstructure.Superstructure;
import frc.robot.subsystems.vision.NoteDetection;

import java.util.function.Supplier;

import static edu.wpi.first.units.Units.*;

public class LimelightNoteAim extends Command {
    private static final double distanceUntilStraight = 24;
    MutableMeasure<Velocity<Distance>> xVel = MetersPerSecond.of(0).mutableCopy();
    MutableMeasure<Velocity<Distance>> yVel = MetersPerSecond.of(0).mutableCopy();
    Measure<Velocity<Angle>> thetaVel = RadiansPerSecond.of(0).mutableCopy();
    private CommandSwerveDrivetrain drivetrain;
    private NoteDetection vision;
    private Superstructure superstructure;
    private PIDController xController = new PIDController(2.5, 0, 0);
    private PIDController yController = new PIDController(2.5, 0, 0);
    private final Supplier<Measure<Velocity<Distance>>> xVelSupplier;
    private final Supplier<Measure<Velocity<Distance>>> yVelSupplier;
    private Rotation2d heading;
    public LimelightNoteAim(CommandSwerveDrivetrain drivetrain, NoteDetection vision, Superstructure superstructure, Supplier<Measure<Velocity<Distance>>> xVelSup, Supplier<Measure<Velocity<Distance>>> yVelSup) {
        this.vision = vision;
        this.drivetrain = drivetrain;
        this.superstructure = superstructure;
        this.xVelSupplier = xVelSup;
        this.yVelSupplier = yVelSup;
        addRequirements(drivetrain, vision, superstructure);
    }

    @Override
    public void initialize() {
        heading = drivetrain.getState().Pose.getRotation();
    }

    @Override
    public void execute() {
        superstructure.setWantIntakeToShooter();
        var maybeNoteLocation = vision.notePose();
        var pose = drivetrain.getState().Pose;
        var speeds = ChassisSpeeds.fromFieldRelativeSpeeds(
                xVelSupplier.get().in(MetersPerSecond),
                yVelSupplier.get().in(MetersPerSecond),
                0,
                pose.getRotation());
        if (maybeNoteLocation.isPresent()) {
            superstructure.startLimelightIntaking();
            var noteTranslation = maybeNoteLocation.get();
            double x = noteTranslation.getX();
            double y = noteTranslation.getY();
            if (Math.abs(x) > distanceUntilStraight) {
                yController.setSetpoint(0);
                yVel = yVel.mut_replace(yController.calculate(y), InchesPerSecond);
            } else {
                yController.setSetpoint(0);
                yVel = yVel.mut_replace(yController.calculate(y), InchesPerSecond);
            }
            drivetrain.driveRobotCentric(
                    xVel.mut_replace(speeds.vxMetersPerSecond, MetersPerSecond),
                    yVel,
                    RadiansPerSecond.zero()
            );
        } else {
            superstructure.stopLimelightIntaking();
            drivetrain.driveFieldCentric(
                    xVelSupplier.get(),
                    yVelSupplier.get(),
                    RadiansPerSecond.zero()
            );
        }
    }
    @Override
    public boolean isFinished() {
        return superstructure.hasGamePiece();
    }
    @Override
    public void end(boolean isInterrupted) {
        superstructure.stopLimelightIntaking();
        superstructure.idle();
        superstructure.cancelAction();
    }
}
