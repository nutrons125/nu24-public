package frc.robot.subsystems.superstructure;

import edu.wpi.first.wpilibj2.command.Command;

public class RevAimOnly extends Command {
    protected RevAimOnly() {
        super();
    }

    @Override
    public void initialize() {
        super.initialize();
    }

    @Override
    public void execute() {
        super.execute();
    }

    @Override
    public void end(boolean interrupted) {
        super.end(interrupted);
    }

    @Override
    public boolean isFinished() {
        return super.isFinished();
    }
}
