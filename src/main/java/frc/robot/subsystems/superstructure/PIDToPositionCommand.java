package frc.robot.subsystems.superstructure;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.units.Distance;
import edu.wpi.first.units.MutableMeasure;
import edu.wpi.first.units.Velocity;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.drivetrain.CommandSwerveDrivetrain;

import static edu.wpi.first.units.Units.MetersPerSecond;

public class PIDToPositionCommand extends Command {
    //require drivetrive
    //take pose2d, constructor
//    pid controller on x y .
    //in exxecute drivefacingangle for angle
    private CommandSwerveDrivetrain drivetrain;
    private PIDController xController = new PIDController(2, 0, 0);
    private PIDController yController = new PIDController(2, 0, 0);
    private Pose2d targetPose;
    public PIDToPositionCommand(CommandSwerveDrivetrain drivetrain, Pose2d pose) {
        this.drivetrain = drivetrain;
        this.targetPose = pose;
        addRequirements(drivetrain);
    }

    private MutableMeasure<Velocity<Distance>> xVel = MetersPerSecond.of(0).mutableCopy();
    private MutableMeasure<Velocity<Distance>> yVel = MetersPerSecond.of(0).mutableCopy();

    @Override
    public void execute() {
        var curPose = drivetrain.getState().Pose;
        xController.setSetpoint(targetPose.getX());
        yController.setSetpoint(targetPose.getY());
        xVel.mut_replace(xController.calculate(curPose.getX()), MetersPerSecond);
        yVel.mut_replace(yController.calculate(curPose.getY()), MetersPerSecond);
        drivetrain.faceAngleFieldCentric(targetPose.getRotation(), xVel, yVel, false);
    }
}
