package frc.robot.subsystems.superstructure;

import com.ctre.phoenix6.BaseStatusSignal;
import com.ctre.phoenix6.StatusSignal;
import com.ctre.phoenix6.Utils;
import com.ctre.phoenix6.configs.*;
import com.ctre.phoenix6.controls.CoastOut;
import com.ctre.phoenix6.controls.DutyCycleOut;
import com.ctre.phoenix6.controls.MotionMagicVoltage;
import com.ctre.phoenix6.controls.StaticBrake;
import com.ctre.phoenix6.hardware.TalonFX;
import com.ctre.phoenix6.signals.InvertedValue;
import edu.wpi.first.math.system.plant.DCMotor;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.simulation.DCMotorSim;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.RobotMap;
import frc.robot.util.TalonUtil;


public class TriTake extends SubsystemBase {

    private static TriTake instance;
    public final StatusSignal<Double> maintIntakeDCSignal;
    public final StatusSignal<Double> triForceBottomDCSignal;
    public final StatusSignal<Double> triForceBottomSupplySignal;
    public final StatusSignal<Double> mainIntakeStatorCurrentSignal;
    public final StatusSignal<Double> mainIntakeSupplyCurrentSignal;
    public final StatusSignal<Double> triForceStatorCurrentSignal;
    public final StatusSignal<Double> triForceSupplyCurrentSignal;
    public final StatusSignal<Double> feederDCSignal;
    public final StatusSignal<Double> feederCurrentStatorSignal;
    public final DutyCycleOut maintIntake1Request;
    public final DutyCycleOut triForceBottomRequest;
    public final MotionMagicVoltage feederPositionRequest;// = new DutyCycleOut(0.1);
    public final DigitalInput frontIntakeBeamBreak = new DigitalInput(RobotMap.FRONT_INTAKE_BEAM_BREAK_ID);
    public final DigitalInput rearIntakeBeamBreak = new DigitalInput(RobotMap.REAR_INTAKE_BEAM_BREAK_ID);
    public DutyCycleOut feederDutyCycleRequest;// = new DutyCycleOut(0.1);
    public ShuffleboardTab triTakeTab = Shuffleboard.getTab("TriTake");
    TalonFX mainIntakeMotor;
    TalonFX triForceBottomMotor;
    TalonFX feederMotor;
    DCMotorSim feederSim = new DCMotorSim(DCMotor.getKrakenX60(1), 1, 0.001);
    private boolean openLoop = true;
    private boolean front = false;
    private boolean back = false;
    private CoastOut coastOut = new CoastOut();
    private StaticBrake staticBrake = new StaticBrake();
    private boolean isFrontSimBeamBroken;
    private boolean isRearSimBeamBroken;
    private boolean isShooterBeamBroken;

    private TriTake() {
        double supplyLimit = 60;
        double statorLimit = 80;//was 100
        if (Utils.isSimulation()) {
            supplyLimit = 250;
            statorLimit = 300;
        }
        var mainOutput = new MotorOutputConfigs()
                .withInverted(InvertedValue.CounterClockwise_Positive);
        var triForceOutput = new MotorOutputConfigs()
                .withInverted(InvertedValue.CounterClockwise_Positive);
        var feederOutput = new MotorOutputConfigs()
                .withInverted(InvertedValue.CounterClockwise_Positive);
        var feederMotionMagic = new MotionMagicConfigs()
                .withMotionMagicCruiseVelocity(96.6)
                .withMotionMagicAcceleration(96.6 * 4) // Reach max vel in 1 second
                .withMotionMagicJerk(96.6 * 4 * 4); //todo same as shooter?
        var ramp = new OpenLoopRampsConfigs()
                .withDutyCycleOpenLoopRampPeriod(0.01);
        var curLimits = new CurrentLimitsConfigs()
                .withSupplyCurrentLimitEnable(true)
                .withSupplyCurrentLimit(supplyLimit)
                .withStatorCurrentLimitEnable(true)
                .withStatorCurrentLimit(statorLimit);
        var feedercurLimits = new CurrentLimitsConfigs()
                .withSupplyCurrentLimitEnable(true)
                .withSupplyCurrentLimit(supplyLimit)
                .withStatorCurrentLimitEnable(true)
                .withStatorCurrentLimit(90);
        var pidConfig = new Slot0Configs()
                .withKP(2)
                .withKV(0.11);

        mainIntakeMotor = new TalonFX(RobotMap.MAIN_INTAKE_MOTOR_CANID, "drivetrain");
        var res = TalonUtil.applyAndCheckConfiguration(mainIntakeMotor, new TalonFXConfiguration()
                .withMotorOutput(mainOutput)
                .withCurrentLimits(curLimits)
                .withOpenLoopRamps(ramp), 2);
        triTakeTab.addBoolean("Main Intake Init", () -> res);
        triForceBottomMotor = new TalonFX(RobotMap.TRIFORCE_BOTTOM_MOTOR_CANID, "drivetrain");
        TalonUtil.applyAndCheckConfiguration(triForceBottomMotor, new TalonFXConfiguration()
                .withMotorOutput(triForceOutput)
                .withCurrentLimits(feedercurLimits)
                .withOpenLoopRamps(ramp), 2);

        feederMotor = new TalonFX(RobotMap.FEEDER_MOTOR_CANID, "drivetrain");
        TalonUtil.applyAndCheckConfiguration(feederMotor, new TalonFXConfiguration()
                .withMotorOutput(feederOutput)
                .withMotionMagic(feederMotionMagic)
                .withSlot0(pidConfig)
                .withCurrentLimits(feedercurLimits)
                .withOpenLoopRamps(ramp), 2);

        maintIntakeDCSignal = mainIntakeMotor.getDutyCycle();
        triForceBottomDCSignal = triForceBottomMotor.getDutyCycle();
        triForceBottomSupplySignal = triForceBottomMotor.getSupplyCurrent();
        feederDCSignal = feederMotor.getDutyCycle();
        feederCurrentStatorSignal = feederMotor.getStatorCurrent();
        mainIntakeStatorCurrentSignal = mainIntakeMotor.getStatorCurrent();
        mainIntakeSupplyCurrentSignal = mainIntakeMotor.getSupplyCurrent();
        triForceStatorCurrentSignal = mainIntakeMotor.getStatorCurrent();
        triForceSupplyCurrentSignal = mainIntakeMotor.getSupplyCurrent();

        BaseStatusSignal.setUpdateFrequencyForAll(
                100,
                maintIntakeDCSignal,
                triForceBottomDCSignal,
                triForceSupplyCurrentSignal,
                triForceBottomSupplySignal,
                mainIntakeStatorCurrentSignal,
                feederCurrentStatorSignal,
                mainIntakeSupplyCurrentSignal,
                triForceStatorCurrentSignal
        );
        mainIntakeMotor.optimizeBusUtilization();
        triForceBottomMotor.optimizeBusUtilization();

        maintIntake1Request = new DutyCycleOut(0);
        triForceBottomRequest = new DutyCycleOut(0);
        feederDutyCycleRequest = new DutyCycleOut(0);
        feederPositionRequest = new MotionMagicVoltage(0);// todo 0 or current position
        triTakeTab.addDouble("Main Intake supply current", mainIntakeSupplyCurrentSignal::getValue);
        triTakeTab.addDouble("Main Intake Stator current", mainIntakeStatorCurrentSignal::getValue);
        triTakeTab.addDouble("Triforce Bottom supply", triForceBottomSupplySignal::getValue);
        triTakeTab.addDouble("Feeder Stator", feederCurrentStatorSignal::getValue);
        triTakeTab.addBoolean("Front Beam Break", this::getFrontIntakeBeamBreak);
        triTakeTab.addBoolean("Rear Beam Break", this::getRearIntakeBeamBreak);
    }

    public static TriTake getInstance() {
        if (instance == null) {
            instance = new TriTake();
        }
        return instance;
    }

    @Override
    public void periodic() {
        BaseStatusSignal.refreshAll(
                maintIntakeDCSignal,
                triForceBottomDCSignal,
                triForceSupplyCurrentSignal,
                triForceBottomSupplySignal,
                mainIntakeStatorCurrentSignal,
                mainIntakeSupplyCurrentSignal,
                feederCurrentStatorSignal,
                triForceStatorCurrentSignal,
                triForceSupplyCurrentSignal
        );

        if (!openLoop) {
            feederMotor.setControl(feederPositionRequest);
        } else {
            feederMotor.setControl(feederDutyCycleRequest);
        }

        front = frontIntakeBeamBreak.get();
        back = rearIntakeBeamBreak.get();
    }

    public void idle() {
        setMainIntakeDutyCycle(0);
        setFeederDutyCycle(0);
        setTriForceBottomDutyCycle(0);
    }

    public void intakingOrLoadingRear() {
        // towards shooter?
        setFeederDutyCycle(1.0);
        setTriForceBottomDutyCycle(1);
        setMainIntakeDutyCycle(1);
    }

    public void shootTaking() {
        setFeederDutyCycle(1.0);
        setTriForceBottomDutyCycle(1);
        setMainIntakeDutyCycle(1);
    }

    public void intakingFloorToTramp() {
        setFeederDutyCycle(0.5);
        setTriForceBottomDutyCycle(1);
        setMainIntakeDutyCycle(1);
    }

    public void fromTrampToShooter() {
        setFeederDutyCycle(0.5);
        setTriForceBottomDutyCycle(1);
        setMainIntakeDutyCycle(-1);
    }

    public void fromShooterToTramp() {
        setFeederDutyCycle(-0.3);
        setTriForceBottomDutyCycle(-1);
        setMainIntakeDutyCycle(1);
    }

    public void loadingFront() {
        setFeederDutyCycle(-0.5);
    }

    public void loaded() {
        idle();
        //has gamepiece
    }

    public double getFeederPosition() {
        return feederMotor.getPosition().getValue();
    }

    public void setMainIntakeDutyCycle(double speed) {
        maintIntake1Request.Output = speed;
        mainIntakeMotor.setControl(maintIntake1Request);
    }

    public void setFeederDutyCycle(double speed) {
        openLoop = true;
        feederDutyCycleRequest.Output = speed;
//        feederMotor.setControl(feederDutyCycleRequest);
    }

    public void setTriForceBottomDutyCycle(double speed) {
        triForceBottomRequest.Output = speed;
        triForceBottomMotor.setControl(triForceBottomRequest);
    }

    public void coastMotor() {
        triForceBottomMotor.setControl(coastOut);
        feederMotor.setControl(coastOut);
        mainIntakeMotor.setControl(coastOut);
    }

    public void brakeMotor() {
        triForceBottomMotor.setControl(staticBrake);
        feederMotor.setControl(staticBrake);
        mainIntakeMotor.setControl(staticBrake);
    }

    public void setFrontSimBeamBroken(boolean poop) {
        isFrontSimBeamBroken = poop;
    }

    public void setRearSimBeamBroken(boolean poop) {
        isRearSimBeamBroken = poop;
    }

    public boolean getFrontIntakeBeamBreak() {
        return front;
    }

    public boolean getFrontBeamBreakSlow() {
        return front;
    }

    public boolean getRearIntakeBeamBreak() {
        return back;
    }

    public void setShooterSimBeamBroken(boolean poop) {
        isShooterBeamBroken = poop;
    }

    @Override
    public void simulationPeriodic() {
        var simState = feederMotor.getSimState();
        simState.setSupplyVoltage(12);
        simState.setRawRotorPosition(feederSim.getAngularPositionRotations());
        feederSim.setInputVoltage(simState.getMotorVoltage());
        feederSim.update(0.02);
    }


}
