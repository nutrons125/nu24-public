package frc.robot.subsystems.superstructure;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.units.Angle;
import edu.wpi.first.units.Measure;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.FieldConstants;
import frc.robot.subsystems.drivetrain.CommandSwerveDrivetrain;

import java.util.function.BooleanSupplier;
import java.util.function.DoubleSupplier;

import static edu.wpi.first.units.Units.*;
import static frc.robot.subsystems.superstructure.MovingShotCommand.STATIONARY_VEL_TOLERANCE;

public class FixedShotCommand extends Command {
    BooleanSupplier shoot;
    Measure<Angle> hoodAngle = Degrees.of(21);
    Measure<Angle> cannonHoodAngle = Degrees.of(45);//was 55
    private static final double SHOOTER_BARREL_LENGTH_M = 0.381;

    private CommandSwerveDrivetrain drivetrain;
    private boolean slowShoot;
    private ShotType shotType;
    private Superstructure superstructure;
    private boolean autonomous;

    public FixedShotCommand(CommandSwerveDrivetrain drivetrain, Superstructure superstructure, BooleanSupplier shouldShoot, ShotType shotType) {
        this.drivetrain = drivetrain;
        this.superstructure = superstructure;
        shoot = shouldShoot;
        this.shotType = shotType;
        addRequirements(superstructure);
    }

    public FixedShotCommand(CommandSwerveDrivetrain drivetrain, Superstructure superstructure, DoubleSupplier xSup, DoubleSupplier ySup, BooleanSupplier shouldShoot, ShotType shotType) {
        this.drivetrain = drivetrain;
        this.superstructure = superstructure;
        shoot = shouldShoot;
        this.shotType = shotType;

        addRequirements(superstructure);
    }

    @Override
    public void initialize() {
        var ang = SmartDashboard.getNumber("Hood Angle", 21);
        if (ang != hoodAngle.in(Degrees)) {
            System.out.println("New angle is: " + ang);
            hoodAngle = Degrees.of(ang);
        } else {
            System.out.println("Unchanged angle :(");
        }
        superstructure.setAimed(false);
    }

    @Override
    public void execute() {
        // Make a swerverequest for the drive
        // Find velocity from DT
        // RUn shooter calculations
        double distance = drivetrain.getState().Pose.getTranslation().getDistance(FieldConstants.getCannonShotTranslation());
        // got the 1007.92 by finding physics equation and then pluggin in current rpm and distance we used in matches to find a conversion
        double yInitial = SHOOTER_BARREL_LENGTH_M * Math.sin(cannonHoodAngle.in(Radians));

        double cannonRPM = 900 * Math.sqrt(distance/Math.sin(cannonHoodAngle.in(Radians)));
        cannonRPM = MathUtil.clamp(cannonRPM, 1000, 5000);
        SmartDashboard.putNumber("cannon shot distance", distance);
        SmartDashboard.putNumber("cannon shot RPM", cannonRPM);
        Shooter.BasicShotData shotData = switch (shotType) {
            case SLOW -> new Shooter.BasicShotData(hoodAngle, RPM.of(2000), RPM.of(2000));
            case NORMAL -> new Shooter.BasicShotData(hoodAngle, MovingShotCommand.LEFT_RPM_SETPOINT, MovingShotCommand.RIGHT_RPM_SETPOINT);
            case CANNON -> new Shooter.BasicShotData(cannonHoodAngle, RPM.of(cannonRPM * 0.7), RPM.of(cannonRPM));
        };
//        drivetrain.stop();
        // Spin up shooter

            superstructure.setWantShoot();
            superstructure.setShotData(shotData);
            if(shotType.equals(ShotType.SLOW) || shotType.equals(ShotType.CANNON)) {
                superstructure.fasterShooting();
            }
        // If shooter ready and aimed, then shoot
        if (shoot.getAsBoolean()) {
            superstructure.setAimed(true);
        }
    }

    @Override
    public boolean isFinished() {
        if (DriverStation.isAutonomous()) {
            return !superstructure.hasGamePiece();
        }
        return false;
    }

    @Override
    public void end(boolean interrupted) {
        superstructure.cancelAction();
    }

    public enum ShotType {
        SLOW,
        NORMAL,
        CANNON
    }
}
