package frc.robot.subsystems.superstructure;

import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.FieldConstants;
import frc.robot.subsystems.drivetrain.CommandSwerveDrivetrain;

import static edu.wpi.first.units.Units.Degrees;
import static edu.wpi.first.units.Units.RPM;
import static frc.robot.subsystems.superstructure.MovingShotCommand.*;

public class ShootTakeAimedCommand extends Command {
    private final Superstructure superstructure;
    private final CommandSwerveDrivetrain drivetrain;
    private final Shooter shooter;
    private final Hood hood;
    boolean shouldFeed = false;

    public ShootTakeAimedCommand(CommandSwerveDrivetrain drivetrain, Superstructure superstructure) {
        this.drivetrain = drivetrain;
        this.shooter = Shooter.getInstance();
        this.hood = Hood.getInstance();
        addRequirements(this.superstructure = superstructure);
    }

    @Override
    public void initialize() {
        drivetrain.setAutonomousShooting(true);
        shouldFeed = false;
    }

    @Override
    public void execute() {
        // Get speeds and velocity
        var relativeSpeeds = drivetrain.getCurrentRobotChassisSpeeds();
        var fieldSpeeds = ChassisSpeeds.fromFieldRelativeSpeeds(relativeSpeeds, drivetrain.getState().Pose.getRotation().unaryMinus());
        var fieldVel = new Translation2d(fieldSpeeds.vxMetersPerSecond, fieldSpeeds.vyMetersPerSecond);


        // Calculate shot data and set shot angle
        Shooter.MagicShotCalculator.ShotData data = Shooter.MagicShotCalculator.calculate(FieldConstants.getSpeakerPosition(), drivetrain.getState().Pose.getTranslation(), fieldVel);
        drivetrain.setAutonomousShotHeading(data.goalHeading());
        var angleDeg = DIST_TO_HOOD_MAP.get(Units.metersToFeet(data.effectiveRobotToSpeakerDist()));
        Shooter.BasicShotData shotData = new Shooter.BasicShotData(
                Degrees.of(angleDeg),
                LEFT_RPM_SETPOINT,
                RIGHT_RPM_SETPOINT);

        shouldFeed = shouldFeed ||
                (shooter.rightAtGoal(shotData.rightRPM(), RPM.of(1000)) &&
                        shooter.leftAtGoal(shotData.leftRPM(), RPM.of(1000)) &&
                        Math.abs(hood.getPosition().getDegrees() - shotData.angle().in(Degrees)) < 4);
        // Shoot
        superstructure.shootTake(shotData, true, shouldFeed);
        SmartDashboard.putBoolean("shouldFeed", shouldFeed);
    }

    @Override
    public void end(boolean interrupted) {
        drivetrain.setAutonomousShooting(false);
        superstructure.cancelAction();
        shouldFeed = false;
    }
}
