package frc.robot.subsystems.superstructure;

import com.ctre.phoenix6.Utils;
import edu.wpi.first.units.Angle;
import edu.wpi.first.units.Measure;
import edu.wpi.first.units.MutableMeasure;
import edu.wpi.first.units.Velocity;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.RobotState;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.subsystems.drivetrain.LimelightAim;
import frc.robot.subsystems.vision.LimelightHelpers;
import frc.robot.util.Util;

import static edu.wpi.first.units.Units.*;

enum State { // i made this cause i think we'll need it but idk where to use it just yet
    IDLE, PREPPING_CLIMB, PRE_RAISE_CLIMB, HOOKING_ON, CLIMBING, SCORING_IN_TRAP, LOWERING_CLIMB, RAISING_HOOD, SCORING_TO_AMP, INTAKING_TO_TRAMP, TRAMP_UPTAKE, INTAKING_TO_TRAMP_FROM_SHOOTER, INTAKING_TO_SHOOTER_FROM_TRAMP, PAUSING_B4_BACKING_UP, STARTING_BACKUP, BACKING_UP, PREPPING_SHOT, SHOOTING, INTAKING_TO_SHOOTER, LOADED, MANUAL, PREPPING_AMP_SHOT, RAISING_ELEVATOR, LOWERING_ELEVATOR, SMALL_UNCLIMB, DEMO_MODE, DEMO_BACKUP, APRILTAG_TRACKING
}

enum NoteLocation {
    NONE, SHOOTER, TRAMP
}

enum Action {
    NONE, INTAKE_TO_SHOOTER, INTAKE_TO_TRAMP, TRAMP_TO_SHOOTER, SHOOTER_TO_TRAMP, SHOOT_TO_SPEAKER, SCORE_IN_AMP, PREP_SHUFFLE, CLIMB, REVERSE_CLIMBING
}

public class Superstructure extends SubsystemBase {
    private static Superstructure instance;
    private final TriTake tritake;
    private final Shooter shooter;
    private final Hood hood;
    private final Tramp tramp;
    private final Climber climber;
    private final LEDStrip led;
    public ShuffleboardTab superstructureTab = Shuffleboard.getTab("superstructure");
    public double SERVO_UP = 0.35;
    public double SERVO_DOWN = 0.725;
    boolean intakeWithTime = false;
    private State state = State.IDLE;
    private Action desiredAction = Action.NONE;
    private Shooter.BasicShotData shotData = new Shooter.BasicShotData(Degrees.of(0), RPM.of(0), RPM.of(0));
    private boolean rightAtSpeed;
    private boolean leftAtSpeed;
    private boolean hoodAtAngle;
    private boolean sure;
    private NoteLocation noteLocation = NoteLocation.NONE;
    private double timeStartedAction = 0;
    private boolean aimed = false;
    private boolean confirmClimb = false;
    private boolean abortClimb = false;
    private boolean smallUnclimb = false;
    private boolean shouldUptake = true;
    private boolean isStaged = false;
    private boolean overrideLocation = false;
    private boolean startHalfClimb = false;
    private boolean preRaise = false;
    private double aimTime = 0;
    private boolean limelightIntaking = false;
    private boolean fasterShooting = false;
    private MutableMeasure<Angle> demoModePosition = Degrees.of(0).mutableCopy();
    private boolean inDemoMode = false;
    private boolean demoAimed = false;
    private boolean apriltagTracking = false;
    public void setDemoAimed(boolean demoAimed) {
        this.demoAimed = demoAimed;
    }
    public void setDemoMode(boolean inDemoMode) {
        this.inDemoMode = inDemoMode;
    }
    public void setDemoModePosition(Measure<Angle> demoAngle) {
        this.demoModePosition.mut_replace(demoAngle);
    }
    public void setApriltagTracking(boolean apriltagTracking) {
        this.apriltagTracking = apriltagTracking;
    }

    private Superstructure() {
        hood = Hood.getInstance();
        tramp = Tramp.getInstance();
        climber = Climber.getInstance();
        led = LEDStrip.getInstance();
        superstructureTab.addString("state", () -> state.toString());
        superstructureTab.addBoolean("Right wheel at speed", () -> rightAtSpeed);
        superstructureTab.addBoolean("Left wheel at speed", () -> leftAtSpeed);
        superstructureTab.addBoolean("Hood at angle", () -> hoodAtAngle);
        superstructureTab.addBoolean("Sure???", () -> sure);
        superstructureTab.addString("Desired Action", () -> desiredAction.toString());
        superstructureTab.addString("Note location", () -> noteLocation.toString());
        superstructureTab.addBoolean("Tramp Beam Break", tramp::getTrampBeamBreak);
        superstructureTab.addDouble("Aim Time", () -> aimTime);
        shooter = Shooter.getInstance();
        tritake = TriTake.getInstance();
    }

    public static Superstructure getInstance() {
        if (instance == null) {
            instance = new Superstructure();
        }
        return instance;
    }

    public void setConfirmClimb(boolean confirmClimb) {
        this.confirmClimb = confirmClimb;
        abortClimb = false;
    }

    public void manualShooting(Measure<Angle> ang, Measure<Velocity<Angle>> leftvel, Measure<Velocity<Angle>> rightvel) {
        state = State.MANUAL;
        hood.setDesiredAngle(ang);
        shooter.setLeftCL(leftvel);
        shooter.setRightCL(rightvel);
    }

    public void setTrampServoPos(double pos) {
        tramp.setTrapServo(pos);
    }


    public void manualShot() {
        state = State.MANUAL;
        shooter.setDutyCycle(1.0, 1.0);
    }


    public void idle() {
        tritake.setMainIntakeDutyCycle(0);
        shooter.setDutyCycle(0, 0);
        tramp.setTrampMiddleDutyCycle(0);
        tritake.setFeederDutyCycle(0);
        tritake.setTriForceBottomDutyCycle(0);
        hood.setDesiredAngle(Degrees.of(0));
        state = State.IDLE;
    }

    public boolean isIdle() {
        return (state == State.IDLE);
    }

    public void resetState() { // dunno if i can do this in idle, dont wanna mess up other things
        noteLocation = NoteLocation.NONE;
        desiredAction = Action.NONE;
        state = State.IDLE;
    }

    public void dumbIdle() {
        tritake.setMainIntakeDutyCycle(0);
        shooter.setDutyCycle(0, 0);
        tramp.setTrampMiddleDutyCycle(0);
        tramp.setTrampTopDutyCycle(0);
        tritake.setFeederDutyCycle(0);
        tritake.setTriForceBottomDutyCycle(0);
        hood.setDesiredAngle(Degrees.of(0));
    }


    public void shootIntoSpeaker() {
        tritake.setMainIntakeDutyCycle(0);
        tramp.setTrampMiddleDutyCycle(0);
        tritake.setFeederDutyCycle(0.5);
        tritake.setTriForceBottomDutyCycle(0);
    }

    public void setWantIntakeToShooter() {
        desiredAction = Action.INTAKE_TO_SHOOTER;
    }

    public void setPreRaiseClimb() {
        desiredAction = Action.CLIMB;
        preRaise = true;
        abortClimb = false;
    }

    public void setWantClimb() {
        desiredAction = Action.CLIMB;
        abortClimb = false;
        startHalfClimb = true;
        //drive!!!!
    }

    public void abortClimb() {
        abortClimb = true;
        desiredAction = Action.NONE;
        preRaise = false;
        startHalfClimb = false;
        confirmClimb = false;
    }

    public void setWantShootToTramp() {
        desiredAction = Action.SCORE_IN_AMP;
    }

    public void setWantShoot() {
        desiredAction = Action.SHOOT_TO_SPEAKER;
        overrideLocation = false;
    }

    public void cancelAction() {
        desiredAction = Action.NONE;
        state = State.IDLE;
    }

    public void clearNode() {
        noteLocation = NoteLocation.NONE;
    }

    public void cancelAndClear() {
        cancelAction();
        clearNode();
    }

    public void fasterShooting() {
        fasterShooting = true;
    }

    public void cancelDesirdAction() {
        desiredAction = Action.NONE;
    }

    public void shooterToTramp() {
        desiredAction = Action.SHOOTER_TO_TRAMP;
        shouldUptake = false;
    }

    public void setTriforceSpeed(double speed) {
        state = State.MANUAL;
        tritake.setTriForceBottomDutyCycle(speed);
    }

    public State getState() {
        return state;
    }

    public void dumbFloorToTramp() {
        state = State.MANUAL;
        tritake.intakingFloorToTramp();
        tramp.setTrampMiddleDutyCycle(1);
        shooter.setDutyCycle(-0.7, -0.7);
    }

    public boolean hasGamePiece() {
        return noteLocation != NoteLocation.NONE;
    }

    public void setGamePiece(NoteLocation note) {
        noteLocation = note;
    }

    public void spinShooter(double speed) {
        state = State.MANUAL;
        shooter.setDutyCycle(speed, speed);
    }

    public void startLimelightIntaking() {
        limelightIntaking = true;
    }

    public void stopLimelightIntaking() {
        limelightIntaking = false;
    }

    public void startAuto() {
        noteLocation = NoteLocation.SHOOTER;
        state = State.IDLE;
        intakeWithTime = false;
    }
    // hood, shooter and feeder always spinning, if we're intaking also run bottom triforce and main intake

    public void shootTake(Shooter.BasicShotData shotData, boolean runIntake) {
        state = State.MANUAL;
        shootWithParams(shotData);
        if (runIntake) {
            tritake.shootTaking();
            tramp.setTrampMiddleDutyCycle(-1); // Keep notes down
        } else {
            tritake.setFeederDutyCycle(1.0);
            tramp.setTrampMiddleDutyCycle(0);
            tritake.setTriForceBottomDutyCycle(0);
            tritake.setMainIntakeDutyCycle(0);
        }
    }

    public void shootTake(Shooter.BasicShotData shotData, boolean runIntake, boolean feedShooter) {
        state = State.MANUAL;
        shootWithParams(shotData);
//        if (runIntake) {
//            if (feedShooter) {
//                tritake.shootTaking();
//            } else {
//                tritake.setFeederDutyCycle(0);
//                tritake.setTriForceBottomDutyCycle(0);
//                tritake.setMainIntakeDutyCycle(0);
//            }
//            tramp.setTrampMiddleDutyCycle(-1); // Keep notes down
//        } else {
//            tritake.setFeederDutyCycle(1.0);
//            tramp.setTrampMiddleDutyCycle(0);
//            tritake.setTriForceBottomDutyCycle(0);
//            tritake.setMainIntakeDutyCycle(0);
//        }


        if (feedShooter) {
            tritake.setFeederDutyCycle(1.0);
            tritake.setTriForceBottomDutyCycle(1);
            tritake.setMainIntakeDutyCycle(1);
            tramp.setTrampMiddleDutyCycle(-1);
        } else {
            tritake.setFeederDutyCycle(0);
            tritake.setTriForceBottomDutyCycle(0);
            tritake.setMainIntakeDutyCycle(0);
            tramp.setTrampMiddleDutyCycle(0);
        }
    }


    public void outtake() {
        state = State.MANUAL;
        tritake.setFeederDutyCycle(-0.7);
        tritake.setTriForceBottomDutyCycle(-0.7);
        tritake.setMainIntakeDutyCycle(-0.7);
        tramp.setTrampMiddleDutyCycle(-1);
        tramp.setTrampTopDutyCycle(-1);
        hood.setDesiredAngle(Degrees.of(45));

    }


    public void outtakeWithouthood() {
        state = State.MANUAL;
        tritake.setFeederDutyCycle(-0.7);
        tritake.setTriForceBottomDutyCycle(-0.7);
        tritake.setMainIntakeDutyCycle(-0.7);
        tramp.setTrampMiddleDutyCycle(-1);
        tramp.setTrampTopDutyCycle(-1);
    }


    public void stopAuto() {
        intakeWithTime = false;
    }

    @Override
    public void periodic() {
        if (RobotState.isDisabled()) {
            return;
        }
        switch (state) {
            case MANUAL -> {
                //victors mom
            }
            case IDLE -> {
                shooter.setDutyCycle(0, 0);
                hood.setDesiredAngle(Degrees.of(0));
                tramp.setTrampMiddleDutyCycle(0);
                tramp.setTrampTopDutyCycle(0);
                tritake.idle();
                climber.climbDown();
                tramp.setTrampExtensionMotor(Inches.of(0));
                led.resetProgress();
                fasterShooting = false;

                if (noteLocation == NoteLocation.NONE) {
                    led.setAllRed();
                } else if (noteLocation == NoteLocation.SHOOTER) {
                    led.setAllGreen();
                } else {
                    led.setAllBlue();
                }

                if (desiredAction == Action.INTAKE_TO_SHOOTER && noteLocation != NoteLocation.SHOOTER) {
                    state = State.INTAKING_TO_SHOOTER;
                } else if (desiredAction == Action.SHOOT_TO_SPEAKER) {
                    if (noteLocation == NoteLocation.TRAMP && !overrideLocation) {
                        state = State.INTAKING_TO_SHOOTER_FROM_TRAMP;
                    } else {
                        state = State.STARTING_BACKUP;
                    }
                } else if (desiredAction == Action.INTAKE_TO_TRAMP) {
                    if (noteLocation == NoteLocation.NONE) {
//                        state = State.INTAKING_TO_TRAMP;
//                        timeStartedAction = Utils.getCurrentTimeSeconds();
                    } else if (noteLocation == NoteLocation.SHOOTER) {
                        state = State.INTAKING_TO_TRAMP_FROM_SHOOTER;
                    }
                } else if (desiredAction == Action.SHOOTER_TO_TRAMP) {
                    state = State.INTAKING_TO_TRAMP_FROM_SHOOTER;
                    timeStartedAction = Utils.getCurrentTimeSeconds();
                } else if (desiredAction == Action.SCORE_IN_AMP) {
                    timeStartedAction = Utils.getCurrentTimeSeconds();
                    if (noteLocation == NoteLocation.SHOOTER) {
                        state = State.INTAKING_TO_TRAMP_FROM_SHOOTER;
                        shouldUptake = true;
                    } else {
                        if (isStaged) {
                            state = State.TRAMP_UPTAKE;
                            isStaged = false;
                        } else {
                            state = State.PREPPING_AMP_SHOT;
                        }
                    }
                } else if (desiredAction == Action.CLIMB) {
                    if (noteLocation == NoteLocation.SHOOTER) {
                        state = State.INTAKING_TO_TRAMP_FROM_SHOOTER;
                        shouldUptake = false;
                    } else {
                        if (isStaged) {
                            state = State.TRAMP_UPTAKE;
                            tramp.recordTopRolerPos();
                            isStaged = false;
                        } else if (preRaise) {
                            state = State.PRE_RAISE_CLIMB;
                        }
                    }
                } else if (desiredAction == Action.REVERSE_CLIMBING) {
                    state = State.LOWERING_CLIMB;
                }
                if (inDemoMode) {
                    state = State.STARTING_BACKUP;
                }
            }
            case INTAKING_TO_SHOOTER -> {
                if (limelightIntaking) {
                    led.pinkBlinkAnimation();
                } else {
                    led.redBlinkAnimation();
                }
                if ((tritake.getFrontIntakeBeamBreak() && tritake.getRearIntakeBeamBreak()) || (intakeWithTime && Utils.getCurrentTimeSeconds() - timeStartedAction > 3.125)) {
                    state = State.LOADED;
                } else {
                    tritake.intakingOrLoadingRear();
                    tramp.setTrampMiddleDutyCycle(-1);
                    shooter.setDutyCycle(-0.7, -0.7);
                }
            }
            case INTAKING_TO_TRAMP -> {
                tritake.intakingFloorToTramp();
                tramp.setTrampMiddleDutyCycle(1);
                shooter.setDutyCycle(-0.7, -0.7);

                if (tramp.getTrampBeamBreak()) {
                    if (shouldUptake) {
                        tramp.recordTopRolerPos();
                        timeStartedAction = Utils.getCurrentTimeSeconds();
                        state = State.TRAMP_UPTAKE;
                    } else {
                        noteLocation = NoteLocation.TRAMP;
                        state = State.IDLE;
                        isStaged = true;
                    }
                }
            }

            case TRAMP_UPTAKE -> {
                tramp.setTrampTopDutyCycle(0.45);
                tramp.setTrampMiddleDutyCycle(0.5);
                var timeElapsed = Utils.getCurrentTimeSeconds() - timeStartedAction;
                if (!tramp.getTrampBeamBreak() && timeElapsed > 0.05) {
                    noteLocation = NoteLocation.TRAMP;
                    if (desiredAction == Action.INTAKE_TO_TRAMP || desiredAction == Action.SHOOTER_TO_TRAMP) {
                        desiredAction = Action.NONE;
                    }
                    state = State.IDLE;
                    led.off();
                }
            }
            case INTAKING_TO_TRAMP_FROM_SHOOTER -> {
                led.blueBlinkAnimation();
                tritake.fromShooterToTramp();
                tramp.setTrampTopDutyCycle(0.25);
                tramp.setTrampMiddleDutyCycle(1);
                shooter.setDutyCycle(-0.7, -0.7);
                if (tramp.getTrampBeamBreak()) {
                    if (shouldUptake) {
                        state = State.TRAMP_UPTAKE;
                        tramp.recordTopRolerPos();
                        isStaged = false;
                    } else {
                        noteLocation = NoteLocation.TRAMP;
                        state = State.IDLE;
                        isStaged = true;
                        desiredAction = Action.NONE;
                    }
                }
            }
            case INTAKING_TO_SHOOTER_FROM_TRAMP -> {
                if (tritake.getRearIntakeBeamBreak() && tritake.getFrontIntakeBeamBreak()) {
                    state = State.LOADED;
                }
                tritake.fromTrampToShooter();
                tramp.setTrampMiddleDutyCycle(-1);
                shooter.setDutyCycle(0, 0);

            }
            case LOADED -> {
                noteLocation = NoteLocation.SHOOTER;
                state = State.IDLE;
                if (desiredAction == Action.INTAKE_TO_SHOOTER || desiredAction == Action.TRAMP_TO_SHOOTER) {
                    desiredAction = Action.NONE;
                }
            }
            case PAUSING_B4_BACKING_UP -> {
                timeStartedAction = Utils.getCurrentTimeSeconds();
                state = State.STARTING_BACKUP;
            }
            case STARTING_BACKUP -> {
                if (!tritake.getRearIntakeBeamBreak() && !tritake.getFrontIntakeBeamBreak()) {
                    if (inDemoMode) {
                        state = State.DEMO_MODE;
                    }
                    else {
                        state = State.PREPPING_SHOT;
                    }
                }
//                feederPos = tritake.getFeederPosition();
//                tritake.setDesiredFeederPosition(feederPos - 1); todo
                if (Utils.getCurrentTimeSeconds() - timeStartedAction > 0.2) {
                    tritake.setFeederDutyCycle(-0.2);
                    shooter.setDutyCycle(-0.1, -0.1);
                    tramp.setTrampMiddleDutyCycle(0.2);
                    tritake.setTriForceBottomDutyCycle(-0.3);
                    timeStartedAction = Utils.getCurrentTimeSeconds();
                    state = State.BACKING_UP;
                }
            }
            case BACKING_UP -> {
                if ((!tritake.getFrontBeamBreakSlow() || !tritake.getRearIntakeBeamBreak()) && Utils.getCurrentTimeSeconds() - timeStartedAction > 0.1) {
                    if (inDemoMode) {
                        state = State.DEMO_MODE;
                    }
                    else {
                        state = desiredAction == Action.SHOOT_TO_SPEAKER ? State.PREPPING_SHOT : State.IDLE;
                    }
                    tritake.setFeederDutyCycle(0);
                    tramp.setTrampMiddleDutyCycle(0);
                    tritake.setTriForceBottomDutyCycle(0);
                    shooter.setDutyCycle(0, 0);
                }
            }
            case PREPPING_SHOT -> {
                var data = shotData;
                shootWithParams(data);
                if (readyToShoot(data)) {
                    state = State.SHOOTING;
                    timeStartedAction = Utils.getCurrentTimeSeconds();
                    aimTime = Timer.getFPGATimestamp() - MovingShotCommand.velocityLowTimestamp;

                }
            }
            case SHOOTING -> {
                led.setShooting(true);
                shootIntoSpeaker();
                // try lower shot time for ALL teleop shots, and then maybe auto too idk
                double shootTIme = DriverStation.isAutonomous() ? 0.275 : 0.15; // 0.275 , 0.125
//                if (fasterShooting) {
//                    shootTIme = 0.125;
//                }
                if ((Utils.getCurrentTimeSeconds() - timeStartedAction) > shootTIme) {
                    noteLocation = NoteLocation.NONE;
                    state = State.IDLE;
                    desiredAction = Action.NONE;
                    setAimed(false);
                }
            }
            case SCORING_TO_AMP -> {
                tramp.setTrampMiddleDutyCycle(0);
                tramp.setTrampTopDutyCycle(-1);
                if (Utils.getCurrentTimeSeconds() - timeStartedAction > 0.75 && (tramp.getTopStatorCurrent() < 20)) {
                    noteLocation = NoteLocation.NONE;
                    state = State.LOWERING_ELEVATOR;
                    desiredAction = Action.NONE;
                }
            }
            case PREPPING_AMP_SHOT -> {
                // Stop tramp
                tramp.setTrampMiddleDutyCycle(0.0);
                // Wait 0.1s to until raising the elevator
                if (Utils.getCurrentTimeSeconds() - timeStartedAction > 0.1) {
                    state = State.RAISING_ELEVATOR;
                }

            }

            case RAISING_ELEVATOR -> {
                //go here before scoring
                double extend = 12;
                tramp.setTrampExtensionMotor(Inches.of(extend));
                tramp.setTrampTopDutyCycle(0);
                tramp.setTrampMiddleDutyCycle(0);
                if (Math.abs(tramp.getExtensionInches() - extend) < 2) {
                    timeStartedAction = Utils.getCurrentTimeSeconds();
                    state = State.SCORING_TO_AMP;
                }
            }
            case LOWERING_ELEVATOR -> {
                //go here after scoring
                tramp.setTrampExtensionMotor(Inches.of(0));
                if (Math.abs(tramp.getExtensionInches()) < 1) {
                    state = State.IDLE;
                    desiredAction = Action.NONE;
                    noteLocation = NoteLocation.NONE;
                }
            }
            case PRE_RAISE_CLIMB -> {
                if (abortClimb) {
                    tramp.setTrampExtensionMotor(Inches.of(0));
                    desiredAction = Action.NONE;
                    state = State.IDLE;
                    break;
                }
                var mag = 6;
                tramp.setTrampExtensionMotor(Inches.of(mag));
                if (Util.epsilonEquals(tramp.getExtensionInches(), mag, 1) && startHalfClimb) {
                    state = State.PREPPING_CLIMB;
                    timeStartedAction = Utils.getCurrentTimeSeconds();
                }
            }

            case PREPPING_CLIMB -> {
                if (abortClimb) {
                    tramp.setTrampMiddleDutyCycle(-0.5);
                    if (Utils.getCurrentTimeSeconds() - timeStartedAction > 0.5) {
                        tramp.setTrampMiddleDutyCycle(0);
                        tramp.setTrampExtensionMotor(Inches.of(0));
                        climber.climbDown();
                        abortClimb = false;
                        state = State.PRE_RAISE_CLIMB;
                        break;
                    }
                }
                climber.setHooksUp();
                if ((Util.epsilonEquals(climber.getClimberRotationPos(), Climber.HOOKS_UP_POS, 3)) && confirmClimb) {
                    state = State.HOOKING_ON;
                }
            }

            case HOOKING_ON -> {
                //drive slightly forward
                if (abortClimb) {
                    climber.climbDown();
                    tramp.setTrapServo(SERVO_UP);
                    tramp.setTrampExtensionMotor(Inches.of(0));
                    if (Util.epsilonEquals(climber.getClimberRotationPos(), Climber.CLIMB_DOWN_POS, 1) && Util.epsilonEquals(tramp.getExtensionInches(), 0, 1)) {
                        state = State.PREPPING_CLIMB;
                    }
                    break;
                }
                climber.climbUp();
                tramp.setTrapServo(SERVO_DOWN); // pp out
                if (climber.getClimberRotationPos() > 37) {
                    state = State.CLIMBING;
                }

            }
            case CLIMBING -> {
                if (abortClimb) {
                    climber.climbDown();
                    tramp.setTrapServo(SERVO_UP);
                    tramp.setTrampExtensionMotor(Inches.of(0));
                    if (Util.epsilonEquals(climber.getClimberRotationPos(), Climber.CLIMB_DOWN_POS, 1) && Util.epsilonEquals(tramp.getExtensionInches(), 0, 1)) {
                        state = State.HOOKING_ON;
                    }
                    break;
                }
                var mag = 17.51;
                tramp.setTrampExtensionMotor(Inches.of(mag));
                if (Util.epsilonEquals(climber.getClimberRotationPos(), Climber.CLIMB_DOWN_POS, 2) && Util.epsilonEquals(tramp.getExtensionInches(), mag, 1)) {
                    timeStartedAction = Utils.getCurrentTimeSeconds();
                    state = State.SCORING_IN_TRAP;
                }
            }
            case SCORING_IN_TRAP -> {
                noteLocation = NoteLocation.NONE;
                if (Utils.getCurrentTimeSeconds() - timeStartedAction > 1) {
                    led.rainbowAnimation();
                    tramp.setTrampTopDutyCycle(-1);
                }

                if (smallUnclimb) {
                    state = State.SMALL_UNCLIMB;
                }
                if (abortClimb) {
                    state = State.LOWERING_CLIMB;
                    led.redBlinkAnimation();
                }
//                 if (Utils.getCurrentTimeSeconds() - timeStartedAction > 2 && !confirmClimb) {
//                    state = State.LOWERING_CLIMB;
//                }
            }

            case SMALL_UNCLIMB -> {
                climber.setSmallUnclimbPos();
                if (abortClimb) {
                    state = State.LOWERING_CLIMB;
                    led.redBlinkAnimation();
                }
            }

            case LOWERING_CLIMB -> {
                confirmClimb = false;
                tramp.setTrampTopDutyCycle(0);
                climber.climbDown();
//                tramp.setTrampExtensionMotor(Inches.of(0));
                tramp.setTrapServo(SERVO_UP); // pp back in
                if (Climber.CLIMB_DOWN_POS - climber.getClimberRotationPos() > 20) {
                    tramp.setTrampExtensionMotor(Inches.of(10));
                }
                if (Climber.CLIMB_DOWN_POS - climber.getClimberRotationPos() > 50) {
                    tramp.setTrampExtensionMotor(Inches.of(0));
                    state = State.IDLE;
                    desiredAction = Action.NONE;
                    abortClimb = false;
                }
            }
            case DEMO_BACKUP -> {
//                feederPos = tritake.getFeederPosition();
//                tritake.setDesiredFeederPosition(feederPos - 1); todo
                tritake.setFeederDutyCycle(-0.2);
                shooter.setDutyCycle(-0.1, -0.1);
                tramp.setTrampMiddleDutyCycle(0.2);
                tritake.setTriForceBottomDutyCycle(-0.3);
                timeStartedAction = Utils.getCurrentTimeSeconds();
                if (!tritake.getRearIntakeBeamBreak() && !tritake.getFrontIntakeBeamBreak()) {
                    state = State.DEMO_MODE;
                }
            }
            case DEMO_MODE -> {
                double demoSpeeds = 1400;
                if (apriltagTracking) {
                    demoModePosition.mut_replace(Degrees.of(Math.max(-LimelightHelpers.getTY("limelight")+57, 0)));
                }
                System.out.println(LimelightHelpers.getTA("limelight"));
                Shooter.BasicShotData data = new Shooter.BasicShotData(demoModePosition, RPM.of(demoSpeeds), RPM.of(demoSpeeds));
                shootWithParams(data);
                setAimed(demoAimed);
                if (readyToShoot(data)) {
                    shootIntoSpeaker();
                }
                if (!inDemoMode) {
                    setDemoAimed(false);
                    setAimed(false);
                    noteLocation = NoteLocation.NONE;
                    state = State.IDLE;
                }
            }

            case APRILTAG_TRACKING -> {
                //new LimelightAim()
            }

            //unclimb sequence:
            //start unclimbing climber,
            // if 4 inches down, retract tramp extension 4 inches
            // when cliumber is 8 icnesh down , tramp all da way down
        }
    }


    public void coast() {
        hood.coastMotor();
        climber.coastMotor();
        shooter.coastMotor();
        tritake.coastMotor();
        tramp.coastMotor();
    }

    public void brake() {
        hood.brakeMotor();
        climber.brakeMotor();
        shooter.brakeMotor();
        tritake.brakeMotor();
        tramp.brakeMotor();
    }

    public void setAimed(boolean aimed) {
        this.aimed = aimed;
    }

    public void setShotData(Shooter.BasicShotData data) {
        shotData = data;
    }

    public void setSmallUnclimb() {
        smallUnclimb = true;
    }

    private void shootWithParams(Shooter.BasicShotData shotData) {
        shooter.setLeftCL(shotData.leftRPM());
        shooter.setRightCL(shotData.rightRPM());
        hood.setDesiredAngle(shotData.angle());
    }

    public boolean readyToShoot(Shooter.BasicShotData shotData) {
        rightAtSpeed = shooter.rightAtGoal(shotData.rightRPM(), RPM.of(200));
        leftAtSpeed = shooter.leftAtGoal(shotData.leftRPM(), RPM.of(200));
        hoodAtAngle = Math.abs(hood.getPosition().getDegrees() - shotData.angle().in(Degrees)) < (shotData.angle().in(Degrees) * 0.015);
        sure = aimed;

        led.setRPMProgress(rightAtSpeed && leftAtSpeed);

        return rightAtSpeed && leftAtSpeed && hoodAtAngle && sure;
    }

}