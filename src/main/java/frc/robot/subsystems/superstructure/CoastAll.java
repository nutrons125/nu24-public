package frc.robot.subsystems.superstructure;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.generated.TunerConstants;

public class CoastAll extends Command {
    public CoastAll() {
        addRequirements(TunerConstants.DriveTrain, Superstructure.getInstance());
    }

    @Override
    public void initialize() {
        Superstructure.getInstance().coast();
        TunerConstants.DriveTrain.coastAll();
    }

    @Override
    public boolean isFinished() {
        return false;
    }
}
