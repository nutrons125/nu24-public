package frc.robot.subsystems.superstructure;

import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.FieldConstants;
import frc.robot.subsystems.drivetrain.CommandSwerveDrivetrain;

import static edu.wpi.first.units.Units.Degrees;
import static frc.robot.subsystems.superstructure.MovingShotCommand.*;

public class ShootTakeCommand extends Command {
    private final Superstructure superstructure;
    private final CommandSwerveDrivetrain drivetrain;
    boolean shouldIntake;

    public ShootTakeCommand(CommandSwerveDrivetrain drivetrain, Superstructure superstructure) {
        this.drivetrain = drivetrain;
        addRequirements(this.superstructure = superstructure);
    }

    @Override
    public void initialize() {
        drivetrain.setAutonomousShooting(true);
    }

    @Override
    public void execute() {
        // Get speeds and velocity
        var relativeSpeeds = drivetrain.getCurrentRobotChassisSpeeds();
        var fieldSpeeds = ChassisSpeeds.fromFieldRelativeSpeeds(relativeSpeeds, drivetrain.getState().Pose.getRotation().unaryMinus());
        var fieldVel = new Translation2d(fieldSpeeds.vxMetersPerSecond, fieldSpeeds.vyMetersPerSecond);

        // Don't intake when moving forward to avoid catastrophe
        shouldIntake = relativeSpeeds.vxMetersPerSecond < -0.0125;

        // Calculate shot data and set shot angle
        Shooter.MagicShotCalculator.ShotData data = Shooter.MagicShotCalculator.calculate(FieldConstants.getSpeakerPosition(), drivetrain.getState().Pose.getTranslation(), fieldVel);
        drivetrain.setAutonomousShotHeading(data.goalHeading());
        var angleDeg = DIST_TO_HOOD_MAP.get(Units.metersToFeet(data.effectiveRobotToSpeakerDist()));
        Shooter.BasicShotData shotData = new Shooter.BasicShotData(
                Degrees.of(angleDeg + 0.5),
                LEFT_RPM_SETPOINT,
                RIGHT_RPM_SETPOINT);

        // Shoot
        superstructure.shootTake(shotData, shouldIntake);
    }

    @Override
    public void end(boolean interrupted) {
        drivetrain.setAutonomousShooting(false);
        superstructure.cancelAction();
    }
}
