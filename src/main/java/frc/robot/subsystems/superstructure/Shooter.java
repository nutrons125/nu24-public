package frc.robot.subsystems.superstructure;

import com.ctre.phoenix6.BaseStatusSignal;
import com.ctre.phoenix6.StatusSignal;
import com.ctre.phoenix6.configs.*;
import com.ctre.phoenix6.controls.*;
import com.ctre.phoenix6.hardware.TalonFX;
import com.ctre.phoenix6.signals.InvertedValue;
import com.ctre.phoenix6.sim.ChassisReference;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.system.plant.DCMotor;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.units.Angle;
import edu.wpi.first.units.Measure;
import edu.wpi.first.units.MutableMeasure;
import edu.wpi.first.units.Velocity;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.simulation.FlywheelSim;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.RobotMap;
import frc.robot.util.TalonUtil;

import static edu.wpi.first.units.Units.RPM;
import static edu.wpi.first.units.Units.RotationsPerSecond;


public class Shooter extends SubsystemBase {

    public final StatusSignal<Double> leftShooterDCSignal;
    public final StatusSignal<Double> rightShooterDCSignal;
    private final StatusSignal<Double> leftRefSignal;
    private final StatusSignal<Double> leftErrSignal;
    private final StatusSignal<Double> rightRefSignal;
    private final StatusSignal<Double> rightErrSignal;
    private final StatusSignal<Double> rightVelSignal;
    private final StatusSignal<Double> leftVelSignal;
    private final StatusSignal<Double> leftStatorCurrent;
    private final StatusSignal<Double> rightStatorCurrent;
    private final StatusSignal<Double> rightSupplyCurrent;

    public final DutyCycleOut leftShooterRequest;
    public final DutyCycleOut rightShooterRequest;
    private static Shooter instance;
    public final double SHOOTER_RPM = 3000;
    public final double SHOOTER_GEAR_RATIO = 1;// 28 / 20.;
    private static final double SHOOTER_RIGHT_RATIO = 1;// 2.0;
    public final double SHOOTER_INERTIA = 0.000001;//0.5 * Units.lbsToKilograms(0.25) * Math.pow(Units.inchesToMeters(3), 2);


    public static Shooter getInstance() {
        if (instance == null) {
            instance = new Shooter();
        }
        return instance;
    }


    TalonFX leftShooter;
    TalonFX rightShooter;

    private com.ctre.phoenix6.sim.TalonFXSimState leftShooterSimState;
    private com.ctre.phoenix6.sim.TalonFXSimState rightShooterSimState;
    private FlywheelSim leftFwSim;
    private FlywheelSim rightFwSim;
    private static final double FF_VALUE = 6. / (2669.0 / 60.0);

    public ShuffleboardTab shooter = Shuffleboard.getTab("Shootah");

    public Shooter() {
        leftShooter = new TalonFX(RobotMap.LEFT_SHOOTER_CANID, "drivetrain");
        rightShooter = new TalonFX(RobotMap.RIGHT_SHOOTER_CANID, "drivetrain");
        Slot0Configs voltageConfigs = new Slot0Configs()
                .withKV(FF_VALUE) // at 12 Volts, 6000 RPM == 100 RPS => 12 / 100 = 0.12
                .withKP(0.11);
        MotionMagicConfigs magicConfigs = new MotionMagicConfigs()
                .withMotionMagicCruiseVelocity(96.6)
                .withMotionMagicAcceleration(9600) // Reach max vel in 0.1 second
                .withMotionMagicJerk(9600 * 100);
        var rampConfig = new OpenLoopRampsConfigs()
                .withDutyCycleOpenLoopRampPeriod(0.01);
        var closedLoopRampConfig = new ClosedLoopRampsConfigs()
                .withVoltageClosedLoopRampPeriod(0.005);
        var leftShooterConfig = new MotorOutputConfigs()
                .withInverted(InvertedValue.CounterClockwise_Positive);
        var rightShooterConfig = new MotorOutputConfigs()
                .withInverted(InvertedValue.Clockwise_Positive);
        var curLimit = new CurrentLimitsConfigs().withStatorCurrentLimit(80)
                .withStatorCurrentLimitEnable(true)
                .withSupplyCurrentLimit(60)
                .withSupplyCurrentThreshold(120)
                .withSupplyTimeThreshold(0.25)
                .withStatorCurrentLimitEnable(true);
        var leftConfig = new TalonFXConfiguration()
                .withSlot0(voltageConfigs)
                .withMotionMagic(magicConfigs)
                .withOpenLoopRamps(rampConfig)
                .withMotorOutput(leftShooterConfig)
                .withClosedLoopRamps(closedLoopRampConfig)
                .withCurrentLimits(curLimit);
        var rightConfig = new TalonFXConfiguration()
                .withSlot0(voltageConfigs)
                .withMotionMagic(magicConfigs)
                .withOpenLoopRamps(rampConfig)
                .withMotorOutput(rightShooterConfig)
                .withClosedLoopRamps(closedLoopRampConfig)
                .withCurrentLimits(curLimit);

        TalonUtil.applyAndCheckConfiguration(leftShooter, leftConfig);
        TalonUtil.applyAndCheckConfiguration(rightShooter, rightConfig);

        leftRefSignal = leftShooter.getClosedLoopReference();
        leftErrSignal = leftShooter.getClosedLoopError();
        rightRefSignal = rightShooter.getClosedLoopReference();
        rightErrSignal = rightShooter.getClosedLoopError();

        leftVelSignal = leftShooter.getVelocity();
        rightVelSignal = rightShooter.getVelocity();

        leftShooterDCSignal = leftShooter.getDutyCycle();
        rightShooterDCSignal = rightShooter.getDutyCycle();
        leftShooterRequest = new DutyCycleOut(0);
        rightShooterRequest = new DutyCycleOut(0);

        leftStatorCurrent = leftShooter.getStatorCurrent();
        rightStatorCurrent = rightShooter.getStatorCurrent();
        rightSupplyCurrent = rightShooter.getSupplyCurrent();

        leftFwSim = new FlywheelSim(DCMotor.getKrakenX60Foc(1), SHOOTER_GEAR_RATIO, SHOOTER_INERTIA);
        rightFwSim = new FlywheelSim(DCMotor.getKrakenX60Foc(1), SHOOTER_RIGHT_RATIO, SHOOTER_INERTIA);
        BaseStatusSignal.setUpdateFrequencyForAll(
                100,
                leftShooterDCSignal,
                rightShooterDCSignal,
                leftRefSignal,
                rightRefSignal,
                leftErrSignal,
                rightErrSignal,
                leftVelSignal,
                rightVelSignal,
                leftStatorCurrent,
                rightStatorCurrent,
                rightSupplyCurrent
        );

        leftShooter.optimizeBusUtilization();
        rightShooter.optimizeBusUtilization();

        shooter.addDouble("Left RPM", this::getLeftRPM);
        shooter.addDouble("Right RPM", this::getRightRPM);
        shooter.addDouble("Left Duty Cycle", this.leftShooterDCSignal::getValue);
        shooter.addDouble("Right Duty Cycle", this.rightShooterDCSignal::getValue);
        shooter.addDouble("Left Stator Current", this.leftStatorCurrent::getValue);
        shooter.addDouble("Right Stator Current", this.rightStatorCurrent::getValue);
        shooter.addDouble("Right Supply Current", this.rightSupplyCurrent::getValue);

        leftShooterSimState = leftShooter.getSimState();
        rightShooterSimState = rightShooter.getSimState();
    }

    @Override
    public void periodic() {
        var ec = BaseStatusSignal.refreshAll(
                leftShooterDCSignal,
                rightShooterDCSignal,
                leftRefSignal,
                rightRefSignal,
                leftErrSignal,
                rightErrSignal,
                leftVelSignal,
                rightVelSignal,
                leftStatorCurrent,
                rightStatorCurrent,
                rightSupplyCurrent
        );
        SmartDashboard.putString("Shooter EC", ec.toString());

    }

    CoastOut coast = new CoastOut();
    StaticBrake brake = new StaticBrake();

    public void coastMotor() {
        leftShooter.setControl(coast);
        rightShooter.setControl(coast);
    }

    public void brakeMotor() {
        leftShooter.setControl(brake);
        rightShooter.setControl(brake);
    }

    public void setDutyCycle(double speed1, double speed2) {
        leftShooter.setControl(leftShooterRequest.withEnableFOC(true).withOutput(speed1));
        rightShooter.setControl(rightShooterRequest.withEnableFOC(true).withOutput(speed2));
    }

    MotionMagicVelocityVoltage leftVelSetter = new MotionMagicVelocityVoltage(0).withEnableFOC(true);
    MotionMagicVelocityVoltage rightVelSetter = new MotionMagicVelocityVoltage(0).withEnableFOC(true);

    public void runClosedLoop(Measure<Velocity<Angle>> leftRpm, Measure<Velocity<Angle>> rightRpm) {
        setLeftCL(leftRpm);
        setRightCL(rightRpm);
    }


    public void setLeftCL(Measure<Velocity<Angle>> leftRpm) {
        leftShooter.setControl(leftVelSetter.withVelocity(leftRpm.in(RotationsPerSecond)));
    }

    public void setRightCL(Measure<Velocity<Angle>> rightRpm) {
        rightShooter.setControl(rightVelSetter.withVelocity(rightRpm.in(RotationsPerSecond)));
    }

    static boolean isClose(double goal, double actual, double tol) {
        return Math.abs(goal - actual) < tol;
    }

    public boolean rightAtGoal(Measure<Velocity<Angle>> rpmGoal, Measure<Velocity<Angle>> tolRpm) {
        double rpsGoal = rpmGoal.in(RotationsPerSecond);
        var tolRps = tolRpm.in(RotationsPerSecond);
        return rightVelSignal.getValue() > rpsGoal - tolRps;
//        return isClose(rpsGoal, rightVelSignal.getValue(), tolRps);
    }

    public boolean leftAtGoal(Measure<Velocity<Angle>> rpmGoal, Measure<Velocity<Angle>> tolRpm) {
        double rpsGoal = rpmGoal.in(RotationsPerSecond);
        var tolRps = tolRpm.in(RotationsPerSecond);
        return leftVelSignal.getValue() > rpsGoal - tolRps;
//        return isClose(rpsGoal, leftVelSignal.getValue(), tolRps);
    }

    private MutableMeasure<Velocity<Angle>> leftVel = RPM.of(0).mutableCopy();
    private MutableMeasure<Velocity<Angle>> rightVel = RPM.of(0).mutableCopy();


    public double getLeftRPM() {
        return leftVel.mut_replace(leftVelSignal.getValue(), RotationsPerSecond).in(RPM);
    }


    public double getRightRPM() {
        return rightVel.mut_replace(rightVelSignal.getValue(), RotationsPerSecond).in(RPM);
    }

    public double getLeftError() {
        return leftErrSignal.getValue() / 60;
    }

    public double getRightError() {
        return rightErrSignal.getValue() / 60;
    }


    public void simulationPeriodic() {
        leftShooterSimState.Orientation = ChassisReference.CounterClockwise_Positive;
        rightShooterSimState.Orientation = ChassisReference.CounterClockwise_Positive;

        leftShooterSimState = leftShooter.getSimState();
        leftShooterSimState.setSupplyVoltage(12);
        leftShooterSimState.setRotorVelocity(Units.radiansToRotations(leftFwSim.getAngularVelocityRadPerSec()));
        leftFwSim.setInputVoltage(leftShooterSimState.getMotorVoltage());
        leftFwSim.update(0.02);

        rightShooterSimState = rightShooter.getSimState();
        rightShooterSimState.setSupplyVoltage(12);
        rightShooterSimState.setRotorVelocity(Units.radiansToRotations(rightFwSim.getAngularVelocityRadPerSec()));
        rightFwSim.setInputVoltage(rightShooterSimState.getMotorVoltage());
        rightFwSim.update(0.02);

    }

    private VoltageOut leftOL = new VoltageOut(0).withEnableFOC(true);
    private VoltageOut rightOL = new VoltageOut(0).withEnableFOC(true);

    public void setVoltage(double volt) {
        leftShooter.setControl(leftOL.withOutput(volt));
        rightShooter.setControl(rightOL.withOutput(volt));
    }

    public record BasicShotData(
            Measure<Angle> angle,
            Measure<Velocity<Angle>> leftRPM,
            Measure<Velocity<Angle>> rightRPM
    ) {
    }

    public static class MagicShotCalculator {
        public record ShotData(
                double effectiveRobotToSpeakerDist,
                double radialFeedforward, // ff value due to radial velocity of robot to speaker
                Rotation2d goalHeading) {
        } // heading of robot to match tangential velocity


        public static Shooter.MagicShotCalculator.ShotData calculate(
                Translation2d speakerTranslation, Translation2d robotTranslation, Translation2d linearFieldVelocity
        ) {
            return calculate(speakerTranslation, robotTranslation, linearFieldVelocity, 0.2);
        }

        /**
         * In theory we will aim at different locations inside speakerTranslation
         */
        public static Shooter.MagicShotCalculator.ShotData calculate(
                Translation2d speakerTranslation, Translation2d robotTranslation, Translation2d linearFieldVelocity, double shotTime) {
            // Calculate radial and tangential velocity from speakerTranslation
            Rotation2d speakerToRobotAngle = robotTranslation.minus(speakerTranslation).getAngle();
            Translation2d tangentialVelocity =
                    linearFieldVelocity.rotateBy(speakerToRobotAngle.unaryMinus());
            // Positive when velocity is away from speakerTranslation
            double radialComponent = tangentialVelocity.getX();
            // Positive when traveling CCW about speakerTranslation
            double tangentialComponent = tangentialVelocity.getY();

            // Add robot velocity to raw shot speed
            double rawDistToGoal = robotTranslation.getDistance(speakerTranslation);
            double shotSpeed = rawDistToGoal / shotTime + radialComponent;
            if (shotSpeed <= 0.0) shotSpeed = 0.0;
            // Aim opposite of tangentialComponent (negative lead when tangentialComponent is positive)
            // rotate back into field frame then add take opposite since shooter is on FRONT
            Rotation2d goalHeading =
                    new Rotation2d(shotSpeed, tangentialComponent)
                            .rotateBy(speakerTranslation.minus(robotTranslation).getAngle())
                            .plus(Rotation2d.fromRadians(0));
            double effectiveDist = shotTime * Math.hypot(tangentialComponent, shotSpeed);

//            Logger.recordOutput("ShootWhileMoving/heading", goalHeading);
//            Logger.recordOutput("ShootWhileMoving/radialFF", radialComponent);
//            Logger.recordOutput("ShootWhileMoving/effectiveDistance", effectiveDist);
            // Use radial component of velocity for ff value
            return new Shooter.MagicShotCalculator.ShotData(effectiveDist, radialComponent, goalHeading);
        }
    }

}
