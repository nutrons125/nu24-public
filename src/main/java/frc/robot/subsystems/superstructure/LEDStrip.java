package frc.robot.subsystems.superstructure;

import com.ctre.phoenix.led.Animation;
import com.ctre.phoenix.led.CANdle;
import com.ctre.phoenix.led.RainbowAnimation;
import com.ctre.phoenix.led.StrobeAnimation;
import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.RobotMap;

public class LEDStrip extends SubsystemBase {
    // General
    public static final int CANDLE_LED_COUNT = 8;
    public static final int FRONT_LED_COUNT = 20;
    public static final int BACK_LED_COUNT = 21;
    public static final int LED_COUNT = FRONT_LED_COUNT + BACK_LED_COUNT + CANDLE_LED_COUNT;
    // Limelight status section
    public static final int LIMELIGHT_START = 8 - 1 + CANDLE_LED_COUNT;
    public static final int LIMELIGHT_END = LIMELIGHT_START + 5;
    // Shoot progress
    public static final int PROGRESS_START_0 = CANDLE_LED_COUNT;
    public static final int PROGRESS_START_1 = CANDLE_LED_COUNT + 7;
    public static final int PROGRESS_START_2 = CANDLE_LED_COUNT + 7 + 6;

    public static final int DEFAULT_WHITE = 200;
    private static final Animation RED_BLINK_ANIMATION;
    private static final Animation PINK_BLINK_ANIMATION;
    private static final Animation BLUE_BLINK_ANIMATION;
    private static final Animation RAINBOW_ANIMATION;
    private static final Color PROGRESS_COLOR = Color.kOrangeRed;
    private static final Color LIMELIGHT_TARGET_COLOR = Color.kBlue;
    private static final double BLINK_DURATION = 0.25;
    private static final double RAINBOW_BLINK_DURATION = 0.1;
    private static LEDStrip instance;

    static {
        RED_BLINK_ANIMATION = new StrobeAnimation(255, 0, 0, DEFAULT_WHITE, BLINK_DURATION, LED_COUNT);
        PINK_BLINK_ANIMATION = new StrobeAnimation(255, 192, 203, 5, BLINK_DURATION, LED_COUNT);
        BLUE_BLINK_ANIMATION = new StrobeAnimation(0, 0, 255, DEFAULT_WHITE, BLINK_DURATION, LED_COUNT);
        RAINBOW_ANIMATION = new RainbowAnimation(200, 0.75, LED_COUNT);
    }

    private final CANdle candle;
    private Color backgroundColor = new Color();
    private Animation currentAnimation = null;
    private boolean shooterAtSpeedProgress = false;
    private boolean robotStationaryProgress = false;
    private boolean drivetrainAlignedProgress = false;
    private boolean limelightHasTarget = false;
    private boolean shooting = false;
    private boolean playingAnimation = false;

    private LEDStrip() {
        candle = new CANdle(RobotMap.CANDLE_CAN_ID, "drivetrain");
        candle.configLEDType(CANdle.LEDStripType.GRB);
    }

    public static LEDStrip getInstance() {
        return instance == null ? (instance = new LEDStrip()) : instance;
    }

    private static void colorToIntArr(Color c, int[] out) {
        out[0] = (int) (c.red * 255.);
        out[1] = (int) (c.green * 255.);
        out[2] = (int) (c.blue * 255.);
    }

    public void update() {
        if (currentAnimation == null) { // If CANdle had a thing to pass an array that would be nice but I guess not
            playingAnimation = false;
            int[] rgb = new int[3];

            // If shooting override all
            if (shooting) {
                colorToIntArr(Color.kWhite, rgb);
                candle.setLEDs(rgb[0], rgb[1], rgb[2]);
            } else {
                // Set full strip color
                colorToIntArr(backgroundColor, rgb);
                candle.setLEDs(rgb[0], rgb[1], rgb[2]);

                // Set drivetrain aligned progress
                if (drivetrainAlignedProgress) {
                    colorToIntArr(PROGRESS_COLOR, rgb);

                    // Carry over progress bar to next sections
                    if (shooterAtSpeedProgress && robotStationaryProgress) {
                        candle.setLEDs(rgb[0], rgb[1], rgb[2], DEFAULT_WHITE, PROGRESS_START_0, 7 + 6 + 7);
                    } else if (shooterAtSpeedProgress) {
                        candle.setLEDs(rgb[0], rgb[1], rgb[2], DEFAULT_WHITE, PROGRESS_START_0, 7 + 6);
                    } else {
                        candle.setLEDs(rgb[0], rgb[1], rgb[2], DEFAULT_WHITE, PROGRESS_START_0, 7);
                    }
                } else if (shooterAtSpeedProgress) {// Set shooter speed progress and progress bar didn't carry over from next condition
                    colorToIntArr(PROGRESS_COLOR, rgb);

                    // Carry over progress bar to next section
                    if (robotStationaryProgress) {
                        candle.setLEDs(rgb[0], rgb[1], rgb[2], DEFAULT_WHITE, PROGRESS_START_1, 6 + 7);
                    } else {
                        candle.setLEDs(rgb[0], rgb[1], rgb[2], DEFAULT_WHITE, PROGRESS_START_1, 6);
                    }
                } else if (robotStationaryProgress) {// Set robot stationary progress and progress bar didn't carry over from previous conditions
                    colorToIntArr(PROGRESS_COLOR, rgb);
                    candle.setLEDs(rgb[0], rgb[1], rgb[2], DEFAULT_WHITE, PROGRESS_START_2, 7);
                }

                // Set limelight section color
                if (limelightHasTarget) {
                    colorToIntArr(LIMELIGHT_TARGET_COLOR, rgb);
                    candle.setLEDs(rgb[0], rgb[1], rgb[2], DEFAULT_WHITE, LIMELIGHT_START, LIMELIGHT_END - LIMELIGHT_START);
                }
            }
        } else {
            if (!playingAnimation) {
                candle.animate(currentAnimation, 0);
                playingAnimation = true;
            }
        }
    }

    public void redBlinkAnimation() {
        if (currentAnimation != RED_BLINK_ANIMATION) {
            playingAnimation = false;
        }

        currentAnimation = RED_BLINK_ANIMATION;
    }

    public void pinkBlinkAnimation() {
        if (currentAnimation != PINK_BLINK_ANIMATION) {
            playingAnimation = false;
        }

        currentAnimation = PINK_BLINK_ANIMATION;
    }

    public void blueBlinkAnimation() {
        if (currentAnimation != BLUE_BLINK_ANIMATION) {
            playingAnimation = false;
        }

        currentAnimation = BLUE_BLINK_ANIMATION;
    }

    public void rainbowAnimation() {
        if (currentAnimation != RAINBOW_ANIMATION) {
            playingAnimation = false;
        }

        currentAnimation = RAINBOW_ANIMATION;
    }

    public void setAllRed() {
        stopCurrentAnimation();
        this.backgroundColor = Color.kRed;
    }

    public void setAllGreen() {
        stopCurrentAnimation();
        this.backgroundColor = Color.kGreen;
    }

    public void setAllBlue() {
        stopCurrentAnimation();
        this.backgroundColor = Color.kBlue;
    }

    public void off() {
        stopCurrentAnimation();
        this.backgroundColor = Color.kBlack;
    }

    public void setShooting(boolean shooting) {
        this.shooting = shooting;
    }

    public void setLimelightHasTarget(boolean targetAcquired) {
        limelightHasTarget = targetAcquired;
    }

    public void setRPMProgress(boolean atSpeed) {
        shooterAtSpeedProgress = atSpeed;
    }

    public void setStationaryProgress(boolean stationary) {
        robotStationaryProgress = stationary;
    }

    public void setDrivetrainAlignedProgress(boolean aligned) {
        drivetrainAlignedProgress = aligned;
    }

    public void resetProgress() {
        shooterAtSpeedProgress = false;
        robotStationaryProgress = false;
        drivetrainAlignedProgress = false;
        shooting = false;
    }

    public void setLeds(Color color, int start, int end) {
        int frontStart = CANDLE_LED_COUNT + start;
        int backStart = CANDLE_LED_COUNT + FRONT_LED_COUNT + start;

        int[] rgb = {(int) (color.red * 255.0), (int) (color.green * 255.0), (int) (color.blue * 255.0)};

        stopCurrentAnimation();
        candle.setLEDs(rgb[0], rgb[1], rgb[2], DEFAULT_WHITE, frontStart, end - start);
        candle.setLEDs(rgb[0], rgb[1], rgb[2], DEFAULT_WHITE, backStart, end - start);
    }

    private void stopCurrentAnimation() {
        if (currentAnimation != null) {
            candle.clearAnimation(0);
            currentAnimation = null;
            playingAnimation = false;
        }
    }
}
