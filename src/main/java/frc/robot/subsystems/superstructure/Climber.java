package frc.robot.subsystems.superstructure;

import com.ctre.phoenix6.BaseStatusSignal;
import com.ctre.phoenix6.StatusSignal;
import com.ctre.phoenix6.configs.*;
import com.ctre.phoenix6.controls.*;
import com.ctre.phoenix6.hardware.TalonFX;
import com.ctre.phoenix6.signals.ControlModeValue;
import com.ctre.phoenix6.signals.GravityTypeValue;
import com.ctre.phoenix6.signals.InvertedValue;
import com.ctre.phoenix6.signals.NeutralModeValue;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.RobotMap;
import frc.robot.util.TalonUtil;

public class Climber extends SubsystemBase {

    TalonFX climberMotor = new TalonFX(RobotMap.CLIMBER_MOTOR_CANID, "drivetrain");

    private static Climber instance;
    private final StatusSignal<Double> climberDCSignal;
    private final StatusSignal<Double> climberPosSignal;

    public final DutyCycleOut climberRequest;
    private static final double GEAR_RATIO = 30.56;
    private static final double PULLEY_DIAM_INChes = 1.432;
    public static final double HOOKS_UP_POS = 40;//16.58;
    public static final double SMALL_UNCLIMB_POS = 126;
    public static final double CLIMB_DOWN_POS = 176;//176
    private static final double KG_AMPS = 17.9; // Amps to lift robot


    private final MotionMagicExpoTorqueCurrentFOC unLoadedPosRequest;
    private final MotionMagicExpoTorqueCurrentFOC climbingRequest;
    private StatusSignal<Double> refSignal;

    public static Climber getInstance() {
        if (instance == null) {
            instance = new Climber();
        }
        return instance;
    }

    private StatusSignal<ControlModeValue> modeSignal;

    public Climber() {

        climberDCSignal = climberMotor.getDutyCycle();
        climberPosSignal = climberMotor.getPosition();
        climberRequest = new DutyCycleOut(0);
        var ramp = new OpenLoopRampsConfigs()
                .withDutyCycleOpenLoopRampPeriod(0.005);
        var directionconfig = new MotorOutputConfigs().withInverted(InvertedValue.Clockwise_Positive)
                .withNeutralMode(NeutralModeValue.Brake);
    //todo add kG and stuff
        var currentLimits = new CurrentLimitsConfigs()
                .withSupplyCurrentLimitEnable(true)
                .withSupplyCurrentLimit(40)
                .withStatorCurrentLimitEnable(true)
                .withStatorCurrentLimit(60);
//        var softLimitConfig = new SoftwareLimitSwitchConfigs().withForwardSoftLimitEnable(true)
//                .withForwardSoftLimitThreshold(MAX_HEIGHT / GEAR_RATIO)
//                .withReverseSoftLimitEnable(true)
//                .withReverseSoftLimitThreshold(0);

        var motionMagicConfig = new MotionMagicConfigs()
                .withMotionMagicCruiseVelocity(96.6)
                .withMotionMagicAcceleration(600) //todo 2225.530583
                .withMotionMagicJerk(1200)
                .withMotionMagicExpo_kV(0.08)
                .withMotionMagicExpo_kA(0.012);
        climbingRequest = new MotionMagicExpoTorqueCurrentFOC(0).withSlot(1);
        unLoadedPosRequest = new MotionMagicExpoTorqueCurrentFOC(0);
        modeSignal = climberMotor.getControlMode();
        var slot0 = new Slot0Configs()
                .withGravityType(GravityTypeValue.Elevator_Static)
                .withKG(0)
                .withKP(10)
                .withKD(2)
                .withKA(0.048)
                .withKS(1);

        refSignal = climberMotor.getClosedLoopReferenceSlope();

        var slot1 = new Slot1Configs()
                .withGravityType(GravityTypeValue.Elevator_Static)
                .withKG(KG_AMPS)
                .withKP(29)
                .withKD(2)
                .withKA(0.048)
                .withKS(1);
        TalonFXConfiguration climberConfig = new TalonFXConfiguration()
                .withOpenLoopRamps(ramp)
                .withMotorOutput(directionconfig)
                .withCurrentLimits(currentLimits)
                .withSlot0(slot0)
                .withSlot1(slot1)
                .withMotionMagic(motionMagicConfig);

        TalonUtil.applyAndCheckConfiguration(climberMotor, climberConfig);
        BaseStatusSignal.setUpdateFrequencyForAll(
                100,
                climberPosSignal,
                climberDCSignal,
                modeSignal,
                refSignal
        );
        climberMotor.optimizeBusUtilization();
        climberMotor.setControl(climberRequest);

        climberMotor.setPosition(0);

    }
    @Override
    public void periodic() {
        BaseStatusSignal.refreshAll(climberDCSignal,
                modeSignal,
                climberPosSignal,
                refSignal
        );
        SmartDashboard.putNumber("Climber DC", climberDCSignal.getValue());
        SmartDashboard.putNumber("Climber Rotations", climberPosSignal.getValue());
        SmartDashboard.putString("Climber Mode", modeSignal.getValue().toString());
        SmartDashboard.putNumber("Climber Ref", refSignal.getValue());
    }

    public void setClimberDutyCycle(double speed) {
        climberMotor.setControl(climberRequest.withOutput(speed));
    }

    private CoastOut coastOut = new CoastOut();
    private StaticBrake staticBrake = new StaticBrake();

    public void coastMotor() {
        climberMotor.setControl(coastOut);
    }

    public void brakeMotor() {
        climberMotor.setControl(staticBrake);
    }

    public void setHooksUp() {
        //uhhh
        climberMotor.setControl(unLoadedPosRequest.withPosition(HOOKS_UP_POS));
    }

    public double getClimberRotationPos() {
        return climberPosSignal.getValue();
    }

    public void climbUp() {
        climberMotor.setControl(climbingRequest.withPosition(CLIMB_DOWN_POS));
    }

    public void setSmallUnclimbPos() {
        climberMotor.setControl(climbingRequest.withPosition(SMALL_UNCLIMB_POS));
    }

    public void climbDown() {
        climberMotor.setControl(unLoadedPosRequest.withPosition(0));
    }


}
