package frc.robot.subsystems.superstructure;

import com.ctre.phoenix6.Utils;
import edu.wpi.first.math.filter.Debouncer;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.interpolation.InterpolatingDoubleTreeMap;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.units.*;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.FieldConstants;
import frc.robot.subsystems.drivetrain.CommandSwerveDrivetrain;
import frc.robot.subsystems.vision.LimelightVision;

import java.util.function.BooleanSupplier;
import java.util.function.Supplier;

import static edu.wpi.first.units.Units.Degrees;
import static edu.wpi.first.units.Units.RPM;

public class MovingShotCommand extends Command {
    public static final Measure<Velocity<Angle>> LEFT_RPM_SETPOINT = RPM.of(3500);
    public static final Measure<Velocity<Angle>> RIGHT_RPM_SETPOINT = RPM.of(5000);
    public static final double
            ANGLE_TOLERANCE = 10, // was 1.25
            MOVING_ANGLE_TOLERANCE = 5,
            STATIONARY_VEL_TOLERANCE = 0.1;
    private static final double STATIONARY_TIME = 0.04;
    private static final double RELATIVE_AIM_TIME = 0.06;
    public static InterpolatingDoubleTreeMap DIST_TO_HOOD_MAP = new InterpolatingDoubleTreeMap();
    public static InterpolatingDoubleTreeMap DIST_TO_LEFT_RPM_MAP = new InterpolatingDoubleTreeMap();
    public static InterpolatingDoubleTreeMap DIST_TO_RIGHT_RPM_MAP = new InterpolatingDoubleTreeMap();
    public static double velocityLowTimestamp = -1;

    static {
        // THESE WERE TAKEN FROM FRONT OF ROBOT
        DIST_TO_HOOD_MAP.put(4.3, 58.0);
        DIST_TO_HOOD_MAP.put(6.083333333333333, 51.0);
        DIST_TO_HOOD_MAP.put(7.083333333333333, 45.0);
        DIST_TO_HOOD_MAP.put(8.083333333333334, 40.0 + 2);
        DIST_TO_HOOD_MAP.put(9.083333333333334, 38.0 + 2);
        DIST_TO_HOOD_MAP.put(10.083333333333334, 34.5 + 2);
        DIST_TO_HOOD_MAP.put(11.083333333333334, 33.5 + 2);
        DIST_TO_HOOD_MAP.put(12.083333333333334, 32.5 + 1);
        DIST_TO_HOOD_MAP.put(13.083333333333334, 30.0);
        DIST_TO_HOOD_MAP.put(14.083333333333334, 29.2);
        DIST_TO_HOOD_MAP.put(14.8, 27.8);
//        DIST_TO_HOOD_MAP.put(15.0, 27.0);
        DIST_TO_HOOD_MAP.put(16.5, 26.3);
        DIST_TO_HOOD_MAP.put(17.75, 26.0);
        DIST_TO_HOOD_MAP.put(18.5, 24.75);
        DIST_TO_HOOD_MAP.put(19.5, 24.25);
        DIST_TO_HOOD_MAP.put(21.3, 23.65);
        DIST_TO_HOOD_MAP.put(22.5, 22.5);
        DIST_TO_HOOD_MAP.put(22.85, 22.0);
//        DIST_TO_HOOD_MAP.put(23.5, 22.0);

        DIST_TO_RIGHT_RPM_MAP.put(5.5, 3000.);
        DIST_TO_LEFT_RPM_MAP.put(5.5, 3000. * 0.7);
        DIST_TO_RIGHT_RPM_MAP.put(10., RIGHT_RPM_SETPOINT.in(RPM));
        DIST_TO_LEFT_RPM_MAP.put(10., LEFT_RPM_SETPOINT.in(RPM));
    }

    // Suppliers for X and Y vel. Set these to be joysticks OH MY GOD
    private final Supplier<Measure<Velocity<Distance>>> xVelSupplier;
    private final Supplier<Measure<Velocity<Distance>>> yVelSupplier;
    private final CommandSwerveDrivetrain drivetrain;
    private final Superstructure superstructure;
    private final LEDStrip led;
    private final MutableMeasure<Velocity<Angle>> leftSP = RPM.of(0).mutableCopy();
    private final MutableMeasure<Velocity<Angle>> rightSP = RPM.of(0).mutableCopy();
    private final LimelightVision limelight;
    private final boolean autonomous;
    private Debouncer stationaryDebouncer = new Debouncer(0.0001);
    private Debouncer relativeAimDebouncer = new Debouncer(RELATIVE_AIM_TIME);
    private LimelightVision.LimelightData llRes;
    private Rotation2d desiredHeading = new Rotation2d();
    private double velTolerance = 0.4;
    private double omegaTolerance = Units.degreesToRadians(4);
    private boolean startedShooting = false;
    private BooleanSupplier moveShootSupplier = () -> true;
    private double timeStarted;
    private boolean relativeAimSetting = false;
    private boolean shouldShootSetting = true;
    private boolean stationary = false;
    private boolean angleOnTarget = false;
    private boolean overridePathAngleSetting = false;

    public MovingShotCommand(CommandSwerveDrivetrain drivetrain, Superstructure superstructure, Supplier<Measure<Velocity<Distance>>> xSup, Supplier<Measure<Velocity<Distance>>> ySup, boolean autonomous) {
        this.drivetrain = drivetrain;
        this.superstructure = superstructure;
        xVelSupplier = xSup;
        yVelSupplier = ySup;
        this.autonomous = autonomous;
        led = LEDStrip.getInstance();
        limelight = LimelightVision.getInstance();
        if (this.autonomous) {
            addRequirements(superstructure);
        } else {
            addRequirements(drivetrain, superstructure);
        }

        // Initialize shuffleboard entries
//        ShuffleboardTab shootingTab = Shuffleboard.getTab("Shooting");
//
//        shootingTab.addBoolean("Relative", () -> llRes != null && (relativeAimSetting && llRes.valid()));
//        shootingTab.addBoolean("stationary?", () -> stationary);
//        shootingTab.addBoolean("Angle On Target", () -> angleOnTarget);
//        shootingTab.addNumber("Desired Goal Heading", desiredHeading::getDegrees);
    }

    public static MovingShotCommand autonomousMovingShot(CommandSwerveDrivetrain dt, Superstructure ss) {
        var c = new MovingShotCommand(dt, ss, null, null, true);
        c.overridePathAngleSetting = false;
        return c;
    }

    public static MovingShotCommand autonomousStationaryShot(CommandSwerveDrivetrain dt, Superstructure ss) {
        var c = new MovingShotCommand(dt, ss, null, null, true);
        c.moveShootSupplier = () -> false;
        return c;
    }

    public static MovingShotCommand autonomousStationaryShot(CommandSwerveDrivetrain dt, Superstructure ss, double tolerance, double omegaTol) {
        var c = new MovingShotCommand(dt, ss, null, null, true);
        c.velTolerance = tolerance;
        c.omegaTolerance = Units.degreesToRadians(omegaTol);
        return c;
    }

    public static MovingShotCommand relativeShot(CommandSwerveDrivetrain dt, Superstructure ss, Supplier<Measure<Velocity<Distance>>> xSup, Supplier<Measure<Velocity<Distance>>> ySup, BooleanSupplier moveShootSupplier, boolean relativeAim, boolean autonomous) {
        MovingShotCommand c = new MovingShotCommand(dt, ss, xSup, ySup, autonomous);
        c.moveShootSupplier = moveShootSupplier;
        c.relativeAimSetting = relativeAim;
        return c;
    }

    public static MovingShotCommand optionalStationary(CommandSwerveDrivetrain dt, Superstructure ss, Supplier<Measure<Velocity<Distance>>> xSup, Supplier<Measure<Velocity<Distance>>> ySup, BooleanSupplier moveShootSupplier) {
        MovingShotCommand c = new MovingShotCommand(dt, ss, xSup, ySup, false);
        c.moveShootSupplier = moveShootSupplier;
        return c;
    }

    public static MovingShotCommand autonomousRevOnly(CommandSwerveDrivetrain dt, Superstructure ss) {
        MovingShotCommand c = new MovingShotCommand(dt, ss, null, null, true);
        c.moveShootSupplier = () -> false;
        c.shouldShootSetting = false;
        c.relativeAimSetting = true;
        return c;
    }

    public static MovingShotCommand autonomousRevOnlyOverride(CommandSwerveDrivetrain dt, Superstructure ss) {
        MovingShotCommand c = new MovingShotCommand(dt, ss, null, null, true);
        c.moveShootSupplier = () -> false;
        c.shouldShootSetting = false;
        c.relativeAimSetting = true;
        c.overridePathAngleSetting = true;
        return c;
    }



    @Override
    public void initialize() {
        drivetrain.setAutonomousShooting(overridePathAngleSetting);
        timeStarted = Utils.getCurrentTimeSeconds();
        superstructure.setAimed(false);
        stationaryDebouncer = new Debouncer(STATIONARY_TIME);
        relativeAimDebouncer = new Debouncer(RELATIVE_AIM_TIME);
        if (DriverStation.isTeleop() && FieldConstants.isBlue()) {
            limelight.limelightLedOff();
        }
        velocityLowTimestamp = -1;
    }

    @Override
    public void execute() {
        // Make a swerverequest for the drive
        // Find velocity from DT
        // RUn shooter calculations
        Translation2d translation = drivetrain.getState().Pose.getTranslation();
        llRes = limelight.getData();

        if (relativeAimSetting) {
            if (llRes.valid()) {
                translation = llRes.approxTranslation();
            }
        }

        var fieldSpeeds = ChassisSpeeds.fromFieldRelativeSpeeds(drivetrain.getCurrentRobotChassisSpeeds(), drivetrain.getState().Pose.getRotation().unaryMinus());
        var fieldVel = new Translation2d(fieldSpeeds.vxMetersPerSecond, fieldSpeeds.vyMetersPerSecond);
        SmartDashboard.putNumber("Field Vel Omega", Units.radiansToDegrees(fieldSpeeds.omegaRadiansPerSecond));

        if (!moveShootSupplier.getAsBoolean()) { // if shoot stationary
            Shooter.MagicShotCalculator.ShotData data = Shooter.MagicShotCalculator.calculate(FieldConstants.getSpeakerPosition(), translation, new Translation2d());
            Translation2d modifiedSpeakerPos = FieldConstants.getSpeakerPosition().plus(new Translation2d(FieldConstants.getFlip() * Units.inchesToMeters(6), 0));

            desiredHeading = modifiedSpeakerPos.minus(translation).getAngle(); //drivetrain.getState().Pose.getTranslation().minus(FieldConstants.getSpeakerPosition()).getAngle();
            double angle = DIST_TO_HOOD_MAP.get(Units.metersToFeet(data.effectiveRobotToSpeakerDist()));
            if (DriverStation.isEnabled()) {
                angle += 1 - angle / 100; // TODO: Increase
            }
            double distFeet = Units.metersToFeet(data.effectiveRobotToSpeakerDist());
//            if (relativeAim && llRes.valid()) {
//                distFeet = Units.metersToFeet(llRes.distanceToGoalM());
//            }
            Shooter.BasicShotData stationaryData = new Shooter.BasicShotData(
                    Degrees.of(angle),
                    leftSP.mut_replace(DIST_TO_LEFT_RPM_MAP.get(distFeet), RPM),
                    rightSP.mut_replace(DIST_TO_RIGHT_RPM_MAP.get(distFeet), RPM));

            // Spin up shooter
            superstructure.setWantShoot();
            superstructure.setShotData(stationaryData);
        } else {
            Shooter.MagicShotCalculator.ShotData data = Shooter.MagicShotCalculator.calculate(FieldConstants.getSpeakerPosition(), drivetrain.getState().Pose.getTranslation(), fieldVel, 0.35);
            Shooter.BasicShotData shotData = new Shooter.BasicShotData(
                    Degrees.of(DIST_TO_HOOD_MAP.get(Units.metersToFeet(data.effectiveRobotToSpeakerDist()))),
                    leftSP.mut_replace(DIST_TO_LEFT_RPM_MAP.get(Units.metersToFeet(data.effectiveRobotToSpeakerDist())), RPM),
                    rightSP.mut_replace(DIST_TO_RIGHT_RPM_MAP.get(Units.metersToFeet(data.effectiveRobotToSpeakerDist())), RPM));

            desiredHeading = data.goalHeading();

            // Spin up shooter
            superstructure.setWantShoot();
            superstructure.setShotData(shotData);
        }

        // Aim DT
        if (!autonomous) {
            drivetrain.faceAngleFieldCentric(
                    desiredHeading,
                    xVelSupplier.get(),
                    yVelSupplier.get(),
                    false
            );
        } else {
            drivetrain.setAutonomousShotHeading(desiredHeading);
        }

        // If shooter ready and aimed, then shoot
        if (moveShootSupplier.getAsBoolean()) { // Check moving shot conditions
            if (Math.abs(drivetrain.getState().Pose.getRotation().minus(desiredHeading).getDegrees()) < MOVING_ANGLE_TOLERANCE
                    && superstructure.hasGamePiece()) {
                superstructure.setAimed(true);
                startedShooting = true;
            }
        } else { // if not doing moving shot Check stationary shot conditions`
            double poseMag = drivetrain.getMovingAverageVelMagnitude();
            stationary = poseMag < velTolerance && Math.abs(fieldSpeeds.omegaRadiansPerSecond) < omegaTolerance;

            // if relativeAim is true AND see target, modify `stationary` to just check limelight pose not moving
            if (relativeAimSetting && llRes.valid()) {
                stationary = LimelightVision.getInstance().shouldAim() && Math.abs(fieldSpeeds.omegaRadiansPerSecond) < omegaTolerance;
            }

            angleOnTarget = Math.abs(drivetrain.getState().Pose.getRotation().minus(desiredHeading).getDegrees()) < ANGLE_TOLERANCE;
            if (relativeAimSetting) {
                angleOnTarget = relativeAimDebouncer.calculate(angleOnTarget);
            }

            led.setStationaryProgress(stationary);
            led.setDrivetrainAlignedProgress(angleOnTarget);

            double stationaryTol = STATIONARY_VEL_TOLERANCE;

            boolean velocityLow;
            if (autonomous) {
                velocityLow = stationaryDebouncer.calculate(fieldVel.getNorm() < stationaryTol);
            } else {
                velocityLow = fieldVel.getNorm() < stationaryTol;
            }

            // If velocity is low record the time it started, if it's not reset the timestamp
            if (velocityLow && velocityLowTimestamp == -1) {
                velocityLowTimestamp = Timer.getFPGATimestamp();
            } else if (!velocityLow) {
                velocityLowTimestamp = -1;
            }

            // If note not detected in teleop, shoot anyways by setting the game piece location
            if (DriverStation.isTeleop() && !startedShooting && (Utils.getCurrentTimeSeconds() - timeStarted > 0.3)) { // very important for when sensor breaks DO NOT CHANGE
                superstructure.setGamePiece(NoteLocation.SHOOTER);
            }

            if (angleOnTarget
                    && superstructure.hasGamePiece()
                    && stationary
                    && velocityLow
                    && shouldShootSetting) {
                superstructure.setAimed(true);
                startedShooting = true;
            }
        }

        SmartDashboard.putBoolean("Relative", llRes != null && (relativeAimSetting && llRes.valid()));
        SmartDashboard.putBoolean("stationary?", stationary);
        SmartDashboard.putBoolean("Angle On Target", angleOnTarget);
        SmartDashboard.putNumber("Desired Goal Heading", desiredHeading.getDegrees());
    }

    @Override
    public boolean isFinished() {
        return startedShooting && !superstructure.hasGamePiece();
    }

    @Override
    public void end(boolean interrupted) {
        drivetrain.setAutonomousShooting(false);
        startedShooting = false;
        if (!DriverStation.isAutonomous()) superstructure.cancelAction();
        led.setStationaryProgress(false);
        led.setDrivetrainAlignedProgress(false);
        limelight.limelightLedOff();
    }
}
