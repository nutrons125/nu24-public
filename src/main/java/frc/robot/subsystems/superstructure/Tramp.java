package frc.robot.subsystems.superstructure;

import com.ctre.phoenix6.BaseStatusSignal;
import com.ctre.phoenix6.StatusSignal;
import com.ctre.phoenix6.configs.*;
import com.ctre.phoenix6.controls.*;
import com.ctre.phoenix6.hardware.TalonFX;
import com.ctre.phoenix6.signals.GravityTypeValue;
import com.ctre.phoenix6.signals.InvertedValue;
import edu.wpi.first.math.filter.Debouncer;
import edu.wpi.first.units.Distance;
import edu.wpi.first.units.Measure;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Servo;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.RobotMap;
import frc.robot.util.TalonUtil;
import frc.robot.util.Util;

import static edu.wpi.first.units.Units.Inches;

public class Tramp extends SubsystemBase {

    private static final double GEAR_RATION = 3.75;
    private static final double IN_PER_ROT = Math.PI * 1 / GEAR_RATION;
    public static Tramp instance;
    public final DigitalInput trampBeamBreak = new DigitalInput(RobotMap.TRAMP_BEAM_BREAK_ID);
    public final StatusSignal<Double> trampTopRollerDCSignal;
    public final StatusSignal<Double> trampExtensionSignal;
    public final StatusSignal<Double> trampMiddleRollerDCSignal;
    public final StatusSignal<Double> trampTopRollerCurrentStatorSignal;
    public final StatusSignal<Double> trampTopRollerCurrentSupplySignal;
    public final StatusSignal<Double> trampTopRollerPosSignal;
    public final DutyCycleOut trampShooterRequest;
    public final MotionMagicExpoVoltage trampExtensionRequest;
    public final MotionMagicVoltage trampTopRollerRequest;
    public final DutyCycleOut trampMiddleRollerRequest;
    private final Debouncer trampExtensionDebouncer = new Debouncer(0.5);
    private final double UPTAKE_ROTATIONS = 18;
    TalonFX trampTopMotor = new TalonFX(RobotMap.TRAMP_MOTOR_CANID, "drivetrain");
    TalonFX trampExtensionMotor = new TalonFX(RobotMap.TRAMP_EXTENSION_MOTOR_CANID, "drivetrain");
    TalonFX trampMiddleRollerMotor = new TalonFX(RobotMap.TRAMP_MIDDLE_ROLLER_CANID, "drivetrain");
    Servo trapServo = new Servo(RobotMap.TRAP_SERVO_CHANNEL);
    double startingTopRollerPos;
    private ShuffleboardTab trampTab = Shuffleboard.getTab("Tramp");
    private boolean extensionIsZero = true;
    private boolean beamBreak = false;
    private DutyCycleOut extensionDC = new DutyCycleOut(0);
    private CoastOut coastOut = new CoastOut();
    private StaticBrake staticBrake = new StaticBrake();
    private Debouncer beamBeakDebouncer = new Debouncer(0.1, Debouncer.DebounceType.kRising);
    private boolean latestSensorDebounce;

    public Tramp() {
        var ramp = new OpenLoopRampsConfigs()
                .withDutyCycleOpenLoopRampPeriod(0.005);

        trampTopRollerDCSignal = trampTopMotor.getDutyCycle();
        trampMiddleRollerDCSignal = trampMiddleRollerMotor.getDutyCycle();
        trampExtensionSignal = trampExtensionMotor.getPosition();
        trampTopRollerCurrentStatorSignal = trampTopMotor.getStatorCurrent();
        trampTopRollerCurrentSupplySignal = trampTopMotor.getSupplyCurrent();
        trampTopRollerPosSignal = trampTopMotor.getPosition();

        trampShooterRequest = new DutyCycleOut(0);
        trampExtensionRequest = new MotionMagicExpoVoltage(0).withEnableFOC(true);
        trampTopRollerRequest = new MotionMagicVoltage(0);
        trampMiddleRollerRequest = new DutyCycleOut(0);

        var brake = new StaticBrake();


        var extensionConfig = new MotorOutputConfigs().withInverted(InvertedValue.Clockwise_Positive);
        var motionMagicConfig = new MotionMagicConfigs()
                .withMotionMagicCruiseVelocity(80)
                .withMotionMagicAcceleration(600) //todo 2225.530583
                .withMotionMagicJerk(1200)
                .withMotionMagicExpo_kV(0.154)
                .withMotionMagicExpo_kA(0.012);

        var rollerConfig = new MotionMagicConfigs().withMotionMagicCruiseVelocity(90)
                .withMotionMagicAcceleration(800);
        var rollerslot0Condig = new Slot0Configs()
                .withKP(10)
                .withKV(0.2);
        var slot0Config = new Slot0Configs()
                .withGravityType(GravityTypeValue.Elevator_Static)
                .withKG(0.15) // guesstimate
                .withKP(5) // guess
                .withKV(12.0 / 96.6 * 0.8); // guesstimate

        var curLimits = new CurrentLimitsConfigs()
                .withSupplyCurrentLimitEnable(true)
                .withSupplyCurrentLimit(40)
                .withStatorCurrentLimitEnable(true)
                .withStatorCurrentLimit(60);
        var rollercurLimits = new CurrentLimitsConfigs()
                .withSupplyCurrentLimitEnable(true)
                .withSupplyCurrentLimit(20)
                .withStatorCurrentLimitEnable(true)
                .withStatorCurrentLimit(90);
        var softLimitConfig = new SoftwareLimitSwitchConfigs().withForwardSoftLimitEnable(true)
                .withForwardSoftLimitThreshold((17.6) / IN_PER_ROT)
                .withReverseSoftLimitEnable(true)
                .withReverseSoftLimitThreshold(0);

        TalonFXConfiguration trampExtensionConfig = new TalonFXConfiguration()
                .withSlot0(slot0Config)
                .withMotionMagic(motionMagicConfig)
                .withMotorOutput(extensionConfig)
                .withCurrentLimits(curLimits)
                .withSoftwareLimitSwitch(softLimitConfig);

        TalonUtil.applyAndCheckConfiguration(trampExtensionMotor, trampExtensionConfig);
        trampExtensionMotor.setControl(trampExtensionRequest);

        trampTab.addNumber("Tramp Shooter DC", trampTopRollerDCSignal::getValue);
        trampTab.addNumber("Tramp Extension Position", this::getExtensionInches);
        trampTab.addNumber("Tramp Top Stator", trampTopRollerCurrentStatorSignal::getValue);
        trampTab.addNumber("Tramp Top Supply", trampTopRollerCurrentSupplySignal::getValue);
        trampTab.addNumber("Tramp Top Position", trampTopRollerPosSignal::getValue);
        trampTab.addNumber("Tramp Servo Postition", trapServo::getPosition);
        trampTab.addNumber("Top roller desired Postition", () -> startingTopRollerPos + UPTAKE_ROTATIONS);
        trampExtensionMotor.setPosition(0);
        trampExtensionMotor.setControl(trampExtensionRequest.withPosition(0));
        trapServo.setPosition(0);

        var middleConfig = new MotorOutputConfigs().withInverted(InvertedValue.CounterClockwise_Positive);

        TalonFXConfiguration trampMiddleRollerConfig = new TalonFXConfiguration()
                .withCurrentLimits(curLimits)
                .withMotorOutput(middleConfig)
                .withOpenLoopRamps(ramp);
        var topRollerCfg = new TalonFXConfiguration()
                .withMotorOutput(new MotorOutputConfigs().withInverted(InvertedValue.CounterClockwise_Positive))
                .withOpenLoopRamps(ramp) // todo remove this?
                .withMotionMagic(rollerConfig)
                .withSlot0(rollerslot0Condig)
                .withCurrentLimits(rollercurLimits);
        TalonUtil.applyAndCheckConfiguration(trampTopMotor, topRollerCfg);
        TalonUtil.applyAndCheckConfiguration(trampMiddleRollerMotor, trampMiddleRollerConfig);
        trampTab.addNumber("Tramp Middle Roller DC", trampMiddleRollerDCSignal::getValue);
        trampMiddleRollerMotor.setControl(trampMiddleRollerRequest);
        BaseStatusSignal.setUpdateFrequencyForAll(100,
                trampTopRollerDCSignal,
                trampExtensionSignal,
                trampMiddleRollerDCSignal,
                trampTopRollerCurrentStatorSignal,
                trampTopRollerCurrentStatorSignal,
                trampTopRollerPosSignal
        );
        trampTopMotor.optimizeBusUtilization();
        trampExtensionMotor.optimizeBusUtilization();
        trampMiddleRollerMotor.optimizeBusUtilization();
    }

    //debouncer in periodic, position is 0 && setpos = 0 ... set DC to 0
    public static Tramp getInstance() {
        if (instance == null) {
            instance = new Tramp();
        }
        return instance;
    }

    @Override
    public void periodic() {
        BaseStatusSignal.refreshAll(
                trampTopRollerDCSignal,
                trampExtensionSignal,
                trampMiddleRollerDCSignal,
                trampTopRollerCurrentStatorSignal,
                trampTopRollerCurrentSupplySignal,
                trampTopRollerPosSignal);
        extensionIsZero = trampExtensionDebouncer.calculate(Util.epsilonEquals(getExtensionInches(), 0, 0.1));
        beamBreak = trampBeamBreak.get();
        latestSensorDebounce = beamBeakDebouncer.calculate(beamBreak);
    }

    public boolean getTrampBeamBreak() {
        return latestSensorDebounce;
    }

    public double getExtensionInches() {
        return trampExtensionSignal.getValue() * IN_PER_ROT;
    }

    public void setTrampMiddleDutyCycle(double speed) {
        trampMiddleRollerMotor.setControl(trampMiddleRollerRequest.withOutput(speed));
    }

    public void setTrampTopDutyCycle(double speed) {
        trampTopMotor.setControl(trampShooterRequest.withOutput(speed));
    }


    public void recordTopRolerPos() {
        startingTopRollerPos = trampTopRollerPosSignal.getValue();
    }

    Follower follower = new Follower(RobotMap.TRAMP_MOTOR_CANID, false);

    public void setTopRollerPosition() {
        trampTopMotor.setControl(trampTopRollerRequest.withPosition(startingTopRollerPos + UPTAKE_ROTATIONS));
        trampMiddleRollerMotor.setControl(follower);
    }

    public boolean topRollerAtPosition() {
        return Math.abs((UPTAKE_ROTATIONS - (trampTopRollerPosSignal.getValue() - startingTopRollerPos))) < 1; // todo check if this tolerance good
    }

    public void setTrampExtensionMotor(Measure<Distance> position) {
//        if (extensionIsZero && position.in(Inches) == 0) {
//            trampExtensionMotor.setControl(extensionDC.withOutput(0));
//        } else {
            trampExtensionMotor.setControl(trampExtensionRequest.withPosition(position.in(Inches) / IN_PER_ROT));
//        }
    }

    public double getTopStatorCurrent() {
        return trampTopRollerCurrentStatorSignal.getValue();
    }

    public void setTrapServo(double trapServoPos) {
        trapServo.setPosition(trapServoPos);
    }

    public void coastMotor() {
        trampTopMotor.setControl(coastOut);
        trampExtensionMotor.setControl(coastOut);
        trampMiddleRollerMotor.setControl(coastOut);
    }

    public void brakeMotor() {
        trampTopMotor.setControl(staticBrake);
        trampExtensionMotor.setControl(staticBrake);
        trampMiddleRollerMotor.setControl(staticBrake);
    }

//    private ElevatorSim elevatorSim = new ElevatorSim(12.0 / 96.6, 12.0 / 37.0, DCMotor.getKrakenX60Foc(1), 0.0, Units.inchesToMeters(13.6), true, 0.0);
}

