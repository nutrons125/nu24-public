package frc.robot.subsystems.superstructure;

import com.ctre.phoenix6.BaseStatusSignal;
import com.ctre.phoenix6.StatusSignal;
import com.ctre.phoenix6.configs.*;
import com.ctre.phoenix6.controls.*;
import com.ctre.phoenix6.hardware.TalonFX;
import com.ctre.phoenix6.signals.GravityTypeValue;
import com.ctre.phoenix6.signals.InvertedValue;
import com.ctre.phoenix6.sim.ChassisReference;
import com.ctre.phoenix6.sim.TalonFXSimState;
import edu.wpi.first.math.filter.Debouncer;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.system.plant.DCMotor;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.units.Angle;
import edu.wpi.first.units.Measure;
import edu.wpi.first.wpilibj.RobotState;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.simulation.SingleJointedArmSim;
import edu.wpi.first.wpilibj.smartdashboard.Mechanism2d;
import edu.wpi.first.wpilibj.smartdashboard.MechanismLigament2d;
import edu.wpi.first.wpilibj.smartdashboard.MechanismRoot2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.util.Color8Bit;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.RobotMap;
import frc.robot.util.TalonUtil;
import frc.robot.util.Util;

import static edu.wpi.first.units.Units.*;


public class Hood extends SubsystemBase {
    private TalonFX hoodMotor;
    private double hoodSetpoint;
    private double OFFSET = 50.361;

    private final double FF_VALUE = 4. / (1923 / 60.0);
    private final double GEAR_RATIO = 75;
    public final StatusSignal<Double> hoodPositionSignal;
    public final StatusSignal<Double> hoodSetpointSignal;
    public final StatusSignal<Double> hoodVelocitySignal;

    private ShuffleboardTab hoodTab = Shuffleboard.getTab("Pivot");

    private MotionMagicVoltage hoodPositionVoltage = new MotionMagicVoltage(0)
            .withEnableFOC(true); // change
    private VoltageOut openLoop = new VoltageOut(0);
    private boolean isOpenLoop = true;

    private final Debouncer hoodDebouncer = new Debouncer(15);
    private boolean hoodIsZero = true;

    //singleton?
    private static Hood instance = null;

    public static Hood getInstance() {
        if (instance == null) {
            instance = new Hood();
        }
        return instance;
    }

    private Hood() {
        MotorOutputConfigs outputConfigs = new MotorOutputConfigs().withInverted(InvertedValue.CounterClockwise_Positive);
        var curLimits = new CurrentLimitsConfigs()
                .withSupplyCurrentLimitEnable(true)
                .withSupplyCurrentLimit(20)
                .withStatorCurrentLimitEnable(true)
                .withStatorCurrentLimit(80);

        Slot0Configs voltageConfigs = new Slot0Configs()
                .withKV(11.79 * 0.81)
                .withKG(0.39) // 0.36
                .withGravityType(GravityTypeValue.Arm_Cosine)
                .withKP(130) // was 165.,
                .withKI(0)
                .withKD(25);

        FeedbackConfigs feedbackConfigs = new FeedbackConfigs()
                .withRotorToSensorRatio(1)
                .withSensorToMechanismRatio(GEAR_RATIO);
        var velo = DegreesPerSecond.of(180);


        MotionMagicConfigs magicConfigs = new MotionMagicConfigs()
                .withMotionMagicCruiseVelocity(velo.in(RotationsPerSecond))
                .withMotionMagicAcceleration(velo.in(RotationsPerSecond) * 100) // Reach max vel in 1 second
                .withMotionMagicJerk(velo.in(RotationsPerSecond) * 100 * 10);
        var feederOutput = new MotorOutputConfigs().withInverted(InvertedValue.Clockwise_Positive);

        var softLimitConfig = new SoftwareLimitSwitchConfigs().withForwardSoftLimitEnable(true)
                .withForwardSoftLimitThreshold(65)
                .withReverseSoftLimitEnable(true)
                .withReverseSoftLimitThreshold(0);

        TalonFXConfiguration hoodMotorConfig = new TalonFXConfiguration()
                .withMotorOutput(outputConfigs)
                .withSlot0(voltageConfigs)
                .withMotionMagic(magicConfigs)
                .withFeedback(feedbackConfigs)
                .withMotorOutput(feederOutput)
                .withCurrentLimits(curLimits)
                .withSoftwareLimitSwitch(softLimitConfig); // Use a whole config object, so the rest is factory defaults

        hoodMotor = new TalonFX(RobotMap.HOOD_MOTOR_CAN_ID, "drivetrain");
        TalonUtil.applyAndCheckConfiguration(hoodMotor, hoodMotorConfig);

        hoodPositionSignal = hoodMotor.getPosition();
        hoodSetpointSignal = hoodMotor.getClosedLoopReference();
        hoodVelocitySignal = hoodMotor.getVelocity();

        BaseStatusSignal.setUpdateFrequencyForAll(
                100,
                hoodPositionSignal,
                hoodSetpointSignal,
                hoodVelocitySignal
        );
        hoodMotor.optimizeBusUtilization(); // magic optimization
        hoodTab.addDouble("Hood Position Degrees", () -> getPosition().getDegrees());
        hoodTab.addDouble("Hood Velo", hoodVelocitySignal::getValue);
        hoodTab.addDouble("Hood Setpoint", hoodSetpointSignal::getValue);
        hoodTab.addDouble("Hood Code Setpoint", () -> Units.rotationsToDegrees(hoodSetpoint));
        hoodSimState = hoodMotor.getSimState();
        hoodMotor.setPosition(0);
    }

    public void setDesiredAngle(Measure<Angle> angle) {
        hoodSetpoint = angle.in(Rotations);
        isOpenLoop = false;
    }


    private double latencyCompensatedPosition = 0;

    public Rotation2d getPosition() {
        return Rotation2d.fromRotations(latencyCompensatedPosition);
    }

    public double getErrorDegrees() {
        return (hoodSetpoint - latencyCompensatedPosition) * 360.0;
    }

    @Override
    public void periodic() {
        var retCode = BaseStatusSignal.refreshAll(
                hoodPositionSignal,
                hoodSetpointSignal,
                hoodVelocitySignal
        );
        latencyCompensatedPosition = BaseStatusSignal.getLatencyCompensatedValue(hoodPositionSignal, hoodVelocitySignal);
        SmartDashboard.putString("WaitForAllRes", retCode.toString());

        if (RobotState.isDisabled()) {
            return;
        }
        hoodIsZero = hoodDebouncer.calculate(Util.epsilonEquals(getPosition().getDegrees(), 0,0.5));
        if (!isOpenLoop) {
            hoodMotor.setControl(hoodPositionVoltage.withPosition(hoodSetpoint).withOverrideBrakeDurNeutral(true));
        } else {
            hoodMotor.setControl(openLoop.withOverrideBrakeDurNeutral(true));
        }
        hoodLig.setAngle(getPosition());
    }


    public void setOpenLoop(double v) {
        isOpenLoop = true;
        openLoop = openLoop.withOutput(v);
    }

    public Command setAngle(Rotation2d angle) {
        return this.run(() -> setDesiredAngle(Degrees.of(angle.times(1).getDegrees())));
    }

    private CoastOut coastOut = new CoastOut();
    private StaticBrake staticBrake = new StaticBrake();

    public void coastMotor() {
        hoodMotor.setControl(coastOut);
    }

    public void brakeMotor() {
        hoodMotor.setControl(staticBrake);
    }

    private static final double HOOD_LENGTH = Units.inchesToMeters(3);
    private static final double HOOD_WEIGHT = Units.lbsToKilograms(15);

    private SingleJointedArmSim hoodSim = new SingleJointedArmSim(DCMotor.getKrakenX60(1),
            GEAR_RATIO,
            SingleJointedArmSim.estimateMOI(Units.inchesToMeters(8), HOOD_WEIGHT),
            HOOD_LENGTH,
            Units.degreesToRadians(0),
            Units.degreesToRadians(68),
            true,
            Units.degreesToRadians(0));

    private Mechanism2d hoodMech = new Mechanism2d(Units.inchesToMeters(8), Units.inchesToMeters(9), new Color8Bit(77, 208, 225));
    private MechanismRoot2d hoodRoot = hoodMech.getRoot("we in da hood", 0, 0);
    private MechanismLigament2d hoodLig = hoodRoot.append(new MechanismLigament2d("Foo", HOOD_LENGTH, hoodSetpoint));
    private TalonFXSimState hoodSimState;

    @Override
    public void simulationPeriodic() {
        hoodSimState = hoodMotor.getSimState();
        hoodSimState.setSupplyVoltage(12);
        hoodSimState.Orientation = ChassisReference.Clockwise_Positive;
        hoodSim.setInputVoltage(hoodSimState.getMotorVoltage());
        hoodSim.update(0.02);
        hoodSimState.setRawRotorPosition(Units.radiansToRotations(hoodSim.getAngleRads()) * GEAR_RATIO);
        hoodSimState.setRotorVelocity(Units.radiansToRotations(hoodSim.getVelocityRadPerSec()) * GEAR_RATIO);
        SmartDashboard.putData("Mech2d", hoodMech);

        SmartDashboard.putNumber("Sim Position", Units.radiansToDegrees(hoodSim.getAngleRads()));
        SmartDashboard.putNumber("Sim Volts", hoodSimState.getMotorVoltage());
        SmartDashboard.putNumber("Sim Amps", hoodSim.getCurrentDrawAmps());
    }
}
