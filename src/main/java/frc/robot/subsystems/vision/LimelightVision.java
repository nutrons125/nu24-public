package frc.robot.subsystems.vision;

import edu.wpi.first.math.MathSharedStore;
import edu.wpi.first.math.VecBuilder;
import edu.wpi.first.math.Vector;
import edu.wpi.first.math.filter.Debouncer;
import edu.wpi.first.math.filter.LinearFilter;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.numbers.N3;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.networktables.DoubleArrayPublisher;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.units.Angle;
import edu.wpi.first.units.Measure;
import edu.wpi.first.units.MutableMeasure;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.FieldConstants;
import frc.robot.generated.TunerConstants;
import frc.robot.subsystems.superstructure.LEDStrip;

import java.util.Optional;

import static edu.wpi.first.units.Units.Degrees;

public class LimelightVision extends SubsystemBase {
    public static final String CAMERA_NAME = "limelight";
    private static final double LIMELIGHT_HEIGHT = 25.0;
//    private static final Vector<N3> STD_DEVS = VecBuilder.fill(0.06, 0.06, 27);
    private static final Vector<N3> STD_DEVS = VecBuilder.fill(0.3, 0.3, 27);
    private static LimelightVision instance = null;
    private static boolean USE_JSON = false;
    private static boolean USE_MT2 = true;
    private final LEDStrip led;
    DoubleArrayPublisher poseTopic = NetworkTableInstance.getDefault().getTable("LLCalc").getDoubleArrayTopic("LLPose").publish();
    NetworkTable limelightTable = NetworkTableInstance.getDefault().getTable("limelight");
    Optional<LimelightData> cook = Optional.empty();
    private double tx;
    private double ty;
    private boolean hasResults = false;
    private MutableMeasure<Angle> angleMeasTx = Degrees.of(0).mutableCopy();
    private double latency;
    private LimelightData latestData = new LimelightData(new Translation2d(), Degrees.of(0), 0, false, 0);

    private LimelightVision() {
        led = LEDStrip.getInstance();
        int[] validTags = {3, 4, 7, 8, 5, 6, 11};
        LimelightHelpers.SetFiducialIDFiltersOverride("limelight", validTags);
    }

    public static LimelightVision getInstance() {
        return instance == null ? (instance = new LimelightVision()) : instance;
    }

    static final double DEBOUNCE_TIME = 0.06;
    private final LinearFilter distanceFilter = LinearFilter.singlePoleIIR(DEBOUNCE_TIME, 0.02);
    private Debouncer canSeeDebounce = new Debouncer(DEBOUNCE_TIME, Debouncer.DebounceType.kBoth);
    private boolean canSeeForAiming = false;
    private double prevDist = 0;
    private double distVel = 0;
    private double prevTime = 0;
    private long tid = 0;

    @Override
    public void periodic() {
        boolean hasTarget = limelightTable.getEntry("tv").getInteger(0) > 0;
        canSeeForAiming = canSeeDebounce.calculate(hasTarget);
        if (hasTarget) {
            if (USE_JSON) {
                var results = LimelightHelpers.getLatestResults("limelight");
                if (results.targetingResults.targets_Fiducials.length > 0) {
                    tx = results.targetingResults.targets_Fiducials[0].tx;
                    ty = results.targetingResults.targets_Fiducials[0].ty;
                }
                latency = results.targetingResults.latency_capture + results.targetingResults.latency_capture + results.targetingResults.latency_jsonParse;
            } else {
                tx = limelightTable.getEntry("tx").getDouble(0);
                ty = limelightTable.getEntry("ty").getDouble(0);
                latency = limelightTable.getEntry("tl").getDouble(0);
                tid = limelightTable.getEntry("tid").getInteger(0);
            }
        } else {
            cook = Optional.empty();
        }
        latestData = updateData();
        double deltaDist = latestData.distanceToGoalM - prevDist;
        prevDist = latestData.distanceToGoalM;
        double deltaTime = MathSharedStore.getTimestamp() - prevTime;
        distVel = distanceFilter.calculate(deltaDist / deltaTime);
        prevTime = MathSharedStore.getTimestamp();
        hasResults = hasTarget;
        led.setLimelightHasTarget(hasTarget);
        SmartDashboard.putNumber("LL Dist Vel", distVel);
        poseTopic.set(new double[]{latestData.approxTranslation.getX(), latestData.approxTranslation.getY(), TunerConstants.DriveTrain.getState().Pose.getRotation().getDegrees()});
    }

    public boolean shouldAim() {
        return canSeeForAiming && distVel < Units.inchesToMeters(4);
    }

    public LimelightData getData() {
        return latestData;
    }

    private LimelightData updateData() {
        var dtState = TunerConstants.DriveTrain.getState();
        var latestPoseEst = dtState.Pose;
        if (USE_MT2) {
            boolean doRejectUpdate = false;
            var omegaDegPerSec = Units.radiansToDegrees(dtState.speeds.omegaRadiansPerSecond);
            LimelightHelpers.SetRobotOrientation("limelight", latestPoseEst.getRotation().getDegrees(), 0, 0, 0, 0, 0);
            LimelightHelpers.PoseEstimate mt2 = LimelightHelpers.getBotPoseEstimate_wpiBlue_MegaTag2("limelight");
            if(Math.abs(omegaDegPerSec) > 720) { // if our angular velocity is greater than 720 degrees per second, ignore vision updates
                doRejectUpdate = true;
            }
            if(mt2.tagCount == 0) {
                doRejectUpdate = true;
            }
            if((doRejectUpdate || !hasResults) && latestData != null) {
                return new LimelightData(latestData.approxTranslation, latestData.tx, 0, false, latestData.latency);
            }

            var tagToLookFor = FieldConstants.getSpeakerTagId();
            boolean hasSpeakerTag = false;
            double cameraDist = 0;
            for (var fiducial : mt2.rawFiducials) {
                if (fiducial.id == tagToLookFor) {
                    hasSpeakerTag = true;
                    angleMeasTx.mut_replace(fiducial.txnc, Degrees);
                    cameraDist = fiducial.distToCamera;
                    break;
                }
            }

            if (!hasSpeakerTag) {
                var translation = latestPoseEst.getTranslation();
                Translation2d modifiedSpeakerPos = FieldConstants.getSpeakerPosition();
                var speakerToRobot = modifiedSpeakerPos.minus(translation);
                var headingToGoal = speakerToRobot.getAngle();
                var expectedTx = headingToGoal.minus(latestPoseEst.getRotation());
                angleMeasTx.mut_replace(expectedTx.getDegrees(), Degrees);
                cameraDist = speakerToRobot.getNorm();
            }
            return new LimelightData(mt2.pose.getTranslation(), angleMeasTx, cameraDist, true, mt2.latency);
        }
        double limelightOffset = -Units.degreesToRadians(tx);
        double robotAngle = TunerConstants.DriveTrain.getState().Pose.getRotation().getRadians() + limelightOffset;
//        double distanceToGoal = (FieldConstants.getSpeakerZPos() - LIMELIGHT_HEIGHT) / Math.tan(Units.degreesToRadians(ty + 21.1)) + 2/12.;
        double distanceToGoal = (57.13 - LIMELIGHT_HEIGHT) / Math.tan(Units.degreesToRadians(ty + 21.7)) + 2 / 12.; // angle was 21.1
        // We are filtering all non-speaker tagsgoalRelativeY
        double goalRelativeY = Units.inchesToMeters(distanceToGoal * Math.sin(robotAngle));
        double goalRelativeX = Units.inchesToMeters(distanceToGoal * Math.cos(robotAngle));

        Translation2d t = new Translation2d(goalRelativeX * -1 + FieldConstants.getSpeakerPosition().getX(), goalRelativeY * -1 + FieldConstants.getSpeakerPosition().getY());
        if (!hasResults || tid != FieldConstants.getSpeakerTagId()) {
            return new LimelightData(t, angleMeasTx, 0, false, latency);
        }
        SmartDashboard.putNumber("LL Distance Feet", distanceToGoal / 12.);
        return new LimelightData(t, angleMeasTx.mut_replace(tx, Degrees), Units.inchesToMeters(distanceToGoal), true, latency);
    }

    public boolean canSee() {
        return hasResults;
    }

    public Vector<N3> getLimelightStdDevs(double distanceMeters) {
        if (distanceMeters > Units.feetToMeters(23)) {
            return VecBuilder.fill(Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE);
        }

//        return STD_DEVS.times(distanceMeters * distanceMeters / 100.); // was 30
        return STD_DEVS;
    }

    public double getLatency() {
        return latency * 1e-3;
    }

    public void limelightLedOn(){
        limelightTable.getEntry("ledMode").setNumber(3);
    }

    public void limelightLedOff() {
        limelightTable.getEntry("ledMode").setNumber(1);

    }

    @Override
    public void simulationPeriodic() {
        var translation = TunerConstants.DriveTrain.getState().Pose.getTranslation();
        Translation2d modifiedSpeakerPos = FieldConstants.getSpeakerPosition();
        var headingToGoal = modifiedSpeakerPos.minus(translation).getAngle(); //drivetrain.getState().Pose.getTranslation().minus(FieldConstants.getSpeakerPosition()).getAngle();
        var angleDiff = headingToGoal.minus(TunerConstants.DriveTrain.getState().Pose.getRotation());
        //         double distanceToGoal = (57.13 - LIMELIGHT_HEIGHT) / Math.tan(Units.degreesToRadians(ty + 21.1)) + 2 / 12.;
        if (Math.abs(angleDiff.getDegrees()) < 40) { // FOV!
            limelightTable.getEntry("tx").setDouble(-angleDiff.getDegrees());
            limelightTable.getEntry("tv").setDouble(1);
            var translationToSpeaker = translation.minus(FieldConstants.getSpeakerPosition());
            var angle = new Rotation2d(Units.metersToInches(translationToSpeaker.getNorm()), (57.13 - LIMELIGHT_HEIGHT));
            limelightTable.getEntry("ty").setDouble(angle.getDegrees() - 21.7);
        } else {
            limelightTable.getEntry("tv").setDouble(0);
        }
    }

    public record LimelightData(Translation2d approxTranslation, Measure<Angle> tx, double distanceToGoalM,
                                boolean valid, double latency) {
    }
}
