package frc.robot.subsystems.vision;

import edu.wpi.first.apriltag.AprilTagFieldLayout;
import edu.wpi.first.apriltag.AprilTagFields;
import edu.wpi.first.math.Matrix;
import edu.wpi.first.math.VecBuilder;
import edu.wpi.first.math.geometry.*;
import edu.wpi.first.math.numbers.N1;
import edu.wpi.first.math.numbers.N3;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.RobotBase;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import org.photonvision.EstimatedRobotPose;
import org.photonvision.PhotonCamera;
import org.photonvision.PhotonPoseEstimator;
import org.photonvision.simulation.PhotonCameraSim;
import org.photonvision.simulation.SimCameraProperties;
import org.photonvision.simulation.VisionSystemSim;
import org.photonvision.targeting.PhotonPipelineResult;

import java.io.IOException;
import java.util.Optional;
import java.util.Set;

import static edu.wpi.first.apriltag.AprilTagFieldLayout.loadFromResource;


public class PhotonVision extends SubsystemBase {
    public static final Transform3d ROBOT_TO_CAM = new Transform3d(new Translation3d(0, 0, Units.inchesToMeters(0)), new Rotation3d(0, Units.degreesToRadians(22.5), Units.degreesToRadians(0)));
    private static final Matrix<N3, N1> kSingleTagStdDevs = VecBuilder.fill(1.5, 1.5, 8);
    private static final Matrix<N3, N1> kMultiTagStdDevs = VecBuilder.fill(0.5, 0.5, 3.5);
    private static final double DIST_DROPOFF = 9;
    AprilTagFieldLayout aprilTagFieldLayout;
    // Construct PhotonPoseEstimator
    PhotonPoseEstimator photonPoseEstimator;
    PhotonCameraSim simCam;
    PhotonCamera cam;
    VisionSystemSim simVision = new VisionSystemSim("main");
    double lastEstTimestamp = 0;
    Set<Integer> BAD_TAGS = Set.of(5, 6, 11, 16, 12);
    private Pose2d lastMeasuredPose = new Pose2d();
    private PhotonPipelineResult latestResult = new PhotonPipelineResult();

    public PhotonVision(String camName, Transform3d robotToCam) {
        this.cam = new PhotonCamera(camName);

        try {
            aprilTagFieldLayout = loadFromResource(AprilTagFields.k2024Crescendo.m_resourceFile);
            photonPoseEstimator = new PhotonPoseEstimator(aprilTagFieldLayout, PhotonPoseEstimator.PoseStrategy.MULTI_TAG_PNP_ON_COPROCESSOR, cam, robotToCam);
        } catch (IOException e) {
            // Do nothing LOL
            return;
        }

        photonPoseEstimator.setMultiTagFallbackStrategy(PhotonPoseEstimator.PoseStrategy.CLOSEST_TO_LAST_POSE);

        if (RobotBase.isSimulation()) {
            simVision.addAprilTags(aprilTagFieldLayout);
            var cameraProp = new SimCameraProperties();
            cameraProp.setCalibration(960, 720, Rotation2d.fromDegrees(90));
            cameraProp.setCalibError(0.35, 0.10);
            cameraProp.setFPS(40);
            cameraProp.setAvgLatencyMs(10);
            cameraProp.setLatencyStdDevMs(15);
            // Create a PhotonCameraSim which will update the linked PhotonCamera's values with visible
            // targets.
            simCam = new PhotonCameraSim(cam, cameraProp);
            // Add the simulated camera to view the targets on this simulated field.
            simVision.addCamera(simCam, ROBOT_TO_CAM);

            simCam.enableDrawWireframe(true);
        }
    }

    @Override
    public void periodic() {
        latestResult = cam.getLatestResult();
        latestResult.setTimestampSeconds(Timer.getFPGATimestamp() - latestResult.getLatencyMillis() * 1e-3);
    }

    public void updateSimPose(Pose2d pose) {
        simVision.update(pose);
    }

    public Optional<EstimatedRobotPose> getEstimatedGlobalPose() {
        var visionEst = photonPoseEstimator.update();
        double latestTimestamp = latestResult.getTimestampSeconds();
        photonPoseEstimator.setLastPose(lastMeasuredPose);
        boolean newResult = Math.abs(latestTimestamp - lastEstTimestamp) > 1e-5;
        if (RobotBase.isSimulation()) {
            visionEst.ifPresentOrElse(
                    est ->
                            simVision.getDebugField()
                                    .getObject("VisionEstimation")
                                    .setPose(est.estimatedPose.toPose2d()),
                    () -> {
                        if (newResult) simVision.getDebugField().getObject("VisionEstimation").setPoses();
                    });
        }
        if (newResult) lastEstTimestamp = latestTimestamp;
        if (visionEst.isPresent()) {
            lastMeasuredPose = visionEst.get().estimatedPose.toPose2d();
        }
        return visionEst;
    }

    public PhotonPipelineResult getLatestResult() {
        return latestResult;
    }

    public Matrix<N3, N1> getEstimationStdDevs(Pose2d estimatedPose) {
        var estStdDevs = kSingleTagStdDevs;
        var targets = getLatestResult().getTargets();

        int numTags = 0;
        double avgDist = 0;
        boolean hasBadTag = false;
        double worstAmb = 0;

//        if (DriverStation.isTeleop() && !getLatestResult().getMultiTagResult().fiducialIDsUsed.isEmpty()) {
//            boolean hasSpeakerTag = false;
//            estStdDevs = VecBuilder.fill(0.05, 0.05, 8);
//            for (var tag : getLatestResult().getMultiTagResult().fiducialIDsUsed) {
//                if (tag == 3 || tag == 4 || tag == 7 || tag == 8) {
//                    hasSpeakerTag = true;
//                }
//                var tagPose = photonPoseEstimator.getFieldTags().getTagPose(tag);
//
//                avgDist +=
//                        tagPose.get().toPose2d().getTranslation().getDistance(estimatedPose.getTranslation());
//            }
//            if (hasSpeakerTag) {
//                return estStdDevs.times(1 + (avgDist * avgDist / 30));
//            }
//        }
        for (var tgt : targets) {
            var tagPose = photonPoseEstimator.getFieldTags().getTagPose(tgt.getFiducialId());
            if (BAD_TAGS.contains(tgt.getFiducialId())) {
                hasBadTag = true;
            }
            if (tgt.getPoseAmbiguity() > worstAmb) {
                worstAmb = tgt.getPoseAmbiguity();
            }
            if (tagPose.isEmpty()) continue;
            numTags++;
            avgDist +=
                    tagPose.get().toPose2d().getTranslation().getDistance(estimatedPose.getTranslation());
        }

        if (worstAmb > 0.7) {
            return VecBuilder.fill(Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE);
        }
        if (numTags == 0) return estStdDevs;
        avgDist /= numTags;
        // Decrease std devs if multiple targets are visible
        if (numTags > 1 || !getLatestResult().getMultiTagResult().fiducialIDsUsed.isEmpty())
            estStdDevs = kMultiTagStdDevs;
        // Increase std devs based on (average) distance
        if (numTags == 1 && avgDist > 4.75)
            estStdDevs = VecBuilder.fill(Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE);
        else estStdDevs = estStdDevs.times(1 + (avgDist * avgDist / DIST_DROPOFF));
        if (hasBadTag) {
            estStdDevs = estStdDevs.times(100);
        }
        return estStdDevs;
    }
}
