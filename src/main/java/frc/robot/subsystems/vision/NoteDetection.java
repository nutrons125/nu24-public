package frc.robot.subsystems.vision;

import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import java.util.Optional;

public class NoteDetection extends SubsystemBase {
    private static final double LIMELIGHT_HEIGHT = 20.5; //todo get real num
    private static final double LIMELIGHT_ANGLE = 40.5;
    private static NoteDetection instance;
    NetworkTable limelightTable = NetworkTableInstance.getDefault().getTable("limelight-note");
    private double tx;
    private double ty;
    private Optional<Translation2d> noteLocation;

    private NoteDetection() {
        //hi
    }

    public static NoteDetection getInstance() {
        return instance == null ? (instance = new NoteDetection()) : instance;
    }

    public void periodic() {
        if (limelightTable.getEntry("tv").getInteger(0) > 0) {
            tx = limelightTable.getEntry("tx").getDouble(0);
            ty = limelightTable.getEntry("ty").getDouble(0);
            double d = LIMELIGHT_HEIGHT / Math.tan(Units.degreesToRadians(ty + LIMELIGHT_ANGLE));
            double x = d * Math.cos(Units.degreesToRadians(tx));
            double y = d * Math.sin(Units.degreesToRadians(tx));
            SmartDashboard.putNumber("Note detection x", x);
            SmartDashboard.putNumber("Note detection y", y);
            noteLocation = Optional.of(new Translation2d(x, y));
        } else {
            noteLocation = Optional.empty();
        }
    }

    public Optional<Translation2d> notePose() {
        return noteLocation;
    }
}
