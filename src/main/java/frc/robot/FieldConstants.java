package frc.robot;

import edu.wpi.first.apriltag.AprilTagFields;
import edu.wpi.first.math.geometry.*;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.DriverStation;
import frc.robot.subsystems.superstructure.MovingShotCommand;

public class FieldConstants {

    public static Translation2d shooterPositionOffset = new Translation2d(0, 0);

    public static Translation2d redSpeakerPosition = AprilTagFields.k2024Crescendo.loadAprilTagLayoutField().getTagPose(4).get().getTranslation().toTranslation2d();
    public static Translation2d blueSpeakerPosition = AprilTagFields.k2024Crescendo.loadAprilTagLayoutField().getTagPose(7).get().getTranslation().toTranslation2d();
    private static Pose3d tagPose = null;
    public static double getSpeakerZPos() {
        if (tagPose == null) {
            tagPose = AprilTagFields.k2024Crescendo.loadAprilTagLayoutField().getTagPose(getSpeakerTagId()).get();
        }
        return Units.metersToInches(tagPose.getZ());
    }

    public static Translation2d getCannonShotTranslation() {
        return getSpeakerPosition().plus(new Translation2d(2*getFlip(), 1.5));
    }

    public static Translation2d getSpeakerPosition() {
        if (DriverStation.getAlliance().isPresent() && DriverStation.getAlliance().get() == DriverStation.Alliance.Red) {
            return redSpeakerPosition;
        } else {
            return blueSpeakerPosition;
        }
    }

    public static Translation2d getSubwooferPosition() {
        if (DriverStation.getAlliance().isPresent() && DriverStation.getAlliance().get() == DriverStation.Alliance.Red) {
            return redSpeakerPosition.minus(new Translation2d(Units.feetToMeters(4.083333333333334), 0).minus(shooterPositionOffset));
        }
        return blueSpeakerPosition.plus(new Translation2d(Units.feetToMeters(4.083333333333334), 0).plus(shooterPositionOffset));
    }

    public static boolean shouldFlip() {
        return (DriverStation.getAlliance().map((a) -> a == DriverStation.Alliance.Red).orElse(true));
    }

    public static double getFlip() {
        return DriverStation.getAlliance().map(alliance -> alliance == DriverStation.Alliance.Blue).orElse(true) ? 1 : -1;
    }

    public static boolean isBlue() {
        return DriverStation.getAlliance().map(alliance -> alliance == DriverStation.Alliance.Blue).orElse(true);
    }

    public static Pose2d getStagePose(StageLocation stage) {
        int tagId = shouldFlip() ? stage.tagIdRed : stage.tagIdBlue;
        Pose2d tagPose = AprilTagFields.k2024Crescendo.loadAprilTagLayoutField().getTagPose(tagId).get().toPose2d();
        Transform2d offset = new Transform2d(new Translation2d(Units.inchesToMeters(18), 0), Rotation2d.fromDegrees(0));
        return tagPose.plus(offset);
    }

    public static int getSpeakerTagId() {
        if (isBlue()) {
            return 7;
        }
        return 4;
    }

    public static enum StageLocation {
        RIGHT(12, 16),
        LEFT(11, 15),
        MIDDLE(13, 14);

        private int tagIdRed, tagIdBlue;

        StageLocation(int red, int blue) {
            this.tagIdBlue = blue;
            this.tagIdRed = red;
        }
    }
}
