package frc.robot;

public class RobotMap {

    public RobotMap() {

    }

    public static final int BACK_LEFT_DRIVE_MOTOR_CANID = 10;
    public static final int BACK_LEFT_STEER_MOTOR_CANID = 20;
    public static final int BACK_LEFT_ENCODER_CANID = 30;

    public static final int BACK_RIGHT_DRIVE_MOTOR_CANID = 11;
    public static final int BACK_RIGHT_STEER_MOTOR_CANID = 21;
    public static final int BACK_RIGHT_ENCODER_CANID = 31;

    public static final int FRONT_LEFT_DRIVE_MOTOR_CANID = 12;
    public static final int FRONT_LEFT_STEER_MOTOR_CANID = 22;
    public static final int FRONT_LEFT_ENCODER_CANID = 32;

    public static final int FRONT_RIGHT_DRIVE_MOTOR_CANID = 13;
    public static final int FRONT_RIGHT_STEER_MOTOR_CANID = 23;
    public static final int FRONT_RIGHT_ENCODER_ID = 34;

    public static final int TRAMP_MOTOR_CANID = 45;
    public static final int TRAMP_EXTENSION_MOTOR_CANID = 51;

    public static final int MAIN_INTAKE_MOTOR_CANID = 47;
    public static final int TRIFORCE_BOTTOM_MOTOR_CANID = 43;
    public static final int FEEDER_MOTOR_CANID = 42;
    public static final int HOOD_MOTOR_CAN_ID = 44;
    public static final int LEFT_SHOOTER_CANID = 41;
    public static final int RIGHT_SHOOTER_CANID = 48;
    public static final int TRAMP_MIDDLE_ROLLER_CANID = 52;
    public static final int CLIMBER_MOTOR_CANID = 46;

    public static final int CANDLE_CAN_ID = 59;

    public static final int FRONT_INTAKE_BEAM_BREAK_ID = 9;
    public static final int REAR_INTAKE_BEAM_BREAK_ID = 8;
    public static final int TRAMP_BEAM_BREAK_ID = 7;

    public static final int TRAP_SERVO_CHANNEL = 3;
}
