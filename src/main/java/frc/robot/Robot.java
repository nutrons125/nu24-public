// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import com.ctre.phoenix6.SignalLogger;
import edu.wpi.first.math.MathSharedStore;
import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.Matrix;
import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.math.geometry.*;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.numbers.N1;
import edu.wpi.first.math.numbers.N3;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.units.Angle;
import edu.wpi.first.units.Distance;
import edu.wpi.first.units.MutableMeasure;
import edu.wpi.first.units.Velocity;
import edu.wpi.first.wpilibj.*;
import edu.wpi.first.wpilibj.event.EventLoop;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.button.CommandPS4Controller;
import frc.robot.generated.TunerConstants;
import frc.robot.subsystems.drivetrain.AimAtPosition;
import frc.robot.subsystems.drivetrain.CommandSwerveDrivetrain;
import frc.robot.subsystems.drivetrain.LimelightAim;
import frc.robot.subsystems.drivetrain.LimelightNoteAim;
import frc.robot.subsystems.superstructure.*;
import frc.robot.subsystems.vision.NoteDetection;
import frc.robot.subsystems.vision.LimelightVision;
import frc.robot.subsystems.vision.PhotonVision;
import org.photonvision.EstimatedRobotPose;

import java.util.Deque;
import java.util.Optional;

import static edu.wpi.first.units.Units.*;

public class Robot extends TimedRobot {
    private static final double MAX_SPEED = 6; // 6 meters per second desired top speed
    private static final double MAX_ANGULAR_RATE = 1.5 * Math.PI; // 3/4 of a rotation per second max angular velocity

    private static final Rotation2d NORTH = Rotation2d.fromDegrees(60);//was 0
    private static final Rotation2d EAST = Rotation2d.fromDegrees(270);
    private static final Rotation2d SOUTH = Rotation2d.fromDegrees(300);//was 180
    private static final Rotation2d WEST = Rotation2d.fromDegrees(90);

    private final CommandPS4Controller driverController = new CommandPS4Controller(0); // My joystick
    private final CommandPS4Controller operatorController = new CommandPS4Controller(1); // My joystick
    private final CommandPS4Controller testController = new CommandPS4Controller(2); // My joystick
    private final boolean USE_VISION = RobotBase.isReal();
    private final CommandSwerveDrivetrain drivetrain = TunerConstants.DriveTrain; // My drivetrain
    public PhotonVision rightVision;
    public PhotonVision leftVision;
    TriTake tritake = TriTake.getInstance();
    // driving in open loop
//    private Command runAuto = drivetrain.getAutoPath("Tests");
    MutableMeasure<Distance> speakerDistanceHoriz = Inches.of(0).mutableCopy();
    Autos autos = new Autos();
    private Telemetry logger;
    private Command autonomousCommand;
    private Superstructure superstructure;
    private Climber climber = Climber.getInstance();
    private ChassisSpeeds joySpeeds;
    private EventLoop loop = new EventLoop();
    private Hood hood = Hood.getInstance();
    private LEDStrip led = LEDStrip.getInstance();
    NoteDetection noteDetection = NoteDetection.getInstance();
    private MutableMeasure<Velocity<Distance>> xVelJoystick = FeetPerSecond.of(0).mutableCopy();
    private MutableMeasure<Velocity<Distance>> yVelJoystick = FeetPerSecond.of(0).mutableCopy();
    private MutableMeasure<Velocity<Angle>> thetaVelJoystick = RadiansPerSecond.of(0).mutableCopy();
    private static boolean USE_SLEWRATE = false;
    private static final boolean RELATIVE_AIM = false;
    private SlewRateLimiter driveSlew = new SlewRateLimiter(5, -10, 0);
    //placeholder values cuz idk
    private SlewRateLimiter idleSlew = new SlewRateLimiter(5, -10, 0);
    private SlewRateLimiter notIdleSlew = new SlewRateLimiter(5, 10, 0);

    public Robot() {
    }

    public static double getExponential(final double input, final double exponent, final double weight, final double deadband) {
        if (Math.abs(input) < deadband) {
            return 0;
        }
        double sign = Math.signum(input);
        double v = Math.abs(input);

        double a = weight * Math.pow(v, exponent) + (1 - weight) * v;
        double b = weight * Math.pow(deadband, exponent) + (1 - weight) * deadband;
        v = (a - 1 * b) / (1 - b);

        v *= sign;
        return v;
    }

    /**
     * Origin is always blue corner, so we need to flip certain signs if on red.
     */
    private double getFlip() {
        return DriverStation.getAlliance().map(alliance -> alliance == DriverStation.Alliance.Blue).orElse(true) ? 1 : -1;
    }

    private ChassisSpeeds getSpeeds() {
        double xInput = -driverController.getLeftY() * getFlip(),
                yInput = -driverController.getLeftX() * getFlip();
        double omegaInput = getExponential(-driverController.getRightX(), 4.3, .5, .1) * MAX_ANGULAR_RATE;
        double magInput = Math.hypot(xInput, yInput);
        if (USE_SLEWRATE) {
            if (superstructure.isIdle()) {
                magInput = idleSlew.calculate(magInput);
            }
            else {
                magInput = notIdleSlew.calculate(magInput);
            }
        }
        double magnitude = MathUtil.clamp(
                getExponential(magInput, 3.6, 0.75, .1),
                -1, 1) * MAX_SPEED;
        double angle = Math.atan2(yInput, xInput);
        xInput = Math.cos(angle) * magnitude;
        yInput = Math.sin(angle) * magnitude;

        return new ChassisSpeeds(xInput, yInput, omegaInput);
    }

    @Override
    public void robotInit() {
        SignalLogger.setPath("/media/sda1/");
        superstructure = Superstructure.getInstance();
        logger = new Telemetry(MAX_SPEED);
        configureBindings();
        DataLogManager.start("/media/sda1/");
        DataLogManager.logNetworkTables(true);
        DriverStation.startDataLog(DataLogManager.getLog(), true);

        drivetrain.getDaqThread().setThreadPriority(99);
        autos.initializeAutos();
        autos.putChooserOnDashboard();

        if (USE_VISION) {
            //26.846 FOR OLD CAMERA MOUNT
            /* right */
            rightVision = new PhotonVision("Arducam_OV928_Back", new Transform3d(new Translation3d(Units.inchesToMeters(1.163), Units.inchesToMeters(-7.907), Units.inchesToMeters(26.646 / 2)), new Rotation3d(Units.degreesToRadians(-16.5), Units.degreesToRadians(-23.8), Units.degreesToRadians(27.3))));
            /* left */
            leftVision = new PhotonVision("Arducam_OV9281_Front", new Transform3d(new Translation3d(Units.inchesToMeters(1.163), Units.inchesToMeters(7.664), Units.inchesToMeters(26.646 / 2)), new Rotation3d(Units.degreesToRadians(19.2), Units.degreesToRadians(-26.5), Units.degreesToRadians(-28.5))));
        }
        //26.846 height

//                    frontVision = new Vision("Arducam_OV9281_Front", new Transform3d(new Translation3d(0, Units.inchesToMeters(-9.23), 0), new Rotation3d(Units.degreesToRadians(37.05), Units.degreesToRadians(38), Units.degreesToRadians(25.07))));
//            backVision = new Vision("Arducam_OV9281_Back", new Transform3d(new Translation3d(0, Units.inchesToMeters(9.23), 0), new Rotation3d(Units.degreesToRadians(37.05), Units.degreesToRadians(38), Units.degreesToRadians(-25.07))));
        RobotController.setBrownoutVoltage(6.1);
        superstructure.setTrampServoPos(superstructure.SERVO_UP);
    }

    private MutableMeasure<Angle> demoPos = Degrees.zero().mutableCopy();

    @Override
    public void robotPeriodic() {
        double demoJoystickPos = -testController.getLeftY() * 67;
        if (demoJoystickPos < 0){
            demoJoystickPos = 0;
        }
        SmartDashboard.putNumber("Demo", demoJoystickPos);
        superstructure.setDemoModePosition(demoPos.mut_replace(demoJoystickPos, Degrees));
        DriverStation.silenceJoystickConnectionWarning(true);
        var translationToSpeaker = drivetrain.getState().Pose.getTranslation().minus(FieldConstants.getSpeakerPosition());
        speakerDistanceHoriz.mut_replace(translationToSpeaker.getNorm(), Meters);
        SmartDashboard.putNumber("Distance to Speaker", speakerDistanceHoriz.in(Feet));
        SmartDashboard.putNumber("Driver X", xVelJoystick.in(FeetPerSecond));
        SmartDashboard.putNumber("Driver Y", yVelJoystick.in(FeetPerSecond));
        CommandScheduler.getInstance().run();
        SmartDashboard.putString("DT Command", drivetrain.getCurrentCommand().getName());
        if (superstructure.getCurrentCommand() != null) {
            SmartDashboard.putString("SS Cmd", superstructure.getCurrentCommand().getName());
        } else {
            SmartDashboard.putString("SS Cmd", "NULL");
        }

        if (USE_VISION) {
            visionBlock:
            {
                if (LimelightVision.getInstance().canSee() && !DriverStation.isAutonomous()) {
                    var data = LimelightVision.getInstance().getData();
                    if (data.valid()) {
                        drivetrain.addVisionMeasurement(
                                new Pose2d(data.approxTranslation(), drivetrain.getState().Pose.getRotation()),
                                MathSharedStore.getTimestamp() - LimelightVision.getInstance().getLatency(),
                                LimelightVision.getInstance().getLimelightStdDevs(data.distanceToGoalM())
                        );
                        break visionBlock;
                    }
                }

                Optional<EstimatedRobotPose> visionPoseOpt = rightVision.getEstimatedGlobalPose();
                EstimatedRobotPose measurement;
                Matrix<N3, N1> measurementStdDevs;
                if (visionPoseOpt.isPresent()) {
                    measurement = visionPoseOpt.get();
                    measurementStdDevs = rightVision.getEstimationStdDevs(measurement.estimatedPose.toPose2d());
                    drivetrain.addVisionMeasurement(measurement.estimatedPose.toPose2d(),
                            measurement.timestampSeconds,
                            measurementStdDevs);
                }
                visionPoseOpt = leftVision.getEstimatedGlobalPose();
                if (visionPoseOpt.isPresent()) {
                    measurement = visionPoseOpt.get();
                    measurementStdDevs = leftVision.getEstimationStdDevs(measurement.estimatedPose.toPose2d());
                    drivetrain.addVisionMeasurement(measurement.estimatedPose.toPose2d(),
                            measurement.timestampSeconds,
                            measurementStdDevs);
                }
            }

            if (RobotBase.isSimulation()) {
                rightVision.updateSimPose(drivetrain.getState().Pose);
                leftVision.updateSimPose(drivetrain.getState().Pose);
            }
        }

        joySpeeds = getSpeeds();
        xVelJoystick.mut_replace(joySpeeds.vxMetersPerSecond, MetersPerSecond);
        yVelJoystick.mut_replace(joySpeeds.vyMetersPerSecond, MetersPerSecond);
        thetaVelJoystick.mut_replace(joySpeeds.omegaRadiansPerSecond, RadiansPerSecond);

        led.update();
    }

    @Override
    public void disabledInit() {
        autos.putChooserOnDashboard();
        led.off();
        led.update();
    }

    @Override
    public void disabledPeriodic() {
        if (autonomousCommand != autos.getAutoSelected()) {
            var maybeStartLocation = autos.getStartingAutoPose();
            if (maybeStartLocation.isPresent()) {
                drivetrain.seedFieldRelative(maybeStartLocation.get());
            }
        }
        autonomousCommand = autos.getAutoSelected();
    }

    @Override
    public void disabledExit() {
    }

    @Override
    public void autonomousInit() {
        superstructure.startAuto();
        autos.putChooserOnDashboard();

        autonomousCommand = getAutonomousCommand();
        if (RobotBase.isSimulation()) {
            drivetrain.seedFieldRelative(new Pose2d(new Translation2d(1.4, 6.2), new Rotation2d(0.0)));
        }
        if (autonomousCommand != null) {
            autonomousCommand.schedule();
        }
    }

    @Override
    public void autonomousPeriodic() {
    }

    @Override
    public void autonomousExit() {
        superstructure.stopAuto();
    }

    @Override
    public void teleopInit() {
        // Explicitly start the logger
        SignalLogger.start();
        if (autonomousCommand != null) {
            autonomousCommand.cancel();
        }
//        superstructure.clear();
        // drivetrain.seedFieldRelative(new Pose2d(0, 0, Rotation2d.fromDegrees(0)));
    }

    @Override
    public void teleopPeriodic() {
        SmartDashboard.putNumber("Omega", drivetrain.getCurrentRobotChassisSpeeds().omegaRadiansPerSecond);
    }

    @Override
    public void teleopExit() {
        // Explicitly stop logging
// If the user does not call stop(), then it's possible to lose the last few seconds of data
        SignalLogger.stop();
    }
    /* Path follower */

    @Override
    public void simulationPeriodic() {
        tritake.setFrontSimBeamBroken(operatorController.square().getAsBoolean());
        tritake.setRearSimBeamBroken(operatorController.square().getAsBoolean());
    }

    @Override
    public void testInit() {
        CommandScheduler.getInstance().cancelAll();
    }

    @Override
    public void testPeriodic() {
    }

    @Override
    public void testExit() {
    }

    private void driveFacingAngle(Rotation2d angle) {
        drivetrain.faceAngleFieldCentric(angle,
                xVelJoystick,
                yVelJoystick,
                false);
    }

    private Command faceStaticAngle(Rotation2d angle) {
        return drivetrain.run(() -> driveFacingAngle(angle));
    }

    private void configureBindings() {
//        shooter.setDefaultCommand(shooter.stopCommand().withName("Default, stopped"));

        drivetrain.setDefaultCommand( // Drivetrain will execute this command periodically
                drivetrain.run(() -> drivetrain.driveFieldCentric(xVelJoystick,
                                yVelJoystick,
                                thetaVelJoystick)
                        ).ignoringDisable(true)
                        .withName("DtDefault"));

        // reset the field-centric heading on left bumper press
//        var subwooferPose = FieldConstants.redSpeakerPosition.minus(new Translation2d(Units.feetToMeters(3), 0));

        driverController.options().onTrue(drivetrain.runOnce(() -> drivetrain.seedFieldRelative(new Pose2d(FieldConstants.getSubwooferPosition(), Rotation2d.fromDegrees(FieldConstants.shouldFlip() ? 0 : 180)))).ignoringDisable(true));

        // if (Utils.isSimulation()) {
        //   drivetrain.seedFieldRelative(new
        //   Pose2d(new Translation2d(), Rotation2d.fromDegrees(90)));
        // }
        drivetrain.registerTelemetry(logger::telemeterize);//telemetrizzle

        driverController.triangle().whileTrue(faceStaticAngle(NORTH.times(getFlip())));
        driverController.circle().whileTrue(faceStaticAngle(WEST.times(getFlip())));
        driverController.cross().whileTrue(new LimelightNoteAim(drivetrain, noteDetection, superstructure, () -> xVelJoystick, () -> yVelJoystick));
        driverController.square().whileTrue(faceStaticAngle(EAST.times(getFlip())));
//        new Trigger(RobotState::isDisabled)
//                .debounce(3)
//                .onTrue(new InstantCommand(superstructure::disable, superstructure).ignoringDisable(true));

        driverController
                .L1()
                .and(driverController.L2())
                .and(driverController.L3())
                .and(driverController.R1())
                .and(driverController.R2())
                .and(driverController.R3())
                .onTrue(new InstantCommand(() -> {
                    throw new RestartRobotCodeException();
                }).ignoringDisable(true));

        driverController.R1().onTrue(new InstantCommand(() -> superstructure.setWantIntakeToShooter()));
        driverController.R1().onFalse(new InstantCommand(() -> superstructure.cancelAction()));

        var driverStationaryShot = driverController.R2();
        driverStationaryShot.onTrue(MovingShotCommand.relativeShot(drivetrain, superstructure, () -> xVelJoystick, () -> yVelJoystick, () -> false, RELATIVE_AIM, false));
        driverStationaryShot.onFalse(new InstantCommand(() -> superstructure.idle(), superstructure));//        shooter.setDefaultCommand(new RunCommand(() -> shooter.setShooterSimBeamBroken(false), shooter));

        var driverAndYesBoop = operatorController.R2();
        driverAndYesBoop.onTrue(new FixedShotCommand(drivetrain, superstructure, () -> xVelJoystick.in(MetersPerSecond), () -> yVelJoystick.in(MetersPerSecond), () -> true, FixedShotCommand.ShotType.SLOW));
        driverAndYesBoop.onFalse(new InstantCommand(() -> superstructure.idle(), superstructure));//        shooter.setDefaultCommand(new RunCommand(() -> shooter.setShooterSimBeamBroken(false), shooter));

        driverController.L1().whileTrue(new FixedShotCommand(drivetrain, superstructure, () -> false, FixedShotCommand.ShotType.CANNON).alongWith(new AimAtPosition(drivetrain, FieldConstants.getCannonShotTranslation(), () -> xVelJoystick, () -> yVelJoystick)));

        driverController.L2().onTrue(new InstantCommand(() -> superstructure.setWantShootToTramp()));
        driverController.L2().onFalse(superstructure.runOnce(superstructure::cancelDesirdAction));

        driverController.povUp().whileTrue(drivetrain.pathFind(FieldConstants.getStagePose(FieldConstants.StageLocation.MIDDLE)));
        driverController.povLeft().whileTrue(drivetrain.pathFind(FieldConstants.getStagePose(FieldConstants.StageLocation.LEFT)));
        driverController.povRight().whileTrue(drivetrain.pathFind(FieldConstants.getStagePose(FieldConstants.StageLocation.RIGHT)));


        operatorController.options().or(testController.options()).onTrue(superstructure.runOnce(superstructure::coast).alongWith(drivetrain.runOnce(drivetrain::coastAll)).ignoringDisable(true));
        operatorController.share().or(testController.options()).onTrue(superstructure.runOnce(superstructure::brake).alongWith(drivetrain.runOnce(drivetrain::brakeAll)).ignoringDisable(true));

        operatorController.circle().onTrue(superstructure.runOnce(superstructure::shooterToTramp));
        operatorController.circle().onFalse(superstructure.runOnce(superstructure::cancelAction));

        operatorController.square().onTrue(superstructure.runOnce(superstructure::setSmallUnclimb));

        operatorController.R1().onTrue(superstructure.runOnce(() -> superstructure.spinShooter(0.75)));
        operatorController.R1().onFalse(superstructure.runOnce(() -> superstructure.spinShooter(0.0)));

        operatorController.L2().whileTrue(superstructure.run(superstructure::outtake));
        operatorController.L2().onFalse(superstructure.runOnce(superstructure::cancelAndClear));


        operatorController.L1().whileTrue(superstructure.run(superstructure::outtakeWithouthood));
        operatorController.L1().onFalse(superstructure.runOnce(superstructure::cancelAndClear));


        operatorController.povDown().onTrue(superstructure.runOnce(superstructure::setPreRaiseClimb));
        operatorController.povLeft().onTrue(superstructure.run(superstructure::setWantClimb));
        operatorController.povUp().onTrue(superstructure.run(() -> superstructure.setConfirmClimb(true)));
        operatorController.cross().onTrue(superstructure.run(superstructure::abortClimb));

        operatorController.triangle().or(driverController.share()).onTrue(superstructure.runOnce(superstructure::resetState));


//        driverController.share().whileTrue(drivetrain.pathFindToAutoStartPos());

//        testController.triangle().onTrue(superstructure.runOnce(() -> superstructure.setTrampExtensionPosition(Inches.of(15.61))));
//        testController.cross().onTrue(superstructure.runOnce(() -> superstructure.setTrampExtensionPosition(Inches.of(0))));
        testController.triangle().onTrue(superstructure.runOnce(() -> superstructure.setDemoMode(true)));
        testController.square().onFalse(superstructure.runOnce(() -> superstructure.setDemoMode(false)));

        testController.R1().onTrue(superstructure.runOnce(() -> superstructure.setDemoAimed(true)));

        testController.L1().onTrue(superstructure.runOnce(() -> superstructure.dumbFloorToTramp()));
        testController.L1().onFalse(superstructure.runOnce(() -> superstructure.dumbIdle()));

        testController.L2().whileTrue(new ShootTakeCommand(drivetrain, superstructure));
        testController.L2().onFalse(superstructure.runOnce(superstructure::idle));

        testController.square().whileTrue(new LimelightAim(drivetrain, LimelightVision.getInstance()).alongWith(superstructure.runOnce(() -> superstructure.setApriltagTracking(true))
                .andThen(superstructure.runOnce(() -> superstructure.setDemoMode(true)))));

        testController.povUp().onTrue(superstructure.runOnce(() -> superstructure.setTrampServoPos(superstructure.SERVO_UP)));
        testController.povDown().onTrue(superstructure.runOnce(() -> superstructure.setTrampServoPos(superstructure.SERVO_DOWN)));

//        testController.povDown().onTrue(superstructure.run(superstructure::climbDown));
//        testController.povRight().or(testController.povLeft()).onTrue(superstructure.run(superstructure::hooksUp));
//        testController.povUp().onTrue(superstructure.run(superstructure::climbUp));

        testController.options().or(testController.options()).onTrue(new InstantCommand(() -> superstructure.coast()).ignoringDisable(true));
        testController.share().or(testController.options()).onTrue(new InstantCommand(() -> superstructure.brake()).ignoringDisable(true));

        testController.circle().onTrue(superstructure.run(() -> superstructure.manualShooting(Degrees.of(26), RPM.of(0), RPM.of(0))));
        testController.circle().onFalse(superstructure.runOnce(superstructure::idle));
//        climber.setDefaultCommand(new InstantCommand(() -> superstructure.setClimberSpeed(-testController.getLeftY()), superstructure));

    }

    public Command getAutonomousCommand() {
        /* First put the drivetrain into auto run mode, then run the auto */
        return autos.initializeAuto();
    }

    private static class RestartRobotCodeException extends RuntimeException {
        RestartRobotCodeException() {
            super("Restarting Robot Code...");
        }
    }
}
