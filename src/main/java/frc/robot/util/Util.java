package frc.robot.util;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Twist2d;
import edu.wpi.first.units.Angle;
import edu.wpi.first.units.Measure;

import static edu.wpi.first.units.Units.Radians;

public class Util {
    static final double DEFAULT_EPSILON = 1e-4;
    public static boolean epsilonEquals(double a, double b, double epsilon) {
        return (a - epsilon <= b) && (a + epsilon >= b);
    }

    public static boolean epsilonEquals(double a, double b) {
        return epsilonEquals(a, b, DEFAULT_EPSILON);
    }

    public static boolean epsilonEquals(Twist2d a, Twist2d b, double eps) {
        return epsilonEquals(a.dx, b.dx, eps) &&
                epsilonEquals(a.dy, b.dy, eps) &&
                epsilonEquals(a.dtheta, b.dtheta, eps);
    }

    public static boolean epsilonEquals(Twist2d a, Twist2d b) {
        return epsilonEquals(a, b, DEFAULT_EPSILON);
    }
    static Rotation2d FLIP = Rotation2d.fromDegrees(180);
    public static Rotation2d flip(Rotation2d rot) {
        return rot.rotateBy(FLIP);
    }
    public static boolean epsilonEquals(int a, int b, int epsilon) {
        return (a - epsilon <= b) && (a + epsilon >= b);
    }

    public static boolean epsilonEquals(Rotation2d a, Rotation2d b, Measure<Angle> epsilon) {
        return epsilonEquals(a.getRadians(), b.getRadians(), epsilon.in(Radians));
    }
}
