package frc.robot;

import com.pathplanner.lib.auto.NamedCommands;
import com.pathplanner.lib.commands.PathPlannerAuto;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.ConditionalCommand;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.PrintCommand;
import frc.robot.generated.TunerConstants;
import frc.robot.subsystems.drivetrain.CommandSwerveDrivetrain;
import frc.robot.subsystems.drivetrain.WheelRadiusCharacterization;
import frc.robot.subsystems.superstructure.*;

import java.util.Optional;

import static edu.wpi.first.units.Units.*;
import static edu.wpi.first.wpilibj2.command.Commands.run;

/*
NAMING CONVENTION FOR AUTOS(top to bottom blue):
Starting : R, C, L
Close Notes: Close +  3, 2, 1
Far Notes: Far + 5, 4, 3, 2, 1
Shooting Spots: Shooting + Left, LeftStage, RightStage, Right
 */
public class Autos {
    static final CommandSwerveDrivetrain drivetrain = TunerConstants.DriveTrain;
    static final Superstructure superstructure = Superstructure.getInstance();
    final SendableChooser<Command> mChooser = new SendableChooser<>();
    private Command autoSelected;
    Command twoNoteNoNote;

    Command F5F4F3;
    Command F5F4C1;
    Command F3F4F5;
    Command F2F3F1;
    Command CrazyF2F3F1;
    Command F1F2F3;
    Command BloopF3F34;
    Command C1C2C3F2F1;
    Command BLUEC1C2C3F2F1;
    Command C1C2C3F1F2;
    Command C1C2C3F2F3;
    Command C1C2C3F3F2;
    Command C2F1F2F3;
    Command C2F2F3C3Midline;
    Command C2F1F2C3C1;
    Command C2F2F1C3C1;
    Command C2F1F2C3Midline;
    Command C2F2F1C3Midline;
    Command F1SpectrumStealF2F3;

    Command doNothing;
    CommandSwerveDrivetrain.MOIFinder moiFinder = new CommandSwerveDrivetrain.MOIFinder();
    private static Command Shoot() {
        return new MovingShotCommand(drivetrain, superstructure, () -> MetersPerSecond.of(0), () -> MetersPerSecond.of(0), false);
//        return MovingShotCommand.autonomousMovingShot(drivetrain, superstructure);
    }

    private static Command conditionalShoot() {
        return new ConditionalCommand(MovingShotCommand.relativeShot(drivetrain, superstructure,
                () -> MetersPerSecond.of(0),
                () -> MetersPerSecond.of(0),
                () -> false,
                false,
                false), new InstantCommand(() -> {}, superstructure), superstructure::hasGamePiece).finallyDo(superstructure::idle);
    }

    private static Command conditionalShootRelative() {
        return new ConditionalCommand(MovingShotCommand.relativeShot(drivetrain, superstructure,
                () -> MetersPerSecond.of(0),
                () -> MetersPerSecond.of(0),
                () -> false,
                true,
                false), new InstantCommand(() -> {}, superstructure), superstructure::hasGamePiece).finallyDo(superstructure::idle);
    }

    public static Command cancelAll() {
        return superstructure.runOnce(superstructure::cancelAndClear);
    }

    private static Command ShootThenIntake() {
        return MovingShotCommand.autonomousMovingShot(drivetrain, superstructure).andThen(IntakeUntilIntook());
    }

    private static Command BloopThenIntake() {
        return new FixedShotCommand(drivetrain, superstructure, () -> 0, () -> 0, () -> true, FixedShotCommand.ShotType.SLOW).andThen(IntakeUntilIntook());
    }

    private static Command ShootStationary() {
        return MovingShotCommand.autonomousStationaryShot(drivetrain, superstructure);
    }
    private static Command ShootStationarySlow() {
        return MovingShotCommand.autonomousStationaryShot(drivetrain, superstructure, 0.2, 1);
    }

    private static Command ShootStationaryFast() {
        return MovingShotCommand.autonomousStationaryShot(drivetrain, superstructure, 0.6, 5);
    }

    private static Command MovingShot() {
        return MovingShotCommand.autonomousMovingShot(drivetrain, superstructure);
    }

    private static Command IntakeUntilIntook() {
        return new IntakeCmd().withTimeout(5);
    }

    private static Command RevAndAim() {
        return MovingShotCommand.autonomousRevOnly(drivetrain, superstructure);
    }

    private static Command RevAndAimOverride() {
        return MovingShotCommand.autonomousRevOnlyOverride(drivetrain, superstructure);
    }

    private Runnable victor(CommandSwerveDrivetrain dt) {
        return () -> dt.seedFieldRelative(
                new Pose2d(new Translation2d(2, 7), Rotation2d.fromDegrees(DriverStation
                        .getAlliance()
                        .map(a -> a == DriverStation.Alliance.Blue).orElse(true) ? 0 : 180)));
    }

    public void initializeAutos() {
        NamedCommands.registerCommand("Shoot", Shoot());
        NamedCommands.registerCommand("IntakeUntilIntook", IntakeUntilIntook().withName("IntakeUntil"));
        NamedCommands.registerCommand("ShootThenIntake", ShootThenIntake());
        NamedCommands.registerCommand("ShootTake", new ShootTakeCommand(drivetrain, superstructure));
        NamedCommands.registerCommand("ShootTakeAimed", new ShootTakeAimedCommand(drivetrain, superstructure));
        NamedCommands.registerCommand("MovingShot", MovingShot());
        NamedCommands.registerCommand("BloopThenIntake", BloopThenIntake());
        NamedCommands.registerCommand("ShootStationary", ShootStationary());
        NamedCommands.registerCommand("ShootStationarySlow", ShootStationarySlow());
        NamedCommands.registerCommand("ShootStationaryFast", ShootStationaryFast());
        NamedCommands.registerCommand("ConditionalShoot", conditionalShoot());
        NamedCommands.registerCommand("ConditionalShootRel", conditionalShootRelative());
        NamedCommands.registerCommand("RevAndAim", RevAndAim());
        NamedCommands.registerCommand("RevAndAimOverride", RevAndAimOverride());
        NamedCommands.registerCommand("CancelAll", cancelAll());
        NamedCommands.registerCommand("LimelightShoot", MovingShotCommand.relativeShot(drivetrain, superstructure,
                () -> MetersPerSecond.of(0),
                () -> MetersPerSecond.of(0),
                () -> true,
                true,
                true));
        NamedCommands.registerCommand("LimelightShootWithAim", MovingShotCommand.relativeShot(drivetrain, superstructure,
                () -> MetersPerSecond.of(0),
                () -> MetersPerSecond.of(0),
                () -> true,
                true,
                false).withName("LogicPallet"));

        twoNoteNoNote = new PathPlannerAuto("2Note No Note");

        F5F4F3 = new PathPlannerAuto("F5.F4.F3");
        F5F4C1 = new PathPlannerAuto("F5.F4.C1");
        F1F2F3 = new PathPlannerAuto("F1.F2.F3");
        F3F4F5 = new PathPlannerAuto("F3.F4.F5");
        F2F3F1 = new PathPlannerAuto("F2.F3.F1");
        CrazyF2F3F1 = new PathPlannerAuto("CrazyF2.F3.F1");
        BloopF3F34 = new PathPlannerAuto("Bloop.F3.F4");
        C1C2C3F2F1 = new PathPlannerAuto("C1.C2.C3.F2.F1");
        BLUEC1C2C3F2F1 = new PathPlannerAuto("BLUEC1.C2.C3.F2.F1");
        C1C2C3F1F2 = new PathPlannerAuto("C1.C2.C3.F1.F2");
        C1C2C3F2F3 = new PathPlannerAuto("C1.C2.C3.F2.F3");
        C1C2C3F3F2 = new PathPlannerAuto("C1.C2.C3.F3.F2");
        C2F1F2F3 = new PathPlannerAuto("C2.F1.F2.F3");
        C2F2F3C3Midline = new PathPlannerAuto("C2.F2.F3.C3.Midline");
        C2F1F2C3C1 = new PathPlannerAuto("C2.F1.F2.C3.C1");
        C2F2F1C3C1 = new PathPlannerAuto("C2.F2.F1.C3.C1");
        C2F1F2C3Midline = new PathPlannerAuto("C2.F1.F2.C3.Midline");
        C2F2F1C3Midline = new PathPlannerAuto("C2.F2.F1.C3.Midline");
        F1SpectrumStealF2F3 = new PathPlannerAuto("F1.SpectrumSteal.F3");
        doNothing = new PathPlannerAuto("DoNothing");
        mChooser.addOption("Two Note No note", twoNoteNoNote);
        mChooser.addOption("F5.F4.F3", F5F4F3);
        mChooser.addOption("F5.F4.C1", F5F4C1);
        mChooser.addOption("F1.F2.F3", F1F2F3);
        mChooser.addOption("F2.F3.F1", F2F3F1);
        mChooser.addOption("CrazyF2.F3.F1", CrazyF2F3F1);
        mChooser.addOption("F3.F4.F5", F3F4F5);
        mChooser.addOption("Bloop.F3.F4", BloopF3F34);
        mChooser.addOption("C1.C2.C3.F2.F1", C1C2C3F2F1);
        mChooser.addOption("BLUEC1.C2.C3.F2.F1", BLUEC1C2C3F2F1);
        mChooser.addOption("C1.C2.C3.F1.F2", C1C2C3F1F2);
        mChooser.addOption("C1.C2.C3.F2.F3", C1C2C3F2F3);
        mChooser.addOption("C1.C2.C3.F3.F2", C1C2C3F3F2);
        mChooser.addOption("C2.F1.F2.F3", C2F1F2F3);
        mChooser.addOption("C2.F2.F3.C3.Midline", C2F2F3C3Midline);
        mChooser.addOption("C2.F1.F2.C3.C1", C2F1F2C3C1);
        mChooser.addOption("C2.F2.F1.C3.C1", C2F2F1C3C1);
        mChooser.addOption("C2.F1.F2.C3.Midline", C2F1F2C3Midline);
        mChooser.addOption("C2.F2.F1.C3.Midline", C2F2F1C3Midline);
        mChooser.addOption("CharacterizeRadius", new WheelRadiusCharacterization(drivetrain).withTimeout(60.0));
        mChooser.addOption("CharacterizeCouple", drivetrain.characterizedCoupleRatioTester());
        mChooser.addOption("MOI Finder", drivetrain.applyRequest(() -> moiFinder).withTimeout(10));
        mChooser.addOption("F1.SpectrumSteal.F3", F1SpectrumStealF2F3);
        mChooser.addOption("DO NOTHING", doNothing);
    }

    public void putChooserOnDashboard() {
        SmartDashboard.putData("Chooser", mChooser);
    }

    public Command initializeAuto() {
        autoSelected = mChooser.getSelected();
        superstructure.startAuto();
        return autoSelected;
    }

    public Command getAutoSelected() {
        return mChooser.getSelected();
    }

    public Optional<Pose2d> getStartingAutoPose() {
        var cmd = mChooser.getSelected();
        if (cmd instanceof PathPlannerAuto) {
            var ppCmd = (PathPlannerAuto)cmd;
            var rawPose = PathPlannerAuto.getStaringPoseFromAutoFile(ppCmd.getName());
            return Optional.of(CommandSwerveDrivetrain.getMaybeFlippedPose(rawPose));
        }
        return Optional.empty();
    }

    static class IntakeCmd extends Command {
        public IntakeCmd() {
            addRequirements(superstructure);
        }

        @Override
        public void initialize() {
            superstructure.cancelAndClear();
            superstructure.setWantIntakeToShooter();
        }

        @Override
        public boolean isFinished() {
            return superstructure.hasGamePiece();
        }
    }
}
